<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('trans_date');
            $table->integer('account_id');
            $table->integer('chart_id');
            $table->string('type',10);
            $table->string('dr_cr',2);
            $table->decimal('amount',10,2);
            $table->integer('payer_payee_id')->nullable();
            $table->integer('invoice_id')->nullable();
            $table->integer('purchase_id')->nullable();
            $table->integer('purchase_return_id')->nullable();
            $table->integer('payment_method_id');
            $table->string('reference',50)->nullable();
            $table->text('attachment')->nullable();
            $table->text('note')->nullable();
            $table->integer('company_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
