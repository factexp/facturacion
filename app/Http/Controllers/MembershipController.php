<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Stripe\Stripe;
use Stripe\Charge;
use App\PaymentHistory;
use App\EmailTemplate;
use Illuminate\Support\Facades\Mail;
use App\Mail\PremiumMembershipMail;
use App\Utilities\Overrider;

class MembershipController extends Controller
{
   /**
	* Show the application dashboard.
	*
	* @return \Illuminate\Http\Response
	*/
    public function extend()
    {
		return view('membership.extend');
    }
	
	public function pay(Request $request)
    {
		$validator = Validator::make($request->all(), [
			'extend_type' => 'required',
			'year' => 'required_without:month',
			'month' => 'required_without:year',
			'gateway' => 'required',
		]);
		
		if ($validator->fails()) {
			return redirect('membership/extend')
						->withErrors($validator)
						->withInput();				
		}
		
		
		$data = array();
		if($request->extend_type == "yearly"){
			$data['title'] = "Extend Membership for ".$request->year." year.";
			$data['amount'] = get_option('yearly_cost') * $request->year;
		    $data['custom'] = $request->year.' year';
		}else if($request->extend_type == "montly"){
			$data['title'] = "Extend Membership for ".$request->month." month.";
			$data['amount'] = get_option('monthly_cost') * $request->month;
		    $data['custom'] = $request->month.' month';
		}
		
		//Create Pending Payment
		$payment = new PaymentHistory();
		$payment->user_id = company_id();
		$payment->title = $data['title'];
		$payment->method = "";
		$payment->amount = $data['amount'];
		$payment->extend_type = $request->extend_type;
		$payment->extend = $data['custom'];
		$payment->status = 'pending';
		$payment->save();
		
		$data['payment_id'] = $payment->id;
		
		if($request->gateway == "PayPal"){
			return view('membership.gateway.paypal',$data);
		}elseif($request->gateway == "Stripe"){
			return view('membership.gateway.stripe',$data);
		}
    }
	
	//PayPal Payment Gateway
	public function paypal($action){
		if($action == "return"){
			return redirect('/dashboard')->with('paypal_success', _lang('Thank you, You have sucessfully extended your membership. Please wait until you get confrimation email if you still see your membership has expired.'));
		}else if($action == "cancel"){
			return redirect('membership/extend')->with('message', _lang('Payment Canceled !'));
		}
	}
	

	public function paypal_ipn(Request $request)
	{
		//$file = fopen('paypal.txt');
		$payment_id = $request->item_number;
		$amount = $request->mc_gross;
		
		$payment = PaymentHistory::find($payment_id);
		$increment = $payment->extend;
		
		if( $amount >= $payment->amount){
			$user = \App\User::find($payment->user_id);
			$user->valid_to = date('Y-m-d', strtotime($user->valid_to. " +$increment"));
			$user->last_email = NULL;
			$user->save();

			//Save payment History
			$payment->method = "PayPal";
			$payment->status = 'paid';
			$payment->save();
			
			
			//Replace paremeter
			$replace = array(
				'{name}'=>$user->name,
				'{email}'=>$user->email,
				'{valid_to}' =>date('d M,Y', strtotime($user->valid_to)),
			);
			
			//Send email Confrimation
			Overrider::load("Settings");
			$template = EmailTemplate::where('name','premium_membership')->first();
			$template->body = process_string($replace,$template->body);
			Mail::to($user->email)->send(new PremiumMembershipMail($template));
        }		
    }
	
	//Stripe payment Gateway
	public function stripe_payment($payment_id){
		@ini_set('max_execution_time', 0);
		@set_time_limit(0);
		
		Stripe::setApiKey(get_option('stripe_secret_key'));
 
        $token = request('stripeToken');
		
        $payment = PaymentHistory::find($payment_id);
        $increment = $payment->extend;
 
        $charge = Charge::create([
            'amount' => round($payment->amount) * 100,
            'currency' => get_company_option('stripe_currency','USD'),
            'description' => $payment->title,
            'source' => $token,
        ]);

		
		$user = \App\User::find($payment->user_id);
		$user->valid_to = date('Y-m-d', strtotime($user->valid_to. " +$increment"));
		$user->last_email = NULL;
		$user->save();

		//Save payment History
		$payment->method = "Stripe";
		$payment->status = 'paid';
		$payment->save();
		
		//Replace paremeter
		$replace = array(
			'{name}' =>$user->name,
			'{email}' =>$user->email,
			'{valid_to}' =>date('d M,Y', strtotime($user->valid_to)),
		);
		
		//Send email Confrimation
		Overrider::load("Settings");
		$template = EmailTemplate::where('name','premium_membership')->first();
		$template->body = process_string($replace,$template->body);
		Mail::to($user->email)->send(new PremiumMembershipMail($template));

        return redirect('/dashboard')->with('success', _lang('Thank you, You have sucessfully extended your membership.'));
	}
	
}
