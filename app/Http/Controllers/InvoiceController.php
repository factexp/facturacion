<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use App\InvoiceItem;
use App\Stock;
use App\Transaction;
use App\CompanySetting;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use App\Mail\GeneralMail;
use App\Utilities\Overrider;
use Carbon\Carbon;
use PDF;

class InvoiceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoices = Invoice::where("company_id",company_id())
                           ->orderBy("id","desc")->get();
        return view('backend.accounting.invoice.list',compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
		if( ! $request->ajax()){
		   return view('backend.accounting.invoice.create');
		}else{
           return view('backend.accounting.invoice.modal.create');
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {	
		$validator = Validator::make($request->all(), [
			'invoice_number' => 'required|max:191',
            'client_id' => 'required',
            'invoice_date' => 'required',
            'due_date' => 'required',
		]);
		
		if ($validator->fails()) {
			if($request->ajax()){ 
			    return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
			}else{
				return redirect('invoices/create')
							->withErrors($validator)
							->withInput();
			}			
		}
			
	    $company_id = company_id();
		
        $invoice = new Invoice();
	    $invoice->invoice_number = $request->input('invoice_number');
        $invoice->client_id = $request->input('client_id');
        $invoice->invoice_date = $request->input('invoice_date');
        $invoice->due_date = $request->input('due_date');
        $invoice->grand_total = $request->input('product_total');
        $invoice->tax_total = $request->input('tax_total');
        $invoice->paid = 0;
        $invoice->status = $request->input('status');
        $invoice->note = $request->input('note');
        $invoice->company_id = $company_id;
	
        $invoice->save();

        //Save Invoice Item
        for($i=0; $i<count($request->product_id); $i++ ){
			$invoiceItem = new InvoiceItem();
			$invoiceItem->invoice_id = $invoice->id;
			$invoiceItem->item_id = $request->product_id[$i];
			$invoiceItem->quantity = $request->quantity[$i];
			$invoiceItem->unit_cost = $request->unit_cost[$i];
			$invoiceItem->discount = $request->discount[$i];
			$invoiceItem->tax_id = $request->tax_id[$i];
			$invoiceItem->tax_amount = $request->tax_amount[$i];
			$invoiceItem->sub_total = $request->sub_total[$i];
			$invoiceItem->company_id = $company_id;
			$invoiceItem->save();

			//Update Stock if Order Status is received
			if($request->input('order_status') != 'Canceled'){
				$stock = Stock::where("product_id",$invoiceItem->item_id)->where("company_id",$company_id)->first();
                if(!empty($stock)){
                    $stock->quantity =  $stock->quantity - $invoiceItem->quantity;
                    $stock->company_id =  $company_id;
                    $stock->save();
                }
			}
        }
        
        //Increment Invoice Starting number
        increment_invoice_number();
        
		if(! $request->ajax()){
           return redirect('invoices/create')->with('success', _lang('Invoice Created Sucessfully'));
        }else{
		   return response()->json(['result'=>'success','action'=>'store','message'=>_lang('Invoice Created Sucessfully'),'data'=>$invoice]);
		}
        
   }
	

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $invoice = Invoice::where("id",$id)->where("company_id",company_id())->first();
		$transactions = Transaction::where("invoice_id",$id)
								   ->where("company_id",company_id())->get();
		if(! $request->ajax()){
		    return view('backend.accounting.invoice.view',compact('invoice','transactions','id'));
		}else{
			return view('backend.accounting.invoice.modal.view',compact('invoice','transactions','id'));
		} 
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $invoice = Invoice::where("id",$id)->where("company_id",company_id())->first();
		if(! $request->ajax()){
		   return view('backend.accounting.invoice.edit',compact('invoice','id'));
		}else{
           return view('backend.accounting.invoice.modal.edit',compact('invoice','id'));
		}  
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$validator = Validator::make($request->all(), [
			'invoice_number' => 'required|max:191',
            'client_id' => 'required',
            'invoice_date' => 'required',
            'due_date' => 'required',
		]);
		
		if ($validator->fails()) {
			if($request->ajax()){ 
			    return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
			}else{
				return redirect()->route('invoices.edit', $id)
							->withErrors($validator)
							->withInput();
			}			
		}
	
        $company_id = company_id();
		
        $invoice = Invoice::where("id",$id)->where("company_id",$company_id)->first();
		$invoice->invoice_number = $request->input('invoice_number');
        $invoice->client_id = $request->input('client_id');
        $invoice->invoice_date = $request->input('invoice_date');
        $invoice->due_date = $request->input('due_date');
        $invoice->grand_total = $request->input('product_total');
        $invoice->tax_total = $request->input('tax_total');
        //$invoice->paid = $request->input('paid');
        $invoice->status = $request->input('status');
        $invoice->note = $request->input('note');
        $invoice->company_id = $company_id;
	
        $invoice->save();

        //Update Invoice item
		$invoiceItem = InvoiceItem::where("invoice_id",$id);
        $invoiceItem->delete();

		for($i=0; $i<count($request->product_id); $i++ ){
			$invoiceItem = new InvoiceItem();
			$invoiceItem->invoice_id = $invoice->id;
			$invoiceItem->item_id = $request->product_id[$i];
			$invoiceItem->quantity = $request->quantity[$i];
			$invoiceItem->unit_cost = $request->unit_cost[$i];
			$invoiceItem->discount = $request->discount[$i];
			$invoiceItem->tax_id = $request->tax_id[$i];
			$invoiceItem->tax_amount = $request->tax_amount[$i];
			$invoiceItem->sub_total = $request->sub_total[$i];
			$invoiceItem->company_id = $company_id;
			$invoiceItem->save();
		}
		
		if(! $request->ajax()){
           return redirect('invoices')->with('success', _lang('Invoice updated sucessfully'));
        }else{
		   return response()->json(['result'=>'success','action'=>'update', 'message'=>_lang('Invoice updated sucessfully'),'data'=>$invoice]);
		}
	    
    }
	
	/**
     * Generate PDF
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function download_pdf(Request $request, $id)
    {
		@ini_set('max_execution_time', 0);
	    @set_time_limit(0);
	
		$invoice = Invoice::where("id",$id)->where("company_id",company_id())->first();
		$data['invoice'] = $invoice;
		$data['transactions'] = Transaction::where("invoice_id",$id)
								           ->where("company_id",company_id())->get();

	
			
		$pdf = PDF::loadView("backend.accounting.invoice.pdf_export", $data);
		$pdf->setWarnings(false);
		
		//return $pdf->stream();
		return $pdf->download("invoice_{$invoice->invoice_number}.pdf");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoice = Invoice::where("id",$id)->where("company_id",company_id());
        $invoice->delete();

        $invoiceItem = InvoiceItem::where("invoice_id",$id);
        $invoiceItem->delete();

        return redirect('invoices')->with('success',_lang('Invoice deleted sucessfully'));
    }
	
	public function create_payment(Request $request, $id)
    {
		$invoice = Invoice::where("id",$id)->where("company_id",company_id())->first();
		
		if($request->ajax()){
		   return view('backend.accounting.invoice.modal.create_payment',compact('invoice','id'));
		} 
	}
	
	public function store_payment(Request $request)
    {
        $validator = Validator::make($request->all(), [
			'invoice_id' => 'required',
			'account_id' => 'required',
			'chart_id' => 'required',
			'amount' => 'required|numeric',
			'payment_method_id' => 'required',
			'reference' => 'nullable|max:50',
			'attachment' => 'nullable|mimes:jpeg,png,jpg,doc,pdf,docx,zip',
		]);
		
		if ($validator->fails()) {
			if($request->ajax()){ 
			    return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
			}else{
				return redirect('income/create')
							->withErrors($validator)
							->withInput();
			}			
		}

		$attachment = "";
        if($request->hasfile('attachment'))
		{
		  $file = $request->file('attachment');
		  $attachment = time().$file->getClientOriginalName();
		  $file->move(public_path()."/uploads/transactions/", $attachment);
		}
			
        $company_id = company_id();
		
        $transaction= new Transaction();
	    $transaction->trans_date = date('Y-m-d');
		$transaction->account_id = $request->input('account_id');
		$transaction->chart_id = $request->input('chart_id');
		$transaction->type = 'income';
		$transaction->dr_cr = 'cr';
		$transaction->amount = $request->input('amount');
		$transaction->payer_payee_id = $request->input('client_id');
		$transaction->payment_method_id = $request->input('payment_method_id');
		$transaction->invoice_id = $request->input('invoice_id');
		$transaction->reference = $request->input('reference');
		$transaction->note = $request->input('note');
		$transaction->attachment = $attachment;
		$transaction->company_id = $company_id;
		
        $transaction->save();
		
		//Update Invoice Table
		$invoice = Invoice::where("id",$transaction->invoice_id)
						  ->where("company_id",$company_id)->first();
						  
		$invoice->paid = $invoice->paid + $transaction->amount;				
        if($invoice->paid >= $invoice->grand_total){
			$invoice->status = 'Paid';
		}else if($invoice->paid > 0 && ($invoice->paid < $invoice->grand_total)){
			$invoice->status = 'Partially_Paid';
		}
		$invoice->save();

		if( $request->ajax() ){
		   return response()->json(['result'=>'success','action'=>'store','message'=>_lang('Payment was made Sucessfully'),'data'=>$transaction]);
		}
    }
	
	public function view_payment(Request $request, $invoice_id){

		$transactions = Transaction::where("invoice_id",$invoice_id)
								   ->where("company_id",company_id())->get();
	
	    if(! $request->ajax()){
		    return view('backend.accounting.invoice.view_payment',compact('transactions'));
		}else{
			return view('backend.accounting.invoice.modal.view_payment',compact('transactions'));
		} 
	}
	
	public function create_email(Request $request, $invoice_id)
    {
		$invoice = Invoice::where("id",$invoice_id)
						  ->where("company_id",company_id())->first();
		
		$client_email = $invoice->client->contact_email; 
		if($request->ajax()){
		    return view('backend.accounting.invoice.modal.send_email',compact('client_email','invoice'));
		} 
	}	
	
	public function send_email(Request $request)
    {
		@ini_set('max_execution_time', 0);
	    @set_time_limit(0);
	    Overrider::load("Settings");
	   

		//Send email
		$subject = $request->input("email_subject");
		$message = $request->input("email_message");
		$contact_email = $request->input("contact_email");
		
		$mail  = new \stdClass();
		$mail->subject = $subject;
		$mail->body = $message;
		Mail::to($contact_email)->send(new GeneralMail($mail));
		
        if(! $request->ajax()){
           return back()->with('success', _lang('Email Send Sucessfully'));
        }else{
		   return response()->json(['result'=>'success', 'action'=>'update', 'message'=>_lang('Email Send Sucessfully'),'data'=>$contact]);
		}
    }
	
}
