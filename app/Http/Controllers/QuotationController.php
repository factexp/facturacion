<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quotation;
use App\QuotationItem;
use App\CompanySetting;
use App\Invoice;
use App\InvoiceItem;
use App\Stock;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;
use App\Mail\GeneralMail;
use App\Utilities\Overrider;
use Carbon\Carbon;
use PDF;

class QuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quotations = Quotation::where("company_id",company_id())
							   ->orderBy("id","desc")->get();
        return view('backend.accounting.quotation.list',compact('quotations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
		if( ! $request->ajax()){
		   return view('backend.accounting.quotation.create');
		}else{
           return view('backend.accounting.quotation.modal.create');
		}
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {	
		$validator = Validator::make($request->all(), [
			'quotation_number' => 'required|max:191',
            'client_id' => 'required',
            'quotation_date' => 'required',
		]);
		
		if ($validator->fails()) {
			if($request->ajax()){ 
			    return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
			}else{
				return redirect('quotations/create')
							->withErrors($validator)
							->withInput();
			}			
		}
			
	    $company_id = company_id();
		
        $quotation= new Quotation();
	    $quotation->quotation_number = $request->input('quotation_number');
        $quotation->client_id = $request->input('client_id');
        $quotation->quotation_date = $request->input('quotation_date');
        $quotation->grand_total = $request->input('product_total');
        $quotation->tax_total = $request->input('tax_total');
        $quotation->note = $request->input('note');
        $quotation->company_id = $company_id;
	
        $quotation->save();

        //Save quotation Item
        for($i=0; $i<count($request->product_id); $i++ ){
			$quotationItem = new quotationItem();
			$quotationItem->quotation_id = $quotation->id;
			$quotationItem->item_id = $request->product_id[$i];
			$quotationItem->quantity = $request->quantity[$i];
			$quotationItem->unit_cost = $request->unit_cost[$i];
			$quotationItem->discount = $request->discount[$i];
			$quotationItem->tax_id = $request->tax_id[$i];
			$quotationItem->tax_amount = $request->tax_amount[$i];
			$quotationItem->sub_total = $request->sub_total[$i];
			$quotationItem->save();

        }
        
        //Increment quotation Starting number
        $data = array();
        $data['value'] = $request->quotation_starting_number + 1; 
        $data['company_id'] = $company_id; 
        $data['updated_at'] = Carbon::now();
        
        if(CompanySetting::where('name', "quotation_starting")->where("company_id",$company_id)->exists()){				
           CompanySetting::where('name','quotation_starting')
                         ->where("company_id",$company_id)
                         ->update($data);			
        }else{
           $data['name'] = 'quotation_starting'; 
           $data['created_at'] = Carbon::now();
           CompanySetting::insert($data); 
        }
        
		if(! $request->ajax()){
           return redirect('quotations/create')->with('success', _lang('Quotation Created Sucessfully'));
        }else{
		   return response()->json(['result'=>'success','action'=>'store','message'=>_lang('Quotation Created Sucessfully'),'data'=>$quotation]);
		}
        
   }
	

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $quotation = Quotation::where("id",$id)->where("company_id",company_id())->first();
		if(! $request->ajax()){
		    return view('backend.accounting.quotation.view',compact('quotation','id'));
		}else{
			return view('backend.accounting.quotation.modal.view',compact('quotation','id'));
		} 
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $quotation = Quotation::where("id",$id)->where("company_id",company_id())->first();
		if(! $request->ajax()){
		   return view('backend.accounting.quotation.edit',compact('quotation','id'));
		}else{
           return view('backend.accounting.quotation.modal.edit',compact('quotation','id'));
		}  
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$validator = Validator::make($request->all(), [
			'quotation_number' => 'required|max:191',
            'client_id' => 'required',
            'quotation_date' => 'required',
		]);
		
		if ($validator->fails()) {
			if($request->ajax()){ 
			    return response()->json(['result'=>'error','message'=>$validator->errors()->all()]);
			}else{
				return redirect()->route('quotations.edit', $id)
							->withErrors($validator)
							->withInput();
			}			
		}
	
        $company_id = company_id();
		
        $quotation = Quotation::where("id",$id)->where("company_id",$company_id)->first();
		$quotation->quotation_number = $request->input('quotation_number');
        $quotation->client_id = $request->input('client_id');
        $quotation->quotation_date = $request->input('quotation_date');
        $quotation->grand_total = $request->input('product_total');
        $quotation->tax_total = $request->input('tax_total');
        $quotation->note = $request->input('note');
        $quotation->company_id = $company_id;
	
        $quotation->save();

        //Update quotation item
		$quotationItem = QuotationItem::where("quotation_id",$id);
        $quotationItem->delete();

		for($i=0; $i<count($request->product_id); $i++ ){
			$quotationItem = new quotationItem();
			$quotationItem->quotation_id = $quotation->id;
			$quotationItem->item_id = $request->product_id[$i];
			$quotationItem->quantity = $request->quantity[$i];
			$quotationItem->unit_cost = $request->unit_cost[$i];
			$quotationItem->discount = $request->discount[$i];
			$quotationItem->tax_id = $request->tax_id[$i];
			$quotationItem->tax_amount = $request->tax_amount[$i];
			$quotationItem->sub_total = $request->sub_total[$i];
			$quotationItem->save();
		}
		
		if(! $request->ajax()){
           return redirect('quotations')->with('success', _lang('Quotation updated sucessfully'));
        }else{
		   return response()->json(['result'=>'success','action'=>'update', 'message'=>_lang('Quotation updated sucessfully'),'data'=>$quotation]);
		}
	    
    }
	
	/**
     * Generate PDF
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function download_pdf(Request $request, $id)
    {
		@ini_set('max_execution_time', 0);
	    @set_time_limit(0);
	
		$quotation = Quotation::where('id',$id)->first();
		$data['quotation'] = $quotation;
	    $data['company'] = CompanySetting::where('company_id',$data['quotation']->company_id)->get();
		

		$pdf = PDF::loadView("backend.accounting.quotation.pdf_export", $data);
		$pdf->setWarnings(false);
		
		//return $pdf->stream();
		return $pdf->download("quotation_{$quotation->quotation_number}.pdf");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quotation = Quotation::where("id",$id)->where("company_id",company_id());
        $quotation->delete();

        $quotationItem = QuotationItem::where("quotation_id",$id);
        $quotationItem->delete();

        return redirect('quotations')->with('success',_lang('Quotation Removed Sucessfully'));
    }
	
	public function convert_invoice($quotation_id){
		@ini_set('max_execution_time', 0);
	    @set_time_limit(0);
		
		$company_id = company_id();
		$quotation = Quotation::where("id",$quotation_id)
							  ->where("company_id",$company_id)->first();
		
		
		
        $invoice = new Invoice();
	    $invoice->invoice_number = get_company_option('invoice_prefix').get_company_option('invoice_starting');
        $invoice->client_id = $quotation->client_id;
        $invoice->invoice_date = date('Y-m-d');
        $invoice->due_date = date('Y-m-d');
        $invoice->grand_total = $quotation->grand_total;
        $invoice->tax_total = $quotation->tax_total;
        $invoice->paid = 0;
        $invoice->status = 'Unpaid';
        $invoice->note = $quotation->note;
        $invoice->company_id = $company_id;
	
        $invoice->save();

        //Save Invoice Item
        foreach($quotation->quotation_items as $quotation_item){
			$invoiceItem = new InvoiceItem();
			$invoiceItem->invoice_id = $invoice->id;
			$invoiceItem->item_id = $quotation_item->item_id;
			$invoiceItem->quantity = $quotation_item->quantity;
			$invoiceItem->unit_cost = $quotation_item->unit_cost;
			$invoiceItem->discount = $quotation_item->discount;
			$invoiceItem->tax_id = $quotation_item->tax_id;
			$invoiceItem->tax_amount = $quotation_item->tax_amount;
			$invoiceItem->sub_total = $quotation_item->sub_total;
			$invoiceItem->company_id = $company_id;
			$invoiceItem->save();

			//Update Stock if Order Status is received
			$stock = Stock::where("product_id",$invoiceItem->item_id)->where("company_id",$company_id)->first();
			if(!empty($stock)){
				$stock->quantity =  $stock->quantity - $invoiceItem->quantity;
				$stock->company_id =  $company_id;
				$stock->save();
			}
			
        }
        
        //Increment Invoice Starting number
        increment_invoice_number();
		
		
        return redirect('invoices/'.$invoice->id)->with('success', _lang('Quotation Converted Sucessfully'));
        
		
	}
	
	public function create_email(Request $request, $quotation_id)
    {
		$quotation = Quotation::where("id",$quotation_id)
						  ->where("company_id",company_id())->first();
		
		$client_email = $quotation->client->contact_email; 
		if($request->ajax()){
		    return view('backend.accounting.quotation.modal.send_email',compact('client_email','quotation'));
		} 
	}	
	
	public function send_email(Request $request)
    {
		@ini_set('max_execution_time', 0);
	    @set_time_limit(0);
	    Overrider::load("Settings");
	   

		//Send email
		$subject = $request->input("email_subject");
		$message = $request->input("email_message");
		$contact_email = $request->input("contact_email");
		
		$mail  = new \stdClass();
		$mail->subject = $subject;
		$mail->body = $message;
		Mail::to($contact_email)->send(new GeneralMail($mail));
		
        if(! $request->ajax()){
           return back()->with('success', _lang('Email Send Sucessfully'));
        }else{
		   return response()->json(['result'=>'success', 'action'=>'update', 'message'=>_lang('Email Send Sucessfully'),'data'=>$contact]);
		}
    }
}
