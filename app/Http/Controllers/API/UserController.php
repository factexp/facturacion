<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Hash;


class UserController extends Controller 
{
	public $successStatus = 200;
	
	/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            return response()->json(['success' => $success], $this-> successStatus); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }
	
	/** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:191', 
            'email' => 'required|email|unique:users|max:191', 
            'password' => 'required|max:20|min:6', 
        ]);

		
        if ($validator->fails()) { 
                return response()->json(['error'=>$validator->errors()], 401);            
        }
        
        $input = $request->all(); 
        $input['password'] = Hash::make($input['password']); 
        $input['user_type'] = 'user';
        $input['status'] = 1;
        $input['profile_picture'] = 'default.png';
        $input['currency'] = '$';
        
        $trial_period = get_option('trial_period',7);

        if($trial_period < 1){
        $valid_to = date('Y-m-d', strtotime(date('Y-m-d'). " -1 day"));
        }else{
        $valid_to = date('Y-m-d', strtotime(date('Y-m-d'). " + $trial_period days"));
        }
        $input['valid_to'] = $valid_to;
        
        $user = User::create($input); 
        
        $success['token'] =  $user->createToken('MyApp')->accessToken; 
        $success['name'] =  $user->name;

        return response()->json(['success'=>$success], $this->successStatus); 
			
	}
	
	/** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this->successStatus); 
    } 
}