<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\EmailTemplate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Mail\RegistrationMail;
use App\Utilities\Overrider;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';
	
	public function showRegistrationForm()
	{
		if(get_option('allow_singup','yes') != 'yes'){
			return redirect('login');
		}else{
			return view('auth.register');
		}
	}
	

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
		$trial_period = get_option('trial_period',7);
		
		if($trial_period < 1){
			$valid_to = date('Y-m-d', strtotime(date('Y-m-d'). " -1 day"));
		}else{
			$valid_to = date('Y-m-d', strtotime(date('Y-m-d'). " + $trial_period days"));
		}
		
		$user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'user_type' => 'user',
            'status' => 1,
			'valid_to' => $valid_to,
            'profile_picture' => 'default.png',
            'currency' => '$',
        ]);
		
		//Replace paremeter
		$replace = array(
			'{name}'=>$data['name'],
			'{email}'=>$data['email'],
			'{valid_to}' =>date('d M,Y', strtotime($valid_to)),
	    );
		
		//Send Welcome email
		Overrider::load("Settings");
		$template = EmailTemplate::where('name','registration')->first();
		$template->body = process_string($replace,$template->body);
		
		try{
			Mail::to($data['email'])->send(new RegistrationMail($template));
		}catch (\Exception $e) {
			// Nothing
		}
		return $user;
    }
	
}
