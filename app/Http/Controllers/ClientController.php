<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Invoice;
use App\Quotation;
use Stripe\Stripe;
use Stripe\Charge;
use Auth;

class ClientController extends Controller
{
	
	public function __construct()
    {	

    }

    public function invoices($status = ''){
	  $data = array();
	  if($status != ''){
		  $data['invoices'] = Invoice::where('client_id',Auth::user()->client->id)
									 ->where('status',$status)->get();
	  }else{
		  $data['invoices'] = Invoice::where('client_id',Auth::user()->client->id)->get();
	  }
	  return view('backend.client_panel.invoices',$data);						   

    }
	
	public function view_invoice($id){
		$invoice = Invoice::whereRaw('MD5(id)=?',[$id])
						  ->where('client_id',Auth::user()->client->id)->first();
		$transactions = Transaction::whereRaw('MD5(invoice_id)=?',$id)
								   ->where("payer_payee_id",Auth::user()->client->id)->get();
		return view('backend.client_panel.view_invoice',compact('invoice','transactions'));	
	}
	
	public function make_payment($invoice_id){
		@ini_set('max_execution_time', 0);
		@set_time_limit(0);
		
		$invoice = Invoice::where('id',$invoice_id)
						  ->where('client_id',Auth::user()->client->id)->first();
	    
		
		Stripe::setApiKey(get_company_option('stripe_secret_key'));
 
        $token = request('stripeToken');
		
        $charge = Charge::create([
            'amount' => round($invoice->grand_total - $invoice->paid) * 100,
            'currency' => get_company_option('stripe_currency'),
            'description' => _lang('Invoice Payment'),
            'source' => $token,
        ]);
		
		//echo $charge['amount'];
		//dd($charge);
		$company_id = company_id();
		
		if(get_company_option('default_account') != '' && get_company_option('default_chart_id') != ''){
			$transaction = new Transaction();
			$transaction->trans_date = date('Y-m-d');
			$transaction->account_id = get_company_option('default_account');
			$transaction->chart_id = get_company_option('default_chart_id');
			$transaction->type = 'income';
			$transaction->dr_cr = 'cr';
			$transaction->amount = ($charge['amount'] / 100);
			$transaction->payer_payee_id = $invoice->client_id;
			$transaction->payment_method_id = payment_method('Stripe',$invoice->company_id);
			$transaction->invoice_id = $invoice->id;
			//$transaction->reference = $request->input('reference');
			//$transaction->note = $request->input('note');
			$transaction->company_id = $company_id;
			
			$transaction->save();
		}
	
		
		//Update Invoice Table					  
		$invoice->paid = $invoice->paid + ($charge['amount']/100);				
        $invoice->status = 'Paid';
		$invoice->save();

		return back()->with('success', _lang('Thank You, Your payment was made sucessfully.'));
	    
	}
	
	public function quotations(){
	  $data = array();
	  $data['quotations'] = Quotation::where('client_id',Auth::user()->client->id)->get();
	  
	  return view('backend.client_panel.quotations',$data);						   

    }
	
	public function view_quotation($id){
		$quotation = Quotation::whereRaw('MD5(id)=?',[$id])
							  ->where('client_id',Auth::user()->client->id)->first();
	    return view('backend.client_panel.view_quotation',compact('quotation'));	
	}
	
	public function transactions(){
		$transactions = Transaction::where('payer_payee_id',Auth::user()->client->id)->get();
	    return view('backend.client_panel.transactions',compact('transactions'));
	}
	
	public function view_transaction(Request $request, $id){
		$transaction = Transaction::where('id',$id)
							      ->where('payer_payee_id',Auth::user()->client->id)->first();
	    if($request->ajax()){
		    return view('backend.client_panel.view_transaction',compact('transaction','id'));
		}

	}
	
	/* PayPal payment gateway */
	public function paypal($action){
		if($action == "return"){
			return redirect('dashboard')->with('paypal_success', _lang('Thank you, Your payment was made sucessfully.'));
		}else if($action == "cancel"){
			return redirect('client/invoices')->with('error', _lang('Payment Canceled !'));
		}
	}
	

	public function paypal_ipn(Request $request)
	{
	 
		$invoice_number = $request->item_number;
		$id = $request->custom;
		$amount = $request->mc_gross;
		
		$invoice = Invoice::where('invoice_number',$invoice_number)
		                  ->where('id',$id)->first();

		if( $amount >= ($invoice->grand_total - $invoice->paid)){
			if(get_company_option('default_account') != '' && get_company_option('default_chart_id') != ''){
			
				$transaction = new Transaction();
				$transaction->trans_date = date('Y-m-d');
				$transaction->account_id = get_company_option('default_account');
				$transaction->chart_id = get_company_option('default_chart_id');
				$transaction->type = 'income';
				$transaction->dr_cr = 'cr';
				$transaction->amount = $amount;
				$transaction->payer_payee_id = $invoice->client_id;
				$transaction->payment_method_id = payment_method('PayPal',$invoice->company_id);
				$transaction->invoice_id = $invoice->id;
				
				$transaction->save();
			}
	
		
			//Update Invoice Table					  
			$invoice->paid = ($invoice->paid + $amount);				
			$invoice->status = 'Paid';
			$invoice->save();	
        }
    }

	
}
