<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\AccessControl;

class PermissionController extends Controller
{
    public function index($user_id='')
    {
		$permission_list = array();
		$user_id = $user_id;
		
		if ($user_id !=''){
		   $permission_list = AccessControl::where("user_id",$user_id)->pluck('permission')->toArray(); 
		}
		
		$notallowed = array(
		    '\App\Http\Controllers\Auth\LoginController',
		    'App\Http\Controllers\Auth\LoginController',
			'App\Http\Controllers\Auth\RegisterController',
			'App\Http\Controllers\Auth\ForgotPasswordController',
			'App\Http\Controllers\Auth\ResetPasswordController',
			'App\Http\Controllers\DashboardController',
			'App\Http\Controllers\ProfileController',
			'App\Http\Controllers\UserController',
			'App\Http\Controllers\LanguageController',
			'App\Http\Controllers\UtilityController',
			'App\Http\Controllers\StaffController',
			'App\Http\Controllers\EmailTemplateController',
			'App\Http\Controllers\MembershipController',
			'App\Http\Controllers\CronJobsController',
			'App\Http\Controllers\PermissionController',
			'\Laravel\Passport\Http\Controllers\AuthorizationController',
			'\Laravel\Passport\Http\Controllers\ApproveAuthorizationController',
			'\Laravel\Passport\Http\Controllers\DenyAuthorizationController',
			'\Laravel\Passport\Http\Controllers\AccessTokenController',
			'\Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController',
			'\Laravel\Passport\Http\Controllers\TransientTokenController',
			'\Laravel\Passport\Http\Controllers\ClientController',
			'\Laravel\Passport\Http\Controllers\ScopeController',
			'\Laravel\Passport\Http\Controllers\PersonalAccessTokenController',
			'App\Http\Controllers\API\UserController',
			'App\Http\Controllers\Install\InstallController',
			
		);
		
		$ignoreRoute = array(
		    //'events.show',
			//'notices.show',
		);
		
		$app = app();

		$routeCollection = $app->routes->getRoutes();
		
		$routes = [];
	
		
		// loop through the collection of routes
		foreach ($routeCollection as $route) {

			// get the action which is an array of items
			$action = $route->getAction();

			// if the action has the key 'controller' 
			if (array_key_exists('controller', $action)) {

				// explode the string with @ creating an array with a count of 2
				$explodedAction = explode('@', $action['controller']);

				//If not needed so ignore
				if(in_array($explodedAction[0],$notallowed)){
					continue;
				}
				
				if (!isset($routes[$explodedAction[0]])) {
					$routes[$explodedAction[0]] = [];
				}
				
				$test = new $explodedAction[0]();
				if(method_exists($test ,$explodedAction[1])){
				    $routes[$explodedAction[0]][] = array("method"=>$explodedAction[1],"action"=>$route->action);
				}
				
			}
		}

		$permission = array();
		
		foreach($routes as $key=>$route){
			foreach($route as $r){
				if (strpos($r['method'], 'get') === 0) {
				   continue;
				}	

                if(array_key_exists('as',$r['action'])){
					$routeName = $r['action']['as'];
                    //If not needed so ignore
					if(in_array($routeName,$ignoreRoute)){
						continue;
					}					
			    	$permission[$key][$routeName] = $r['method'];
				}

			}
		}
		
	
		
		foreach($permission as $key=>$val){
			foreach($val as $name=>$url){
				if($url == "store" && in_array("create", $val)){
					unset($permission[$key][$name]);
				}
				if($url == "update" && in_array("edit", $val)){
					unset($permission[$key][$name]);
				}
			}
		}

		return view('backend.permission.create',compact('permission','permission_list','user_id'));
		
    }
	
	public function store(Request $request){
		$this->validate($request, [
            'user_id' => 'required',
            'permissions' => 'required'
        ]);
		
		$permission = AccessControl::where("user_id",$request->user_id);
        $permission->delete();
		
		foreach($request->permissions as $role){
			$permission = new AccessControl();
			$permission->user_id = $request->user_id;
			$permission->permission = $role;
			$permission->save();
		}
		
		return redirect('permission/control')->with('success', _lang('Saved Sucessfully'));
		
	}
	
   
}
