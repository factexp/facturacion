<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payment_histories';
	
	public function user()
    {
        return $this->belongsTo('App\User')->withDefault();
    }
}