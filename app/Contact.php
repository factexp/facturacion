<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contacts';
	
	public function group()
    {
        return $this->hasOne('App\ContactGroup','id','group_id');
    }
	
	public function user()
    {
        return $this->belongsTo('App\User')->withDefault();
    }
}