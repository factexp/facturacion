$(function () {
    //*** Yearly Income and expense Chart ***//
    var link = _url + "Dashboard/json_month_wise_income_expense/";
    $.ajax({
        method: "POST", url: link, success: function (data) {
            var json = JSON.parse(data);
            //alert(json['Income']);
            var cashflow = echarts.init(document.getElementById('cashflow'));

            // specify chart configuration item and data
            var option = {
                /* title : {
                 text: 'Income And Expense'
                 },*/
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['Income', 'Expense']
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: {show: true},
                        dataView: {
                            show: true,
                            readOnly: false,
                            title: 'Data View',
                            lang: ['Data View', 'Cancel', 'Reset']
                        },
                        magicType: {
                            show: true, title: {
                                line: 'Line',
                                bar: 'Bar',
                                stack: 'Stack',
                                tiled: 'Tiled',
                                force: 'Force',
                                chord: 'Chord',
                                pie: 'Pie',
                                funnel: 'Funnel'
                            }, type: ['line', 'bar', 'stack', 'tiled']
                        },
                        restore: {show: true, title: 'Reset'},
                        saveAsImage: {
                            show: true, title: 'Save as Image',
                            type: 'png',
                            lang: ['Click to Save']
                        }
                    }

                },
                calculable: true,
                xAxis: [
                    {
                        type: 'category',
                        boundaryGap: false,
                        //data : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
                        data: json['Months']
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: 'Income',
                        type: 'line',
                        stack: 'stack',
                        color: [
                            '#2196f3'
                        ],
                        //data:[120, 132, 101, 134, 90, 230, 210,560,240,963,630,550]
                        data: json['Income']
                    },
                    {
                        name: 'Expense',
                        type: 'line',
                        stack: 'stack',
                        color: [
                            '#eb3c00'
                        ],
                        //data:[220, 182, 191, 234, 290, 330, 310,101, 134, 90, 230, 210]
                        data: json['Expense']
                    }
                ]
            };

            // use configuration item and data specified to show chart
            cashflow.setOption(option);

        }
    });

    //Income Vs Expense Donut Chart
    var _url = $("#_url").val() + "Dashboard/json_income_vs_expense/";
    $.ajax({
        method: "POST", url: _url, success: function (data) {
            var json = JSON.parse(data);
            var dn_income_expense = echarts.init(document.getElementById('dn_income_expense'));
            var option2 = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data: ['Income', 'Expense']
                },

                calculable: true,
                series: [
                    {
                        name: 'Income & Expense',
                        type: 'pie',
                        radius: ['50%', '70%'],
                        color: ['#2196f3', '#eb3c00'],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true
                                },
                                labelLine: {
                                    show: true
                                }
                            },
                            emphasis: {
                                label: {
                                    show: true,
                                    position: 'center',
                                    textStyle: {
                                        fontSize: '24',
                                        fontWeight: 'bold'
                                    }
                                }
                            }
                        },
                        data: [
                            {value: json['Income'], name: 'Income'},
                            {value: json['Expense'], name: 'Expense'},
                        ]
                    }
                ]
            };


            dn_income_expense.setOption(option2);
        }
    });


    //Income Vs Expense Line Chart
    var _url = $("#_url").val() + "Dashboard/json_day_wise_income_expense/";
    $.ajax({
        method: "POST", url: _url, success: function (data) {
            var json = JSON.parse(data);
            var line_income_expense = echarts.init(document.getElementById('line_income_expense'));

            // specify chart configuration item and data
            var option3 = {
                /* title : {
                 text: 'Income And Expense'
                 },*/
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['Income', 'Expense']
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: {show: true},
                        dataView: {
                            show: true,
                            readOnly: false,
                            title: 'Data View',
                            lang: ['Data View', 'Cancel', 'Reset']
                        },
                        magicType: {
                            show: true, title: {
                                line: 'Line',
                                bar: 'Bar',
                                stack: 'Stack',
                                tiled: 'Tiled',
                                force: 'Force',
                                chord: 'Chord',
                                pie: 'Pie',
                                funnel: 'Funnel'
                            }, type: ['line', 'bar', 'stack', 'tiled']
                        },
                        restore: {show: true, title: 'Reset'},
                        saveAsImage: {
                            show: true, title: 'Save as Image',
                            type: 'png',
                            lang: ['Click to Save']
                        }
                    }

                },
                calculable: true,
                xAxis: [
                    {
                        type: 'category',
                        boundaryGap: false,
                        data: ['01', '02', '03', '04', '05', '06',
                            '07', '08', '09', '10', '11', '12',
                            '13', '14', '15', '16', '17', '18',
                            '19', '20', '21', '22', '23', '24',
                            '25', '26', '27', '28', '29', '30', '31',
                        ]
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: 'Income',
                        type: 'line',
                        smooth: true,
                        itemStyle: {normal: {areaStyle: {type: 'default'}}},
                        color: [
                            '#2196f3'
                        ],
                        data: json['Income']
                    },
                    {
                        name: 'Expense',
                        type: 'line',
                        smooth: true,
                        itemStyle: {normal: {areaStyle: {type: 'default'}}},
                        color: [
                            '#eb3c00'
                        ],
                        data: json['Expense']
                    }
                ]
            };

            line_income_expense.setOption(option3);
        }
    });
    //End Ajax

});


$(window).on('resize', function () {
    location.reload();
});