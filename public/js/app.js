$(document).ready(function () {
	//Set Sidebar Style
    if (typeof(Storage) !== "undefined") {
        if (localStorage.getItem("sidebar") == "responsive") {
            responsive_sidebar();
        } else {
            full_sidebar();
        }
    }

	//Dropdown
    $('.dropdown-toggle').dropdown();

	//Init Navigation
    $('#menu').metisMenu();

    $(".mini-nav").click(function () {
        if ($(".sidebar").width() == 250) {
            responsive_sidebar();
            if (typeof(Storage) !== "undefined") {
                localStorage.setItem("sidebar", "responsive");
            }
        } else {
            full_sidebar();
            if (typeof(Storage) !== "undefined") {
                localStorage.setItem("sidebar", "full");
            }
        }

    });

	//Active menu
    $("#menu li a").each(function () {
        var path = window.location.href;
        if ($(this).attr("href") == path) {
            $("#menu li a").removeClass("active-menu");
            $(this).addClass("active-menu");
            $(this).parent().parent().addClass("in");
            $(this).parent().parent().attr("aria-expanded", true);
            $(this).parent().parent().prev().attr("aria-expanded", true);
            $(this).parent().parent().parent().addClass("active");
        }
    });


    function responsive_sidebar() {
        $(".menu-title").css("display", "none");
        $("#menu span").css("display", "none");
        $(".sidebar").animate({width: '60px'}, 500);
        $(".welcome-options").css("display", "none")
        $(".welcome-box").css("padding", "10px");
        $("#menu").css("padding-left", "7px");	
		//Colse If Expand
        $("#menu li ul").css("display", "none");
		//content Animation
		if(direction != "rtl"){
			$(".content").animate({paddingLeft: '60px'}, 500);
        }else{
			$(".content").animate({paddingRight: '60px'}, 500);
		}
	}

    function full_sidebar() {
        $(".sidebar").animate({width: '250px'}, 300, function () {
            $(".menu-title").css("display", "inline");
            $("#menu span").css("display", "inline");
            $(".welcome-options").removeAttr("style");
            $("#menu li ul").removeAttr("style");
        });

        $(".welcome-box").removeAttr("style");
        $("#menu").css("padding-left", "0px");
		//content Animation
		if(direction != "rtl"){
			$(".content").animate({paddingLeft: '250px'}, 300);
		}else{
			$(".content").animate({paddingRight: '250px'}, 300);
		}
    }


	//prevent menu Click
    $('#menu li').click(function () {
        if ($(".sidebar").width() == 60) {
            $("#menu").find("ul").css("display", "none");
			//$("#menu").find('li').has('ul').children('a').off('click');
        }
    });


	//Menu Hover Effect
    $('#menu li').hover(function () {
		if ($(".sidebar").width() == 60) {
			$(this).find("ul").removeAttr("style");
			$(this).find("ul").css("display", "block");
			$(this).find("ul").addClass("micro_menu");
		}
	},
	function () {
		if ($(".sidebar").width() == 60) {
			$(this).find("ul").css("display", "none");
			$(this).find("ul").removeClass("micro_menu");
		}
	});

	
	$(document).on('click','.btn-remove',function(){
		//Sweet Alert for delete action
		swal({
			title: alert_title,
			text: alert_message,
			icon: "warning",
			buttons: true,
			dangerMode: true,
		  })
		  .then((willDelete) => {
			if (willDelete) {
				$(this).closest('form').submit();
			} else {
				return false;
			} 
		});
		
		return false;
	});
	
	$(".select2").select2(); 

	$(".datepicker").datepicker();
	
	$(".monthpicker").datepicker( {
		format: "mm/yyyy",
		viewMode: "months", 
		minViewMode: "months"
	});	

	$('.dropify').dropify();
	
	$('.datetimepicker').datetimepicker({
		format:'YYYY-MM-DD HH:mm:00'
	});
	
	$('.timepicker').datetimepicker({
		format:'HH:mm:00'
	});

	//Form validation
	validate();	

    /*Summernote editor*/
	if ($("#summernote,.summernote").length) {
		$('#summernote,.summernote').summernote({
			height: 200,
			dialogsInBody: true
		});
	}	
	
	$(".float-field").keypress(function(event) {
	   if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
			(event.which < 48 || event.which > 57)) { event.preventDefault();
		}
	});	

	$(".int-field").keypress(function(event) {
		if ((event.which < 48 || event.which > 57)) { event.preventDefault();
		}
	});	
	
	$(document).on('click','#modal-fullscreen',function(){
		$("#main_modal >.modal-dialog").toggleClass("fullscreen-modal");
	});
	
	//Mask Plugin
	$('.year').mask('0000-0000');
	
	$(".form-horizontal input:required, select:required, textarea:required").parent().prev().append("<span class='required'> *</span>");
	
	$("input:required, select:required, textarea:required").prev().append("<span class='required'> *</span>");
	
	$(document).on("click",".off-canvas-sidebar li a",function(){
		if ( $(this).has("ul")) {
			if(typeof $(this).parent().attr("class") == 'undefined'){
			   $(this).next().slideToggle(200);
			}
		}
	});
	
	//Print Command
	$(document).on('click','.print',function(){
		$("#preloader").css("display","block");
		var div = "#"+$(this).data("print");	
		$(div).print({
			timeout: 1000,
		});		
	});
	
	//Appsvan File Upload Field
	$(".appsvan-file").after("<input type='text' class='form-control filename' readOnly>"
	+"<button type='button' class='btn btn-info appsvan-upload-btn'>Browse</button>");
    
	$(".appsvan-file").each(function(){
		if($(this).data("value")){
			$(this).parent().find(".filename").val($(this).data("value"));
		}
		if($(this).attr("required")){
			$(this).parent().find(".filename").prop("required",true);
		}
	});
	
	$(document).on("click",".appsvan-upload-btn",function(){
		$(this).parent().find("input[type=file]").click();
	});
	
	$(document).on('change','.appsvan-file',function(){
		readFileURL(this);
	});

	function readFileURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {};
	
			$(input).parent().find(".filename").val(input.files[0].name);
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	//Ajax Modal Function
	$(document).on("click",".ajax-modal",function(){
		 var link = $(this).attr("href");
		 var title = $(this).data("title");
		 var fullscreen = $(this).data("fullscreen");
		 $.ajax({
			 url: link,
			 beforeSend: function(){
				$("#preloader").css("display","block"); 
			 },success: function(data){
				$("#preloader").css("display","none");
				$('#main_modal .modal-title').html(title);
				$('#main_modal .modal-body').html(data);
				$("#main_modal .alert-success").css("display","none");
				$("#main_modal .alert-danger").css("display","none");
				$('#main_modal').modal('show'); 
				
				if(fullscreen ==true){
					$("#main_modal >.modal-dialog").addClass("fullscreen-modal");
				}else{
					$("#main_modal >.modal-dialog").removeClass("fullscreen-modal");
				}
				
				//init Essention jQuery Library
				$("select.select2").select2();
				$('.year').mask('0000-0000');
				$(".ajax-submit").validate();
				$(".datepicker").datepicker();	
				$(".dropify").dropify();
				$("input:required, select:required, textarea:required").prev().append("<span class='required'> *</span>");
			    if ($("#summernote,.summernote").length) {
					$('#summernote,.summernote').summernote({
						height: 200,
						dialogsInBody: true
					});
				}	
			 },
			  error: function (request, status, error) {
				console.log(request.responseText);
			  }
		 });
		 
		 return false;
	 }); 
	 
	 $("#main_modal").on('show.bs.modal', function () {
         $('#main_modal').css("overflow-y","hidden"); 		
     });
	 
	 $("#main_modal").on('shown.bs.modal', function () {
		  setTimeout(function(){
		  $('#main_modal').css("overflow-y","auto");
		}, 1000);	
     });
	 
	 
	 //Ajax Modal Submit
	 $(document).on("submit",".ajax-submit",function(){			 
		 var link = $(this).attr("action");
		 $.ajax({
			 method: "POST",
			 url: link,
			 data:  new FormData(this),
			 mimeType:"multipart/form-data",
			 contentType: false,
			 cache: false,
			 processData:false,
			 beforeSend: function(){
				$("#preloader").css("display","block");  
			 },success: function(data){
				$("#preloader").css("display","none"); 
				var json = JSON.parse(data);
				if(json['result'] == "success"){
					$("#main_modal .alert-success").html(json['message']);
					$("#main_modal .alert-success").css("display","block");
					$("#main_modal .alert-danger").css("display","none");
					
					window.setTimeout(function(){window.location.reload()}, 500);

				}else{
					if(Array.isArray(json['message'])){
						jQuery.each( json['message'], function( i, val ) {
						   $("#main_modal .alert-danger").html("<p>"+val+"</p>");
						});
						$("#main_modal .alert-success").css("display","none");
						$("#main_modal .alert-danger").css("display","block");
					}else{
						$("#main_modal .alert-danger").html("<p>"+json['message']+"</p>");
						$("#main_modal .alert-success").css("display","none");
						$("#main_modal .alert-danger").css("display","block");
					}
				}
			 },
			  error: function (request, status, error) {
				console.log(request.responseText);
			  }
		 });

		 return false;
	 });
	 
	 //Ajax submit with validate
	 $(".appsvan-submit-validate").validate({
		 submitHandler: function(form) {
			 var elem = $(form);
			 $(elem).find("button[type=submit]").prop("disabled",true);
			 var link = $(form).attr("action");
			 $.ajax({
				 method: "POST",
				 url: link,
				 data:  new FormData(form),
				 mimeType:"multipart/form-data",
				 contentType: false,
				 cache: false,
				 processData:false,
				 beforeSend: function(){
				   button_val = $(elem).find("button[type=submit]").text();
				   $(elem).find("button[type=submit]").html('<i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i>');
				 
				 },success: function(data){
					$(elem).find("button[type=submit]").html(button_val);
					$(elem).find("button[type=submit]").attr("disabled",false);				
					var json = JSON.parse(data);
					if(json['result'] == "success"){
						Command: toastr["success"](json['message']);
					}else{
						if(Array.isArray(json['message'])){
							jQuery.each( json['message'], function( i, val ) {
							   Command: toastr["error"](val);
							});
						}else{
							Command: toastr["error"](json['message']);
						}
					}
				 }
			 });

			return false; 
		},invalidHandler: function(form, validator) {},
		  errorPlacement: function(error, element) {}
	 });
	 
	 //Ajax submit without validate
	 $(document).on("submit",".appsvan-submit",function(){		 
		 var elem = $(this);
		 $(elem).find("button[type=submit]").prop("disabled",true);
		 var link = $(this).attr("action");
		 $.ajax({
			 method: "POST",
			 url: link,
			 data:  new FormData(this),
			 mimeType:"multipart/form-data",
			 contentType: false,
			 cache: false,
			 processData:false,
			 beforeSend: function(){
			   button_val = $(elem).find("button[type=submit]").text();
			   $(elem).find("button[type=submit]").html('<i class="fa fa-circle-o-notch fa-spin" aria-hidden="true"></i>');
			 
			 },success: function(data){
				$(elem).find("button[type=submit]").html(button_val);
				$(elem).find("button[type=submit]").attr("disabled",false);				
				var json = JSON.parse(data);
				if(json['result'] == "success"){
					Command: toastr["success"](json['message']);
				}else{
					if(Array.isArray(json['message'])){
						jQuery.each( json['message'], function( i, val ) {
						   Command: toastr["error"](val);
						});
					}else{
						Command: toastr["error"](json['message']);
					}
					
				}
			 }
		 });

		 return false;
	 });
	 
	//Hide Empty Menu 
	$("#menu li").each(function(){
		var elem = $(this);
		if($(elem).has("ul").length>0){
			if($(elem).find("ul").has("li").length === 0){
				$(elem).remove();
			}		
		}
	}); 
	   

});


function validate(){
	//Validation Form
	$(".validate").validate({
		submitHandler: function(form) {
			form.submit();
		},invalidHandler: function(form, validator) {},
		  errorPlacement: function(error, element) {}
	});
}
