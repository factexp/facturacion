<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::group(['middleware' => ['install']], function () {	

	Auth::routes();
	Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

	Route::group(['middleware' => ['auth']], function () {
		
		Route::get('/dashboard', 'DashboardController@index');
		
		//Profile Controller
		Route::get('profile/my_profile', 'ProfileController@my_profile');
		Route::get('profile/edit', 'ProfileController@edit');
		Route::post('profile/update', 'ProfileController@update');
		Route::get('profile/change_password', 'ProfileController@change_password');
		Route::post('profile/update_password', 'ProfileController@update_password');
		
		//Extend Membertship
		Route::get('membership/extend', 'MembershipController@extend');
		
		//Payment Gateway PayPal
		Route::post('membership/pay','MembershipController@pay');	
		Route::get('membership/paypal/{action?}','MembershipController@paypal');		
		
		//Payment Gateway Stripe
		Route::post('membership/stripe_payment/{payment_id}','MembershipController@stripe_payment');	

		
		Route::group(['middleware' => ['admin']], function () {
			//User Controller
			Route::resource('users','UserController');
			Route::get('members/payment_history','UserController@payment_history');
			
			//Language Controller
			Route::resource('languages','LanguageController');	
			
			//Utility Controller
			Route::match(['get', 'post'],'administration/general_settings/{store?}', 'UtilityController@settings');
			Route::post('administration/upload_logo', 'UtilityController@upload_logo');
			Route::get('administration/backup_database', 'UtilityController@backup_database');
			
			//Email Template
			Route::resource('email_templates','EmailTemplateController');
			
		});
		
		Route::group(['middleware' => ['company']], function () {

			//Contact Group
			Route::resource('contact_groups','ContactGroupController');
		     
			//Contact Controller
			Route::get('contacts/get_table_data','ContactController@get_table_data');
			Route::post('contacts/send_email/{id}','ContactController@send_email')->name('contacts.send_email');
			Route::resource('contacts','ContactController');

			//Account Controller
			Route::resource('accounts','AccountController');
			

			//Income Controller
			Route::get('income/get_table_data','IncomeController@get_table_data');
			Route::get('income/calendar','IncomeController@calendar')->name('income.income_calendar');
			Route::resource('income','IncomeController');
			
			
			//Expense Controller
			Route::get('expense/get_table_data','ExpenseController@get_table_data');
			Route::get('expense/calendar','ExpenseController@calendar')->name('expense.expense_calendar');
			Route::resource('expense','ExpenseController');
			
			//Transfer Controller
			Route::get('transfer/create', 'TransferController@create')->name('transfer.create');
			Route::post('transfer/store', 'TransferController@store')->name('transfer.store');
			
			//Repeating Income
			Route::get('repeating_income/get_table_data','RepeatingIncomeController@get_table_data');
			Route::resource('repeating_income','RepeatingIncomeController');
			
			//Repeating Expense
			Route::get('repeating_expense/get_table_data','RepeatingExpenseController@get_table_data');
			Route::resource('repeating_expense','RepeatingExpenseController');

			//Chart Of Accounts
			Route::resource('chart_of_accounts','ChartOfAccountController');

			//Payment Method
			Route::resource('payment_methods','PaymentMethodController');
					
			//Supplier Controller
			Route::resource('suppliers','SupplierController');

			//Product Controller
			Route::get('products/get_product/{id}','ProductController@get_product');
			Route::resource('products','ProductController');

			//Product Controller
			Route::resource('services','ServiceController');

			//Purchase Order
			Route::get('purchase_orders/create_payment/{id}','PurchaseController@create_payment')->name('purchase_orders.create_payment');
			Route::post('purchase_orders/store_payment','PurchaseController@store_payment')->name('purchase_orders.create_payment');
			Route::get('purchase_orders/view_payment/{id}','PurchaseController@view_payment')->name('purchase_orders.view_payment');
			Route::get('purchase_orders/download_pdf/{id}','PurchaseController@download_pdf')->name('purchase_orders.download_pdf');
			Route::resource('purchase_orders','PurchaseController');

			//Purchase Return
			Route::resource('purchase_returns','PurchaseReturnController');
			
			//Sales Return
			Route::resource('sales_returns','SalesReturnController');
			
			
			//Invoice Controller
			Route::get('invoices/download_pdf/{id}','InvoiceController@download_pdf')->name('invoices.download_pdf');
			Route::get('invoices/create_payment/{id}','InvoiceController@create_payment')->name('invoices.create_payment');
			Route::post('invoices/store_payment','InvoiceController@store_payment')->name('invoices.create_payment');
			Route::get('invoices/view_payment/{id}','InvoiceController@view_payment')->name('invoices.view_payment');
			Route::get('invoices/create_email/{invoice_id}','InvoiceController@create_email')->name('invoices.send_email');
			Route::post('invoices/send_email','InvoiceController@send_email')->name('invoices.send_email');
			Route::resource('invoices','InvoiceController');

			//Quotation Controller
			Route::get('quotations/download_pdf/{id}','QuotationController@download_pdf')->name('quotations.download_pdf');
			Route::get('quotations/convert_invoice/{quotation_id}','QuotationController@convert_invoice')->name('quotations.convert_invoice');
			Route::get('quotations/create_email/{quotation_id}','QuotationController@create_email')->name('quotations.send_email');
			Route::post('quotations/send_email','QuotationController@send_email')->name('quotations.send_email');
			Route::resource('quotations','QuotationController');

			//Staff Controller
			Route::resource('staffs','StaffController');
			
			//Company Settings Controller
			Route::post('company/upload_logo', 'CompanySettingsController@upload_logo')->name('company.change_logo');
			Route::match(['get', 'post'],'company/general_settings/{store?}', 'CompanySettingsController@settings')->name('company.change_settings');
			
			//Company Email Template
			Route::get('company_email_template/get_template/{id}','CompanyEmailTemplateController@get_template');
			Route::resource('company_email_template','CompanyEmailTemplateController');
			
			//Tax Controller
			Route::resource('taxs','TaxController');
			
			//Product Unit Controller
			Route::resource('product_units','ProductUnitController');
			
			//Permission Controller
			Route::get('permission/control/{user_id?}', 'PermissionController@index')->name('permission.manage');
			Route::post('permission/store', 'PermissionController@store')->name('permission.manage');

			
			//Report Controller
			Route::match(['get', 'post'],'reports/account_statement/{view?}', 'ReportController@account_statement')->name('reports.account_statement');
			Route::match(['get', 'post'],'reports/day_wise_income/{view?}', 'ReportController@day_wise_income')->name('reports.day_wise_income');
			Route::match(['get', 'post'],'reports/date_wise_income/{view?}', 'ReportController@date_wise_income')->name('reports.date_wise_income');
			Route::match(['get', 'post'],'reports/day_wise_expense/{view?}', 'ReportController@day_wise_expense')->name('reports.day_wise_expense');
			Route::match(['get', 'post'],'reports/date_wise_expense/{view?}', 'ReportController@date_wise_expense')->name('reports.date_wise_expense');
			Route::match(['get', 'post'],'reports/transfer_report/{view?}', 'ReportController@transfer_report')->name('reports.transfer_report');
			Route::match(['get', 'post'],'reports/income_vs_expense/{view?}', 'ReportController@income_vs_expense')->name('reports.income_vs_expense');
			Route::match(['get', 'post'],'reports/report_by_payer/{view?}', 'ReportController@report_by_payer')->name('reports.report_by_payer');
			Route::match(['get', 'post'],'reports/report_by_payee/{view?}', 'ReportController@report_by_payee')->name('reports.report_by_payee');

		});
		
		Route::group(['middleware' => ['client']], function () {
		    Route::get('client/invoices/{status?}','ClientController@invoices');
		    Route::get('client/view_invoice/{id}','ClientController@view_invoice');
		    Route::post('client/make_payment/{id}','ClientController@make_payment');
		    Route::get('client/quotations','ClientController@quotations');
		    Route::get('client/view_quotation/{id}','ClientController@view_quotation');
		    Route::get('client/transactions','ClientController@transactions');
			Route::get('client/view_transaction/{id}','ClientController@view_transaction');
		    
			//PayPal Payment gateway
			Route::get('client/paypal/{action?}','ClientController@paypal');	
		});
		
	});
	
});

Route::get('/installation', 'Install\InstallController@index');
Route::get('install/database', 'Install\InstallController@database');
Route::post('install/process_install', 'Install\InstallController@process_install');
Route::get('install/create_user', 'Install\InstallController@create_user');
Route::post('install/store_user', 'Install\InstallController@store_user');
Route::get('install/system_settings', 'Install\InstallController@system_settings');
Route::post('install/finish', 'Install\InstallController@final_touch');		

//JSON data for dashboard chart
Route::get('dashboard/json_month_wise_income_expense','DashboardController@json_month_wise_income_expense')->middleware('auth');
Route::get('dashboard/json_income_vs_expense','DashboardController@json_income_vs_expense')->middleware('auth');


Route::post('membership/paypal_ipn','MembershipController@paypal_ipn');	
Route::post('client/paypal_ipn','ClientController@paypal_ipn');
Route::get('console/run','CronJobsController@run');	
