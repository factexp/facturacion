@extends('layouts.app')

@section('content')
<style>
  .btn{margin-bottom: 2px !important;}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="panel-title">{{ _lang('Detail Expense Report') }}
			</span>
			
			</div>

			<div class="panel-body">
			
				<div class="report-params">
					<form class="validate" method="post" action="{{ url('reports/day_wise_expense/view') }}">
						<div class="col-md-12">
              				{{ csrf_field() }}

						    <div class="col-md-4">
								<div class="form-group">
									<label class="control-label">{{ _lang('From') }}</label>						
									<div class="input-group date">
										<input type="text" class="form-control datepicker" name="date1" id="date1" value="{{ isset($date1) ? $date1 : old('date1') }}" readOnly="true" required>   
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
								<label class="control-label">{{ _lang('To') }}</label>
                                <div class="input-group date">
									<input type="text" class="form-control datepicker" name="date2" id="date2" value="{{ isset($date2) ? $date2 : old('date2') }}" readOnly="true" required>   
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>								
								</div>
							</div>
							
							<div class="col-md-4">
								<button type="submit" class="btn btn-primary btn-sm btn-block" style="margin-top:24px; padding: 8px 25px;">{{ _lang('View Report') }}</button>
							</div>
						</form>

					</div>
				</div><!--End Report param-->
                
				<div class="report-header">
				   <h4>{{ _lang('Detail Expense Report') }}</h4>
				   <h5>{{ isset($date1) ? date('d M, Y',strtotime($date1)).' '._lang('to').' '.date('d M, Y',strtotime($date2)) : '-------------  '._lang('to').'  -------------' }}</h5>
				</div>

				<table class="table table-bordered report-table">
					<thead>
						<th>{{ _lang('Date') }}</th>
						<th>{{ _lang('Expense Type') }}</th>
						<th>{{ _lang('Note') }}</th>
						<th>{{ _lang('Account') }}</th>    
						<th class="text-right">{{ _lang('Amount') }}</th>       
					</thead>
					<tbody>
					 
					@if(isset($report_data))
						@php $currency = currency(); @endphp
			
					   @foreach($report_data as $report) 
						<tr>
						   <td>{{ date('d M, Y',strtotime($report->trans_date)) }}</td>
						   <td>{{ $report->expense_type }}</td>
						   <td>{{ $report->note }}</td>
						   <td>{{ $report->account }}</td>
						   <td class="text-right">{{ $currency." ".decimalPlace($report->amount)  }}</td>
						</tr>
					    @endforeach
					@endif
				    </tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js-script')
<script>
	document.title = $(".panel-title").html();
</script>
@endsection


