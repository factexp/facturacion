@extends('layouts.app')

@section('content')
<style>
  .btn{margin-bottom: 2px !important;}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="panel-title">{{ _lang('Income VS Expense Report') }}
			</span>
			
			</div>

			<div class="panel-body">
			
				<div class="report-params">
					<form class="validate" method="post" action="{{ url('reports/income_vs_expense/view') }}">
						<div class="col-md-12">
              				{{ csrf_field() }}

						    <div class="col-md-4">
								<div class="form-group">
									<label class="control-label">{{ _lang('From') }}</label>						
									<div class="input-group date">
										<input type="text" class="form-control datepicker" name="date1" id="date1" value="{{ isset($date1) ? $date1 : old('date1') }}" readOnly="true" required>   
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
								<label class="control-label">{{ _lang('To') }}</label>
                                <div class="input-group date">
									<input type="text" class="form-control datepicker" name="date2" id="date2" value="{{ isset($date2) ? $date2 : old('date2') }}" readOnly="true" required>   
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>								
								</div>
							</div>
							
							<div class="col-md-4">
								<button type="submit" class="btn btn-primary btn-sm btn-block" style="margin-top:24px; padding: 8px 25px;">{{ _lang('View Report') }}</button>
							</div>
						</form>

					</div>
				</div><!--End Report param-->
                
				<div class="report-header">
				   <h4>{{ _lang('Income VS Expense Report') }}</h4>
				   <h5>{{ isset($date1) ? date('d M, Y',strtotime($date1)).' '._lang('to').' '.date('d M, Y',strtotime($date2)) : '-------------  '._lang('to').'  -------------' }}</h5>
				</div>

				<table class="table table-bordered report-table">
					<thead>
						<th>{{ _lang('Income Date') }}</th>
						<th>{{ _lang('Income Type') }}</th>
						<th class="text-right">{{ _lang('Amount') }}</th>
						<th>{{ _lang('Expense Date') }}</th>
						<th>{{ _lang('Expense Type') }}</th>
						<th class="text-right">{{ _lang('Amount') }}</th>
					</thead>
					<tbody>
					 
					@if(isset($report_data))
						@php 
							$currency = currency();
							$income_total = 0;
							$expense_total = 0;					
						@endphp
			
					   @foreach($report_data as $report) 
						 <tr>
							 <td>{{ $report->income_date }}</td>
							 <td>{{ $report->income_type }}</td>
							 <td class="text-right">{{ $currency." ".decimalPlace($report->income_amount) }}</td>
							 <td>{{ $report->expense_date }}</td>
							 <td>{{ $report->expense_type }}</td>
							 <td class="text-right">{{ $currency." ".decimalPlace($report->expense_amount) }}</td>
						   </tr>
						   
							@php
							  $income_total += $report->income_amount;
							  $expense_total += $report->expense_amount;
							@endphp
					    @endforeach
						 <tr>
							 <td></td>
							 <td>{{ _lang('Total Income') }}</td>
							 <td class="text-right">{{ $currency." ".decimalPlace($income_total) }}</td>
							 <td></td>
							 <td>{{ _lang('Total Expense') }}</td>
							 <td class="text-right">{{ $currency." ".decimalPlace($expense_total) }}</td>
						 </tr>
						
					@endif
				    </tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js-script')
<script>
	document.title = $(".panel-title").html();
</script>
@endsection


