<li>
   <a href="#" aria-expanded="false"><i class="fas fa-users"></i>&nbsp;<span class="menu-title">{{ _lang('User Management') }}</span> <span class="glyphicon arrow"></span></a>
   <ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('users') }}">{{ _lang('All User') }}</a></li>
	  <li><a href="{{ url('users/create') }}">{{ _lang('Add New') }}</a></li>
   </ul>
</li>

<li><a href="{{ url('members/payment_history') }}" aria-expanded="false"><i class="far fa-credit-card"></i>&nbsp;<span class="menu-title">{{ _lang('Payment History') }}</span></a></li>

<li>
   <a href="#" aria-expanded="false"><i class="fas fa-cogs"></i>&nbsp;<span class="menu-title">{{ _lang('Administration') }}</span> <span class="glyphicon arrow"></span></a>
   <ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('administration/general_settings') }}">{{ _lang('General Settings') }}</a></li>
	  <li><a href="{{ url('email_templates') }}">{{ _lang('Email Template') }}</a></li>
	  <li><a href="{{ url('languages') }}">{{ _lang('Language List') }}</a></li>
	  <li><a href="{{ url('administration/backup_database') }}">{{ _lang('Database Backup') }}</a></li>
   </ul>
</li>