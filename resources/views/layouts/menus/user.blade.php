<li>
	<a href="#" aria-expanded="false"><i class="fas fa-address-book"></i>&nbsp;<span class="menu-title">{{ _lang('Contacts') }}</span> <span class="glyphicon arrow"></span></a>
	<ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('contacts') }}">{{ _lang('Contacts List') }}</a></li>
	  <li><a href="{{ url('contacts/create') }}">{{ _lang('Add New') }}</a></li>
	  <li><a href="{{ url('contact_groups') }}">{{ _lang('Contact Group') }}</a></li>
	</ul>
</li>

<li>
	<a href="#" aria-expanded="false"><i class="fas fa-shopping-bag"></i>&nbsp;<span class="menu-title">{{ _lang('Products') }}</span> <span class="glyphicon arrow"></span></a>
	<ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('products/create') }}">{{ _lang('Add New') }}</a></li>
	  <li><a href="{{ url('products') }}">{{ _lang('Product List') }}</a></li>
	</ul>
</li>
<li>
	<a href="#" aria-expanded="false"><i class="fab fa-servicestack"></i>&nbsp;<span class="menu-title">{{ _lang('Service') }}</span> <span class="glyphicon arrow"></span></a>
	<ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('services') }}">{{ _lang('Service List') }}</a></li>
	  <li><a href="{{ url('services/create') }}">{{ _lang('Add New') }}</a></li>
	</ul>
</li>
<li>
	<a href="#" aria-expanded="false"><i class="fas fa-parachute-box"></i>&nbsp;<span class="menu-title">{{ _lang('Supplier') }}</span> <span class="glyphicon arrow"></span></a>
	<ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('suppliers') }}">{{ _lang('Supplier List') }}</a></li>
	  <li><a href="{{ url('suppliers/create') }}">{{ _lang('Add New') }}</a></li>
	</ul>
</li>
<li>
	<a href="#" aria-expanded="false"><i class="fas fa-shopping-cart"></i>&nbsp;<span class="menu-title">{{ _lang('Purchase') }}</span> <span class="glyphicon arrow"></span></a>
	<ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('purchase_orders') }}">{{ _lang('Purchase Orders') }}</a></li>
	  <li><a href="{{ url('purchase_orders/create') }}">{{ _lang('Create Purchase Order') }}</a></li>
	</ul>
</li>

<li>
	<a href="#" aria-expanded="false"><i class="fas fa-reply"></i>&nbsp;<span class="menu-title">{{ _lang('Return') }}</span> <span class="glyphicon arrow"></span></a>
	<ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('purchase_returns') }}">{{ _lang('Purchase Return') }}</a></li>
	  <li><a href="{{ url('sales_returns') }}">{{ _lang('Sales Return') }}</a></li>
	</ul>
</li>

<li>
	<a href="#" aria-expanded="false"><i class="fas fa-cart-plus"></i>&nbsp;<span class="menu-title">{{ _lang('Sales') }}</span> <span class="glyphicon arrow"></span></a>
	<ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('invoices/create') }}">{{ _lang('Add Invoice') }}</a></li>
	  <li><a href="{{ url('invoices') }}">{{ _lang('Invoice List') }}</a></li>
	  <li><a href="{{ url('quotations/create') }}">{{ _lang('Add Quotation') }}</a></li>
	  <li><a href="{{ url('quotations') }}">{{ _lang('Quotation List') }}</a></li>
	</ul>
</li>

<li>
   <a href="#" aria-expanded="false"><i class="fas fa-university"></i>&nbsp;<span class="menu-title">{{ _lang('Bank & Cash Accounts') }}</span> <span class="glyphicon arrow"></span></a>
   <ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('accounts') }}">{{ _lang('List Account') }}</a></li>
	  <li><a href="{{ url('accounts/create') }}">{{ _lang('Add New Account') }}</a></li>
   </ul>
</li>

<li>
   <a href="#" aria-expanded="false"><i class="far fa-credit-card"></i>&nbsp;<span class="menu-title">{{ _lang('Transactions') }}</span> <span class="glyphicon arrow"></span></a>
   <ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('income') }}">{{ _lang('Income/Deposit') }}</a></li>
	  <li><a href="{{ url('expense') }}">{{ _lang('Expense') }}</a></li>
	  <li><a href="{{ url('transfer/create') }}">{{ _lang('Transfer') }}</a></li>
	  <li><a href="{{ url('income/calendar') }}">{{ _lang('Income Calendar') }}</a></li>
	  <li><a href="{{ url('expense/calendar') }}">{{ _lang('Expense Calendar') }}</a></li>
   </ul>
</li>

<li>
   <a href="#" aria-expanded="false"><i class="fas fa-credit-card"></i>&nbsp;<span class="menu-title">{{ _lang('Recurring Transaction') }}</span> <span class="glyphicon arrow"></span></a>
   <ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('repeating_income/create') }}">{{ _lang('Add Repeating Income') }}</a></li>
	  <li><a href="{{ url('repeating_income') }}">{{ _lang('Repeating Income List') }}</a></li>
	  <li><a href="{{ url('repeating_expense/create') }}">{{ _lang('Add Repeating Expense') }}</a></li>
	  <li><a href="{{ url('repeating_expense') }}">{{ _lang('Repeating Expense List') }}</a></li>
   </ul>
</li>


<li>
	<a href="#" aria-expanded="false"><i class="fas fa-user-tie"></i>&nbsp;<span class="menu-title">{{ _lang('Staffs') }}</span> <span class="glyphicon arrow"></span></a>
	<ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('staffs') }}">{{ _lang('All Staff') }}</a></li>
	  <li><a href="{{ url('staffs/create') }}">{{ _lang('Add New') }}</a></li>
	</ul>
</li>

<li>
   <a href="#" aria-expanded="false"><i class="far fa-chart-bar"></i>&nbsp;<span class="menu-title">{{ _lang('Reports') }}</span> <span class="glyphicon arrow"></span></a>
   <ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('reports/account_statement') }}">{{ _lang('Account Statement') }}</a></li>
	  <li><a href="{{ url('reports/day_wise_income') }}">{{ _lang('Detail Income Report') }}</a></li>
	  <li><a href="{{ url('reports/date_wise_income') }}">{{ _lang('Date Wise Income') }}</a></li>
	  <li><a href="{{ url('reports/day_wise_expense') }}">{{ _lang('Detail Expense Report') }}</a></li>
	  <li><a href="{{ url('reports/date_wise_expense') }}">{{ _lang('Date Wise Expense') }}</a></li>
	  <li><a href="{{ url('reports/transfer_report') }}">{{ _lang('Transfer Report') }}</a></li>
	  <li><a href="{{ url('reports/income_vs_expense') }}">{{ _lang('Income VS Expense') }}</a></li>
	  <li><a href="{{ url('reports/report_by_payer') }}">{{ _lang('Report By Payer') }}</a></li>
	  <li><a href="{{ url('reports/report_by_payee') }}">{{ _lang('Report By Payee') }}</a></li>
   </ul>
</li>

<li>
   <a href="#" aria-expanded="false"><i class="fas fa-cog"></i>&nbsp;<span class="menu-title">{{ _lang('Settings') }}</span> <span class="glyphicon arrow"></span></a>
   <ul aria-expanded="false" class="collapse">
	  <li><a href="{{ url('company/general_settings') }}">{{ _lang('Company Settings') }}</a></li>
	  <li><a href="{{ url('company_email_template') }}">{{ _lang('Email Template') }}</a></li>
	  <li><a href="{{ url('permission/control') }}">{{ _lang('Access Control') }}</a></li>
	  <li><a href="{{ url('chart_of_accounts') }}">{{ _lang('Income & Expense Types') }}</a></li>
	  <li><a href="{{ url('payment_methods') }}">{{ _lang('Payment Methods') }}</a></li>
	  <li><a href="{{ url('product_units') }}">{{ _lang('Product Unit') }}</a></li>
	  <li><a href="{{ url('taxs') }}">{{ _lang('Tax Settings') }}</a></li>
   </ul>
</li>
