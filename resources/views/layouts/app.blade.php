<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ get_option('site_title','Smart Cash') }}</title>

      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700" rel="stylesheet">

	  <!-- Bootstrap -->
      <link href="{{ asset('public/css/bootstrap.css') }}" rel="stylesheet">
      <!-- Include roboto.css to use the Roboto web font, material.css to include the theme and ripples.css to style the ripple effect -->
      <link href="{{ asset('public/css/roboto.css') }}" rel="stylesheet">
      <link href="{{ asset('public/css/material.css') }}" rel="stylesheet">
      <link href="{{ asset('public/css/ripples.css') }}" rel="stylesheet">
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	  <link href="{{ asset('public/css/metisMenu.css') }}" rel="stylesheet">
	  <link href="{{ asset('public/css/datatables.css') }}" rel="stylesheet">
      <link href="{{ asset('public/css/select2.css') }}" rel="stylesheet">
	  <link href="{{ asset('public/css/toastr.css') }}" rel="stylesheet">
	  <link href="{{ asset('public/css/dropify.min.css') }}" rel="stylesheet">
	  <link href="{{ asset('public/css/material-fonts.css') }}" rel="stylesheet">
	  <link href="{{ asset('public/css/fullcalendar.min.css') }}" rel="stylesheet">
	  <link href="{{ asset('public/css/summernote.css') }}" rel="stylesheet">
	  <link href="{{ asset('public/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
      <link href="{{ asset('public/css/bootstrap-datepicker.css') }}" rel="stylesheet">
	  <link href="{{ asset('public/css/style.css?v=3.1') }}" rel="stylesheet">
      @if(get_company_option('backend_direction',get_option('backend_direction')) == "rtl")
	    <link href="{{ asset('public/css/RTL.css') }}" rel="stylesheet" />
	  @endif
	  <script type="text/javascript">
	   var direction = "{{ get_option('backend_direction') }}";
	   var _url = "{{ url('') }}";
	   var alert_title = "{{ _lang('Are you sure?') }}";
	   var alert_message = "{{ _lang('Once deleted, you will not be able to recover this information !') }}";
	  </script>
   </head>
   <body>
	 <!-- Main Modal -->
	 <div id="main_modal" class="modal animated bounceInDown" role="dialog">
	  <div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="modal-btn btn btn-danger btn-sm pull-right" data-dismiss="modal"><i class="glyphicon glyphicon-remove-circle"></i> {{ _lang('Exit') }}</button>
			<button type="button" id="modal-fullscreen" class="modal-btn btn btn-primary btn-sm pull-right"><i class="glyphicon glyphicon-fullscreen"></i> {{ _lang('Full Screen') }}</button>
			<h5 class="modal-title"></h5>
		  </div>  
		  <div class="alert alert-danger" style="display:none; margin: 15px;"></div>
		  <div class="alert alert-success" style="display:none; margin: 15px;"></div>			  
		  <div class="modal-body" style="overflow:hidden;"></div>
		</div>

	  </div>
	 </div>
	
	 <div id="preloader">
		<div class="bar"></div>
	 </div>
	 
	 <!-- Start Main Wrapper -->  
	 <div id="main-wrapper">
		<!-- Start Header --> 
		<header class="admin-head">
		   <div class="left-header">
			  <span id="system-name">{{ get_company_option('company_name',get_option('company_name')) }}</span>
			  <div class="mini-nav">
				 <div class="togglebutton">
					<label>
					<input type="checkbox" checked>
					</label>
				 </div>
				 <!--<i class="mdi-image-dehaze"></i>-->
			  </div>
		   </div>
		   <div class="right-header">
			  <div class="navbar custom-navbar navbar-right">
				 <ul class="nav navbar-nav">
					<li class="dropdown">
						<a href="#" class="top-right-menu dropdown-toggle" data-toggle="dropdown">
							<i class="mdi-maps-beenhere"></i>
							<span>{{ _lang('Hi').", ".Auth::user()->name }}</span>
							<b class="caret"></b>
						</a>
							<ul class="dropdown-menu">
								@if(Auth::user()->user_type == 'user' && get_option('membership_system') == 'enabled')
									<li><a href="{{ url('membership/extend') }}">{{ _lang('Extend Membership') }}</a></li>
								@endif
								<li><a href="{{ url('profile/my_profile') }}">{{ _lang('Profile') }}</a></li>
								<li><a href="{{ url('profile/edit') }}">{{ _lang('Update Profile') }}</a></li>
								<li><a href="{{ url('profile/change_password') }}">{{ _lang('Change Password') }}</a></li>
								<li>
									<a class="dropdown-item" href="{{ route('logout') }}"
										onclick="event.preventDefault();
										document.getElementById('logout-form').submit();">
										{{ _lang('Logout') }}
									</a>
								</li>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>
						</ul>
					</li>
				 </ul>
			  </div>
		   </div>
		</header>
		<!-- End Header -->
		<!-- Start Content Area Section -->
		<section class="content-area">
		   <!-- Start Sidebar -->
		   <aside class="sidebar">
			  <!--User Profile -->
			  <div class="widget stay-on-collapse" id="widget-welcomebox">
				 <div class="widget-body welcome-box tabular">
					<div class="tabular-row">
					   <div class="tabular-cell welcome-avatar">
						  <a href="#"><img src="{{ Auth::user()->profile_picture != "" ? asset('public/uploads/profile/'.Auth::user()->profile_picture) :  asset('public/images/avatar.png') }}" class="avatar"></a>
					   </div>
					   <div class="tabular-cell welcome-options">
						  <span class="welcome-text">{{ _lang('Welcome') }},</span>
						  <a href="#" class="name">{{ Auth::user()->name }}</a>
					   </div>
					</div>
				 </div>
			  </div>
			  <div class="border-bottom"></div>
			  <!-- End User Profile -->
			  <nav class="sidebar-nav">
				 <ul class="metismenu" id="menu">
					<li><a href="{{ url('dashboard') }}" aria-expanded="false"><i class="fas fa-tachometer-alt"></i>&nbsp;<span class="menu-title">{{ _lang('Dashboard') }}</span></a></li>
					<li>
					   <a href="#" aria-expanded="false"><i class="fas fa-user-alt"></i>&nbsp;<span class="menu-title">{{ _lang('Profile') }}</span> <span class="glyphicon arrow"></span></a>
					   <ul aria-expanded="false" class="collapse">
						  <li><a href="{{ url('profile/my_profile') }}">{{ _lang('View Profile') }}</a></li>
						  <li><a href="{{ url('profile/edit') }}">{{ _lang('Update Profile') }}</a></li>
						  <li><a href="{{ url('profile/change_password') }}">{{ _lang('Change Password') }}</a></li>
						  <li><a href="{{ url('/logout') }}">{{ _lang('Logout') }}</a></li>
					   </ul>
					</li>
					
					@include('layouts.menus.'.Auth::user()->user_type)
					
				 </ul>
			  </nav>
		   </aside>
		   <!-- End Sidebar -->
		   <!--Start Content -->
			 <section class="content"> 
				<div class="page-title"><h2></h2></div>
				<div class="col-md-12" id="content-area">
                    @if(get_option('membership_system') == 'enabled' && Auth::user()->user_type == 'user')
						@if( Auth::User()->valid_to < date('Y-m-d'))
							<div class="alert alert-danger">
							   <b>{{ _lang('Your membership has expired. Please renew your membership !') }}</b>
							</div>
						@endif
					@endif
					
					@yield('content')
				</div>
			 </section>
		   <!-- End Content -->
		</section>
		<!-- End Content Area Section -->
	 </div>
	 <!-- End Main Wrapper -->
	 <!-- END Site -->

	 <script src="{{ asset('public/js/jquery.min.js') }}"></script>
	 <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
	 <script src="{{ asset('public/js/ripples.min.js') }}"></script>
	 <script src="{{ asset('public/js/echarts.min.js') }}"></script>
	 <script src="{{ asset('public/js/material.min.js') }}"></script>
	 <script src="{{ asset('public/js/metisMenu.min.js') }}"></script>
	 <script src="{{ asset('public/js/datatables.min.js') }}"></script>
	 <script src="{{ asset('public/js/jquery.validate.min.js') }}"></script>
	 <script src="{{ asset('public/js/nicescroll.js') }}"></script>
	 <script src="{{ asset('public/js/moment.min.js') }}"></script>
	 <script src="{{ asset('public/js/bootstrap-datepicker.js') }}"></script>
	 <script src="{{ asset('public/js/bootstrap-datetimepicker.min.js') }}"></script>
	 <script src="{{ asset('public/js/select2.min.js') }}"></script>
	 <script src="{{ asset('public/js/jquery.mask.min.js') }}"></script>
	 <script src="{{ asset('public/js/summernote.js') }}"></script>
	 <script src="{{ asset('public/js/dropify.min.js') }}"></script>
	 <script src="{{ asset('public/js/toastr.js') }}"></script>
	 <script src="{{ asset('public/js/fullcalendar.min.js') }}"></script>
	 <script src="{{ asset('public/js/gauge.min.js') }}"></script>
	 <script src="{{ asset('public/js/print.js') }}"></script>
	 <script src="{{ asset('public/js/sweetalert.min.js') }}"></script>
	 <script src="{{ asset('public/js/app.js?v=3.1') }}"></script>
	 
	 <!-- JS -->
	@yield('js-script')
	 <script type="text/javascript">		
		$(document).ready(function() {
			// This command is used to initialize some elements and make them work properly
			$.material.init();
				
			@if(Request::is('dashboard'))
				$(".page-title>h2").html("{{ _lang('Dashboard') }}"); 
			@else
				$(".page-title>h2").html($(".title").html()); 
				$(".page-title>h2").html($(".panel-title").html());
			@endif			
			
			$(".data-table").DataTable({
				responsive: true,
				"bAutoWidth":false,
				"ordering": false,
				"language": {
				   "decimal":        "",
				   "emptyTable":     "{{ _lang('No Data Found') }}",
				   "info":           "{{ _lang('Showing') }} _START_ {{ _lang('to') }} _END_ {{ _lang('of') }} _TOTAL_ {{ _lang('Entries') }}",
				   "infoEmpty":      "{{ _lang('Showing 0 To 0 Of 0 Entries') }}",
				   "infoFiltered":   "(filtered from _MAX_ total entries)",
				   "infoPostFix":    "",
				   "thousands":      ",",
				   "lengthMenu":     "{{ _lang('Show') }} _MENU_ {{ _lang('Entries') }}",
				   "loadingRecords": "{{ _lang('Loading...') }}",
				   "processing":     "{{ _lang('Processing...') }}",
				   "search":         "{{ _lang('Search') }}",
				   "zeroRecords":    "{{ _lang('No matching records found') }}",
				   "paginate": {
					  "first":      "{{ _lang('First') }}",
					  "last":       "{{ _lang('Last') }}",
					  "next":       "{{ _lang('Next') }}",
					  "previous":   "{{ _lang('Previous') }}"
				  },
				  "aria": {
					  "sortAscending":  ": activate to sort column ascending",
					  "sortDescending": ": activate to sort column descending"
				  }
			  },
			  dom: 'Blfrtip',
			  buttons: [
			  'copy', 'csv', 'excel', 'pdf', 'print'
			  ],
			});
			
			$(".report-table").DataTable({
				responsive: true,
				"bAutoWidth":false,
				"ordering": false,
				"language": {
				   "decimal":        "",
				   "emptyTable":     "{{ _lang('No Data Found') }}",
				   "info":           "{{ _lang('Showing') }} _START_ {{ _lang('to') }} _END_ {{ _lang('of') }} _TOTAL_ {{ _lang('Entries') }}",
				   "infoEmpty":      "{{ _lang('Showing 0 To 0 Of 0 Entries') }}",
				   "infoFiltered":   "(filtered from _MAX_ total entries)",
				   "infoPostFix":    "",
				   "thousands":      ",",
				   "lengthMenu":     "{{ _lang('Show') }} _MENU_ {{ _lang('Entries') }}",
				   "loadingRecords": "{{ _lang('Loading...') }}",
				   "processing":     "{{ _lang('Processing...') }}",
				   "search":         "{{ _lang('Search') }}",
				   "zeroRecords":    "{{ _lang('No matching records found') }}",
				   "paginate": {
					  "first":      "{{ _lang('First') }}",
					  "last":       "{{ _lang('Last') }}",
					  "next":       "{{ _lang('Next') }}",
					  "previous":   "{{ _lang('Previous') }}"
				  },
				  "aria": {
					  "sortAscending":  ": activate to sort column ascending",
					  "sortDescending": ": activate to sort column descending"
				  }
			  },
			  dom: 'Blfrtip',
			  buttons: [
			  'copy', 'csv', 'excel','pdf',
			  {
					extend: 'print',
					title: '',
					customize: function ( win ) {
						$(win.document.body)
							.css( 'font-size', '10pt' )
							.prepend(
								'<div style="text-align:center">'+
								$(".report-header").html()+
								'</div>'
							);
	 
						$(win.document.body).find( 'table' )
							.addClass( 'compact' )
							.css( 'font-size', 'inherit' );
							 
					}
				}
			  ],
			});
			
			//Show Success Message
			@if(Session::has('success'))
			   Command: toastr["success"]("{{session('success')}}")
			@endif
			
			//Show Single Error Message
			@if(Session::has('error'))
			   Command: toastr["error"]("{{session('error')}}")
			@endif
			
			
			@php $i =0; @endphp

			@foreach ($errors->all() as $error)
				Command: toastr["error"]("{{ $error }}");
				
				var name= "{{$errors->keys()[$i] }}";
				
				$("input[name='"+name+"']").addClass('error');
				$("select[name='"+name+"'] + span").addClass('error');
				
				$("input[name='"+name+"'], select[name='"+name+"']").parent().append("<span class='v-error'>{{$error}}</span>");
				
				@php $i++; @endphp
			
			@endforeach
			
		});
	 </script>
   </body>
</html>