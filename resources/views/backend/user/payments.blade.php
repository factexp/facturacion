@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('Payment History') }}</span></div>

			<div class="panel-body">
			  <table class="table table-bordered data-table">
				<thead>
				  <tr>
					<th>{{ _lang('Date') }}</th>
					<th>{{ _lang('Name') }}</th>
					<th>{{ _lang('Email') }}</th>
					<th>{{ _lang('Details') }}</th>
					<th>{{ _lang('Method') }}</th>
					<th class="text-right">{{ _lang('Amount') }}</th>
				  </tr>
				</thead>
				<tbody>
				  @php $currency = get_option('currency','USD'); @endphp
				  @foreach($payment_history as $history)
					<tr>
						<td>{{ date('d M, Y',strtotime($history->created_at)) }}</td>
						<td>{{ isset($history->user->name)? $history->user->name : '' }}</td>
						<td>{{ isset($history->user->email) ? $history->user->email : '' }}</td>
						<td>{{ $history->title }}</td>
						<td>{{ $history->method }}</td>					
						<td class="text-right">{{ $currency.' '.$history->amount }}</td>					
					</tr>
				  @endforeach
				</tbody>
			  </table>
			  
			  <div class="pull-right">
				 {{ $payment_history->links() }}
			  </div>
			</div>
		</div>
	</div>
</div>

@endsection


