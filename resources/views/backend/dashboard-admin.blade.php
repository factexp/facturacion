@extends('layouts.app')

@section('content')

<!-- Cart Box -->
<div class="row">
    <div class="col-md-6">
	 <div class="cart-box-purpel">
		<div class="middle-content">
		    <i class="fas fa-users"></i>
			<h4 class="admin-widget-label">
			{{ _lang('Total User') }}
			<h4>
			<h2 class="admin-widget-content">{{ $total_user }}</h2>
		</div>
	 </div>
  </div>
  <div class="col-md-6">
	 <div class="cart-box-red">
	    <div class="middle-content">
		    <i class="fas fa-money-check-alt"></i>
			<h4 class="admin-widget-label">
			{{ _lang('Total Payment') }}
			<h4>
			<h2 class="admin-widget-content">{{ get_option('currency','USD').' '.decimalPlace($total_payment) }}</h2>
	    </div>
	 </div>
  </div>
</div>
<!-- End Cart-Box -->


<div class="row">
  <!-- Panel 1 -->
  <div class="col-md-6 admin-panel">
	 <div class="panel panel-default dashboard-panel">
		<div class="panel-heading">{{ _lang('New Users') }}</div>
		
		<div class="panel-body">
		   <table class="table table-bordered">
			<thead>
			  <tr>
				<th>{{ _lang('Name') }}</th>
				<th>{{ _lang('Email') }}</th>
				<th class="text-center">{{ _lang('Details') }}</th>
			  </tr>
			</thead>
			<tbody>
			  
			  @foreach($news_users as $user)
			    <tr id="row_{{ $user->id }}">
					<td class='name'>{{ $user->name }}</td>
					<td class='email'>{{ $user->email }}</td>			
					<td class="text-center">
					  <a href="{{action('UserController@show', $user['id'])}}" data-title="{{ $user->name }}" class="btn btn-info btn-xs ajax-modal">{{ _lang('View') }}</a>
					</td>
			    </tr>
			  @endforeach
			</tbody>
		  </table>
		</div>
	 </div>
  </div>
  <!-- End Panel 1 -->
  
  <!-- Panel 2 -->
  <div class="col-md-6 admin-panel">
	 <div class="panel panel-default dashboard-panel">
		<div class="panel-heading">{{ _lang('Recent Payments') }}</div>
		
		<div class="panel-body">
		    <table class="table table-bordered">
				<thead>
				  <tr>
					<th>{{ _lang('Date') }}</th>
					<th>{{ _lang('Name') }}</th>
					<th>{{ _lang('Method') }}</th>
					<th class="text-right">{{ _lang('Amount') }}</th>
				  </tr>
				</thead>
				<tbody>
				  @php $currency = get_option('currency','USD'); @endphp
				  @foreach($recent_payment as $history)
					<tr>
						<td>{{ date('d M, Y',strtotime($history->created_at)) }}</td>
						<td>{{ isset($history->user->name)? $history->user->name : '' }}</td>
						<td>{{ $history->method }}</td>					
						<td class="text-right"><b>{{ $currency.' '.$history->amount }}</b></td>					
					</tr>
				  @endforeach
				</tbody>
			  </table>
		</div>
	 </div>
  </div>
  <!-- End Panel 2 -->
</div>

@endsection
