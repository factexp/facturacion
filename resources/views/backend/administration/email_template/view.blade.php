@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading panel-title">{{ _lang('View Email Template') }}</div>

	<div class="panel-body">
	  <table class="table table-bordered">
		<tr><td>{{ $emailtemplate->name }}</td></tr>
		<tr><td>{{ $emailtemplate->subject }}</td></tr>
		<tr><td>{!! $emailtemplate->body !!}</td></tr>		
	  </table>
	</div>
  </div>
 </div>
</div>
@endsection


