@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('Email Templates') }}</span></div>

			<div class="panel-body">
			<table class="table table-bordered data-table">
			<thead>
			  <tr>
				<th>{{ _lang('Name') }}</th>
				<th>{{ _lang('Subject') }}</th>
				<th>{{ _lang('Action') }}</th>
			  </tr>
			</thead>
			<tbody>
			  
			  @foreach($emailtemplates as $emailtemplate)
			  <tr id="row_{{ $emailtemplate->id }}">
				<td class='name'>{{ $emailtemplate->name }}</td>
				<td class='subject'>{{ $emailtemplate->subject }}</td>
				<td>
					<a href="{{action('EmailTemplateController@edit', $emailtemplate['id'])}}" class="btn btn-warning btn-sm">{{ _lang('Edit') }}</a>
					<a href="{{action('EmailTemplateController@show', $emailtemplate['id'])}}" class="btn btn-info btn-sm">{{ _lang('View') }}</a>
				</td>

			  </tr>
			  @endforeach
			</tbody>
		  </table>
			</div>
		</div>
	</div>
</div>

@endsection


