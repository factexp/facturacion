@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
		  <ul class="nav nav-tabs setting-tab">
			  <li class="active"><a data-toggle="tab" href="#general" aria-expanded="true">{{ _lang('General') }}</a></li>
			  <li class=""><a data-toggle="tab" href="#email" aria-expanded="false">{{ _lang('Email') }}</a></li>
			  <li class=""><a data-toggle="tab" href="#membership_settings" aria-expanded="false">{{ _lang('Membership Settings') }}</a></li>
			  <li class=""><a data-toggle="tab" href="#payment_gateway" aria-expanded="false">{{ _lang('Payment Gateway') }}</a></li>
			  <li class=""><a data-toggle="tab" href="#cron_jobs" aria-expanded="false">{{ _lang('Cron Jobs') }}</a></li>
			  <li class=""><a data-toggle="tab" href="#logo" aria-expanded="false">{{ _lang('Logo') }}</a></li>
		  </ul>
		  <div class="tab-content">
				
			  <div id="general" class="tab-pane fade in active">
				  <div class="panel panel-default">
				  <div class="panel-heading"><span class="panel-title">{{ _lang('General Settings') }}</span></div>

				  <div class="panel-body">
					  <form method="post" class="appsvan-submit params-panel" autocomplete="off" action="{{ url('administration/general_settings/update') }}" enctype="multipart/form-data">
						{{ csrf_field() }}
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Company Name') }}</label>						
							<input type="text" class="form-control" name="company_name" value="{{ get_option('company_name') }}" required>
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Site Title') }}</label>						
							<input type="text" class="form-control" name="site_title" value="{{ get_option('site_title') }}" required>
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Phone') }}</label>						
							<input type="text" class="form-control" name="phone" value="{{ get_option('phone') }}" required>
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Email') }}</label>						
							<input type="text" class="form-control" name="email" value="{{ get_option('email') }}" required>
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Timezone') }}</label>						
							<select class="form-control select2" name="timezone" required>
							<option value="">{{ _lang('-- Select One --') }}</option>
							{{ create_timezone_option(get_option('timezone')) }}
							</select>
						  </div>
						</div>
												
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Language') }}</label>						
							<select class="form-control select2" name="language" required>
							    <option value="">{{ _lang('-- Select One --') }}</option>
								{{ load_language( get_option('language') ) }}
							</select>
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Theme Direction') }}</label>						
							<select class="form-control" name="backend_direction" required>
								<option value="ltr" {{ get_option('backend_direction') == 'ltr' ? 'selected' : '' }}>{{ _lang('LTR') }}</option>
								<option value="rtl" {{ get_option('backend_direction') == 'rtl' ? 'selected' : '' }}>{{ _lang('RTL') }}</option>
							</select>
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Allow Sign Up') }}</label>						
							<select class="form-control" name="allow_singup" required>
								<option value="yes" {{ get_option('allow_singup') == 'yes' ? 'selected' : '' }}>{{ _lang('Enable') }}</option>
								<option value="no" {{ get_option('allow_singup') == 'no' ? 'selected' : '' }}>{{ _lang('Disable') }}</option>
							</select>
						  </div>
						</div>

						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Address') }}</label>						
							<textarea class="form-control" name="address" required>{{ get_option('address') }}</textarea>
						  </div>
						</div>

							
						<div class="form-group">
						  <div class="col-md-12">
							<button type="submit" class="btn btn-primary">{{ _lang('Save Settings') }}</button>
						  </div>
						</div>
					  </form>
				  </div>
				  </div>
			  </div>
			 
			
			  <div id="email" class="tab-pane fade">
				<div class="panel panel-default">
				  <div class="panel-heading"><span class="panel-title">{{ _lang('Email Settings') }}</span></div>
				  <div class="panel-body">
					<form method="post" class="appsvan-submit params-panel" autocomplete="off" action="{{ url('administration/general_settings/update') }}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Mail Type') }}</label>						
							<select class="form-control niceselect wide" name="mail_type" id="mail_type" required>
							  <option value="mail" {{ get_option('mail_type')=="mail" ? "selected" : "" }}>{{ _lang('PHP Mail') }}</option>
							  <option value="smtp" {{ get_option('mail_type')=="smtp" ? "selected" : "" }}>{{ _lang('SMTP') }}</option>
							</select>
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('From Email') }}</label>						
							<input type="text" class="form-control" name="from_email" value="{{ get_option('from_email') }}" required>
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('From Name') }}</label>						
							<input type="text" class="form-control" name="from_name" value="{{ get_option('from_name') }}" required>
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('SMTP Host') }}</label>						
							<input type="text" class="form-control smtp" name="smtp_host" value="{{ get_option('smtp_host') }}">
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('SMTP Port') }}</label>						
							<input type="text" class="form-control smtp" name="smtp_port" value="{{ get_option('smtp_port') }}">
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('SMTP Username') }}</label>						
							<input type="text" class="form-control smtp" autocomplete="off" name="smtp_username" value="{{ get_option('smtp_username') }}">
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('SMTP Password') }}</label>						
							<input type="password" class="form-control smtp" autocomplete="off" name="smtp_password" value="{{ get_option('smtp_password') }}">
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('SMTP Encryption') }}</label>						
							<select class="form-control smtp" name="smtp_encryption">
							   <option value="ssl" {{ get_option('smtp_encryption')=="ssl" ? "selected" : "" }}>{{ _lang('SSL') }}</option>
							   <option value="tls" {{ get_option('smtp_encryption')=="tls" ? "selected" : "" }}>{{ _lang('TLS') }}</option>
							</select>
						  </div>
						</div>
						
						<div class="form-group">
						  <div class="col-md-12">
							<button type="submit" class="btn btn-primary">{{ _lang('Save Settings') }}</button>
						  </div>
						</div>		
					</form>
				   </div>
				 </div>
			  </div>
			  
			  <div id="membership_settings" class="tab-pane fade">
			     <div class="panel panel-default">
				  <div class="panel-heading"><span class="panel-title">{{ _lang('Membership Settings') }}</span></div>
				    <div class="panel-body">
					   <form method="post" class="appsvan-submit params-panel" autocomplete="off" action="{{ url('administration/general_settings/update') }}" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="col-md-6">
							  <div class="form-group">
								<label class="control-label">{{ _lang('Membership System') }}</label>						
								<select class="form-control" name="membership_system" required>
									<option value="enabled" {{ get_option('membership_system') == 'enabled' ? 'selected' : '' }}>{{ _lang('Enable') }}</option>
									<option value="disabled" {{ get_option('membership_system') == 'disabled' ? 'selected' : '' }}>{{ _lang('Disable') }}</option>
								</select>
							  </div>
							</div>
							
							<div class="col-md-6">
							  <div class="form-group">
								<label class="control-label">{{ _lang('Trial Period') }}</label>						
								<select class="form-control" name="trial_period" required>
									@for($i=0; $i<31; $i ++)
										<option value="{{ $i }}" {{ get_option('trial_period') == $i ? 'selected' : '' }}>{{ $i.' '._lang('days') }}</option>
									@endfor
								</select>
							  </div>
							</div>
									
									
							<div class="col-md-6">
							  <div class="form-group">
								<label class="control-label">{{ _lang('Currency').' ('._lang('PayPal Supported Currency').')' }}</label>						
								<select class="form-control select2" name="currency" id="currency" required>
									<option value="USD">{{ _lang('U.S. Dollar') }}</option>
									<option value="AUD">{{ _lang('Australian Dollar') }}</option>
									<option value="BRL">{{ _lang('Brazilian Real') }}</option>
									<option value="CAD">{{ _lang('Canadian Dollar') }}</option>
									<option value="CZK">{{ _lang('Czech Koruna') }}</option>
									<option value="DKK">{{ _lang('Danish Krone') }}</option>
									<option value="EUR">{{ _lang('Euro') }}</option>
									<option value="HKD">{{ _lang('Hong Kong Dollar') }}</option>
									<option value="HUF">{{ _lang('Hungarian Forint') }}</option>
									<option value="INR">{{ _lang('Indian Rupee') }}</option>
									<option value="ILS">{{ _lang('Israeli New Sheqel') }}</option>
									<option value="JPY">{{ _lang('Japanese Yen') }}</option>
									<option value="MYR">{{ _lang('Malaysian Ringgit') }}</option>
									<option value="MXN">{{ _lang('Mexican Peso') }}</option>
									<option value="NOK">{{ _lang('Norwegian Krone') }}</option>
									<option value="NZD">{{ _lang('New Zealand Dollar') }}</option>
									<option value="PHP">{{ _lang('Philippine Peso') }}</option>
									<option value="PLN">{{ _lang('Polish Zloty') }}</option>
									<option value="GBP">{{ _lang('Pound Sterling') }}</option>
									<option value="SGD">{{ _lang('Singapore Dollar') }}</option>
									<option value="SEK">{{ _lang('Swedish Krona') }}</option>
									<option value="CHF">{{ _lang('Swiss Franc') }}</option>
									<option value="TWD">{{ _lang('Taiwan New Dollar') }}</option>
									<option value="THB">{{ _lang('Thai Baht') }}</option>
									<option value="TRY">{{ _lang('Turkish Lira') }}</option>
								</select>
							  </div>
							</div>
							
							<div class="col-md-6">
							  <div class="form-group">
								<label class="control-label">{{ _lang('Monthly Cost') }}</label>						
								<input type="text" class="form-control float-field" name="monthly_cost" value="{{ get_option('monthly_cost') }}" required>
							  </div>
							</div>
							
							<div class="col-md-6">
							  <div class="form-group">
								<label class="control-label">{{ _lang('Yearly Cost') }}</label>						
								<input type="text" class="form-control float-field" name="yearly_cost" value="{{ get_option('yearly_cost') }}" required>
							  </div>
							</div>
							

							<div class="form-group">
							  <div class="col-md-12">
								<button type="submit" class="btn btn-primary">{{ _lang('Save Settings') }}</button>
							  </div>
							</div>		
						</form>
				    </div>
				 </div>
			  </div>
			  
			   <div id="payment_gateway" class="tab-pane fade">
			     <div class="panel panel-default">
				  <div class="panel-heading"><span class="panel-title">{{ _lang('Payment Gateway') }}</span></div>
				    <div class="panel-body">
					   <form method="post" class="appsvan-submit params-panel" autocomplete="off" action="{{ url('administration/general_settings/update') }}" enctype="multipart/form-data">
							{{ csrf_field() }}
							
							<label>{{ _lang('PayPal') }}</label>
							<div class="params-panel">
								<div class="col-md-6">
								  <div class="form-group">
									<label class="control-label">{{ _lang('PayPal Active') }}</label>						
									<select class="form-control" name="paypal_active" required>
									   <option value="Yes" {{ get_option('paypal_active') == 'Yes' ? 'selected' : '' }}>{{ _lang('Yes') }}</option>
									   <option value="No" {{ get_option('paypal_active') == 'No' ? 'selected' : '' }}>{{ _lang('No') }}</option>
									</select>
								  </div>
								</div>
								
								<div class="col-md-6">
								  <div class="form-group">
									<label class="control-label">{{ _lang('PayPal Email') }}</label>						
									<input type="text" class="form-control" name="paypal_email" value="{{ get_option('paypal_email') }}">
								  </div>
								</div>
								
							</div>
							
							<br>
							<label>{{ _lang('Stripe') }}</label>
							<div class="params-panel">
								<div class="col-md-6">
								  <div class="form-group">
									<label class="control-label">{{ _lang('Stripe Active') }}</label>						
									<select class="form-control" name="stripe_active" required>
									   <option value="Yes" {{ get_option('stripe_active') == 'Yes' ? 'selected' : '' }}>{{ _lang('Yes') }}</option>
									   <option value="No" {{ get_option('stripe_active') == 'No' ? 'selected' : '' }}>{{ _lang('No') }}</option>
									</select>
								  </div>
								</div>
								
								<div class="col-md-6">
								  <div class="form-group">
									<label class="control-label">{{ _lang('Secret Key') }}</label>						
									<input type="text" class="form-control" name="stripe_secret_key" value="{{ get_option('stripe_secret_key') }}">
								  </div>
								</div>
								
								<div class="col-md-6">
								  <div class="form-group">
									<label class="control-label">{{ _lang('Publishable Key') }}</label>						
									<input type="text" class="form-control" name="stripe_publishable_key" value="{{ get_option('stripe_publishable_key') }}">
								  </div>
								</div>
							
							</div>

							<br>
							<div class="form-group">
							  <div class="col-md-12">
								<button type="submit" class="btn btn-primary pull-right">{{ _lang('Save Settings') }}</button>
							  </div>
							</div>		
						</form>
				    </div>
				 </div>
			  </div>
			  
			  <div id="cron_jobs" class="tab-pane fade">
			     <div class="panel panel-default">
				  <div class="panel-heading"><span class="panel-title">{{ _lang('Cron Jobs') }}</span></div>
				    <div class="panel-body">
					   <form method="post" class="appsvan-submit params-panel" autocomplete="off" action="{{ url('administration/general_settings/update') }}">				         
							{{ csrf_field() }}
							
							<div class="col-md-12">
							  <div class="form-group">
								<label class="control-label">{{ _lang('Cron Jobs URL') }}</label>						
								<input type="text" class="form-control" value="{{ url('console/run') }}" readOnly>
							  </div>
							</div>
							
								
							
					   </form>	
				   </div>
				 </div>
			  </div>
			  
			  <div id="logo" class="tab-pane fade">
			     <div class="panel panel-default">
				  <div class="panel-heading"><span class="panel-title">{{ _lang('Logo Upload') }}</span></div>
				    <div class="panel-body">
					   <form method="post" class="appsvan-submit params-panel" autocomplete="off" action="{{ url('administration/upload_logo') }}" enctype="multipart/form-data">				         
							
							{{ csrf_field() }}
							
							<div class="col-md-6 col-md-offset-3">
							  <div class="form-group">
								<label class="control-label">{{ _lang('Upload Logo') }}</label>						
								<input type="file" class="form-control dropify" name="logo" data-max-file-size="8M" data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" data-default-file="{{ get_logo() }}" required>
							  </div>
							</div>
							
							<br>
							<div class="form-group">
							  <div class="col-md-4 col-md-offset-4">
								<button type="submit" class="btn btn-primary btn-block">{{ _lang('Upload') }}</button>
							  </div>
							</div>	
							
					   </form>	
				   </div>
				 </div>
			  </div>
			  
		   </div>  
		</div>
	  </div>
     </div>
    </div>
@endsection

@section('js-script')
<script type="text/javascript">
if($("#mail_type").val() != "smtp"){
	$(".smtp").prop("disabled",true);
}
$(document).on("change","#mail_type",function(){
	if( $(this).val() != "smtp" ){
		$(".smtp").prop("disabled",true);
	}else{
		$(".smtp").prop("disabled",false);
	}
});

</script>
@stop

