@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-12">
	    <div class="btn-group pull-right">
	        @if(get_company_option('stripe_active') == 'yes' && $invoice->status != 'Paid')
			<form id="stripe-invoice" action="{{ url('client/make_payment/'.$invoice->id) }}" method="POST">
				{{ csrf_field() }}
				<script
					src="https://checkout.stripe.com/checkout.js" class="stripe-button"
					data-key="{{ get_company_option('stripe_publishable_key') }}"
					data-amount="{{ round($invoice->grand_total-$invoice->paid) * 100 }}"
					data-name="{{ _lang('Invoice Payment') }}"
					data-description=""
					data-currency="{{ get_company_option('stripe_currency') }}"
					data-image="{{ get_company_logo() }}"
					data-locale="auto">
				</script>
				<a class="btn btn-info btn-cs print" href="#" data-print="invoice-view"><i class="fas fa-print"></i> {{ _lang('Print') }}</a>
			</form>
			@else 
			   <a class="btn btn-info btn-cs print" href="#" data-print="invoice-view"><i class="fas fa-print"></i> {{ _lang('Print') }}</a>
			@endif
	   </div>
	   
	   <div class="btn-group pull-right">
		    @if(get_company_option('paypal_active') == 'Yes' && $invoice->status != 'Paid')
				<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
				<!--<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">-->
			    
					<input type="hidden" name="cmd" value="_xclick">
					<input type="hidden" name="business" value="{{ get_company_option('paypal_email') }}">
					<input type="hidden" name="item_name" value="{{ _lang('Invoice Payment') }}">
					<input type="hidden" name="item_number" value="{{ $invoice->invoice_number }}">
					<input type="hidden" name="amount" value="{{ ($invoice->grand_total-$invoice->paid) }}">
					<input type="hidden" name="no_shipping" value="0">
					<input type="hidden" name="custom" value="{{ $invoice->id }}">
					<input type="hidden" name="no_note" value="1">
					<input type="hidden" name="currency_code" value="{{ get_company_option('paypal_currency') }}">
					<input type="hidden" name="lc" value="US">
					<input type="hidden" name="bn" value="PP-BuyNowBF">
					
					<input type="hidden" name="return" value="{{ url('client/paypal/return') }}"/>
					<input type="hidden" name="cancel_return" value="{{ url('client/paypal/cancel') }}" />
					<input type="hidden" name="notify_url" value="{{ url('client/paypal_ipn') }}" />
					
					<button type="submit" name="submit" class="btn btn-primary btn-paypal" alt="PayPal - The safer, easier way to pay online.">
					  <span style="display: block; min-height: 30px;"><i class="fab fa-paypal"></i> {{ _lang('Pay Via PayPal') }}</span>
					</button>
				</form> 
			@endif
		</div>
	<div class="panel panel-default clear">
	<div class="panel-heading hidden">
	   <span class="panel-title">{{ _lang('View Invoice') }}</span>
	</div>

	<div class="panel-body">
		<div class="invoice-box" id="invoice-view">
		    <div class="col-md-12">
				<table cellpadding="0" cellspacing="0">
					<tbody>
						 <tr class="top">
							<td colspan="2">
								<table>
									<tbody>
										 <tr>
											<td>
												 <b>{{ _lang('Invoice') }} #:  {{ $invoice->invoice_number }}</b><br>
												 {{ _lang('Created') }}: <b>{{ date('F d, Y',strtotime( $invoice->invoice_date)) }}</b><br>
												 {{ _lang('Due Date') }}: <b>{{ date('F d, Y',strtotime( $invoice->due_date)) }}</b>							
												 <div class="invoice-status {{ strtolower($invoice->status) }}">{{ d_lang(str_replace('_',' ',$invoice->status)) }}</div>
											</td>
											<td class="invoice-logo">
												 <img src="{{ get_company_logo() }}" style="width:100px;">
											</td>
										 </tr>
									</tbody>
								</table>
							</td>
						 </tr>
						 <tr class="information">
							<td colspan="2">
								<div class="row">
									
									<div class="invoice-col-6 mt-2">
										 <h4><b>{{ _lang('Invoice To') }}</b></h4>
										 @if(isset($invoice->client))	
											 {{ $invoice->client->contact_name }}<br>
											 {{ $invoice->client->contact_email }}<br>
											 {{ $invoice->client->company_name }}<br>
											 {{ $invoice->client->address }}<br>
										 @endif                        
									</div>
									
									<!--Company Address-->
									<div class="invoice-col-6 mt-2">
										<div class="d-inline-block float-md-right"> 
											<h4><b>{{ _lang('Company Details') }}</b></h4>
											{{ get_company_option('company_name') }}<br>
											{{ get_company_option('address') }}<br>
											{{ get_company_option('email') }}<br>

											<!--Invoice Payment Information-->
											<h4>{{ _lang('Invoice Total') }}: &nbsp;<b>{{ currency() }}</b> {{ decimalPlace($invoice->grand_total) }}</h4>
										</div>
									</div>
		
								</div>
							</td>
						</tr>
					</tbody>
				 </table>
			 </div>
			 <!--End Invoice Information-->
			 @php $currency = currency(); @endphp
			 <!--Invoice Product-->
			 <div class="col-md-12">
			    <div class="table-responsive">
					<table class="table table-bordered mt-2">
						 <thead style="background:#dce9f9;">
							 <tr>
								 <th>{{ _lang('Name') }}</th>
								 <th class="text-center" style="width:100px">{{ _lang('Quantity') }}</th>
								 <th class="text-right">{{ _lang('Unit Cost') }}</th>
								 <th class="text-right" style="width:100px">{{ _lang('Discount') }}</th>
								 <th class="no-print">{{ _lang('Tax method') }}</th>
								 <th class="text-right">{{ _lang('Tax') }}</th>
								 <th class="text-right">{{ _lang('Sub Total') }}</th>
							 </tr>
						 </thead>
						 <tbody  id="invoice">
							 @foreach($invoice->invoice_items as $item)
								 <tr id="product-{{ $item->item_id }}">
									 <td>{{ $item->item->item_name }}</td>
									 <td class="text-center">{{ $item->quantity }}</td>
									 <td class="text-right">{{ $currency.' '.$item->unit_cost }}</td>
									 <td class="text-right">{{ $currency.' '.$item->discount }}</td>
									 <td class="no-print">{{ isset($item->item->product) ? strtoupper($item->item->product->tax_method) : strtoupper($item->item->service->tax_method)  }}</td>
									 <td class="text-right">{{ $currency.' '.$item->tax_amount }}</td>
									 <td class="text-right">{{ $currency.' '.$item->sub_total }}</td>
								 </tr>
							 @endforeach
						 </tbody>
					</table>
				</div>	
			 </div>
			 <!--End Invoice Product-->	
			 <!--Summary Table-->
			 <div class="col-md-3 pull-right">
				<table class="table table-bordered" width="100%" style="background:#dce9f9">
					 <tbody>
							<tr>
								 <td>{{ _lang('Tax') }}</td>
								 <td class="text-right">{{ $currency }} {{ $invoice->tax_total }}</td>
							</tr>
							<tr>
								 <td>{{ _lang('Grand Total') }}</td>
								 <td class="text-right">{{ $currency }} {{ $invoice->grand_total }}</td>
							</tr>
							<tr>
								 <td>{{ _lang('Total Paid') }}</td>
								 <td class="text-right">{{ $currency }} {{ $invoice->paid }}</td>
							</tr>
							@if($invoice->status != 'Paid')
								<tr>
									 <td>{{ _lang('Amount Due') }}</td>
									 <td class="text-right">{{ $currency }} {{ decimalPlace($invoice->grand_total-$invoice->paid) }}</td>
								</tr>
							@endif
					 </tbody>
				</table>
			 </div>
			 <!--End Summary Table-->
			 <div class="clear"></div>
			 <!--Related Transaction-->
			 @if( ! $transactions->isEmpty() )
				<div class="col-md-12">
			        <div class="table-responsive">
						<table class="table table-bordered" style="margin-top:20px;">
							<thead style="background:#dce9f9;">
								<tr>
								   <th colspan="7" style="text-align:center">{{ _lang('Payment History') }}</td>
								</tr>
								<tr>
									<th>{{ _lang('Date') }}</th>
									<th>{{ _lang('Account') }}</th>
									<th class="text-right">{{ _lang('Amount') }}</th>
									<th>{{ _lang('Payment Method') }}</th>
								</tr>
							</thead>
							<tbody>
							  
							   @foreach($transactions as $transaction)
									<tr id="transaction-{{ $transaction->id }}">
										<td>{{ date('d/m/Y',strtotime($transaction->trans_date)) }}</td>
										<td>{{ $transaction->account->account_title }}</td>
										<td class="text-right">{{ $currency.' '.decimalPlace($transaction->amount) }}</td>
										<td>{{ $transaction->payment_method->name }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			 @endif
			 <!--END Related Transaction-->		
			 <!--Invoice Note-->
			 @if($invoice->note  != '')
				<div class="clear"></div>
				<div class="col-md-12">
					<div class="invoice-note">{{ $invoice->note }}</div>
				</div>
			 @endif
			 <!--End Invoice Note-->
		</div>
	 </div>
    </div>
 </div>
</div>
@endsection


