@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('Quotation List') }}</span>
			<a class="btn btn-primary btn-sm pull-right" href="{{route('quotations.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
				@php $currency = currency() @endphp
			  <table class="table table-bordered data-table">
				<thead>
				  <tr>
					<th>{{ _lang('Quotation Number') }}</th>
					<th>{{ _lang('Client') }}</th>
					<th>{{ _lang('Quotation Date') }}</th>
					<th class="text-right">{{ _lang('Grand Total') }}</th>
					<th class="text-center">{{ _lang('View') }}</th>
				  </tr>
				</thead>
				<tbody>
				  
				  @foreach($quotations as $quotation)
				  <tr id="row_{{ $quotation->id }}">
					<td class='quotation_number'>{{ $quotation->quotation_number }}</td>
					<td class='client_id'>{{ $quotation->client->contact_name }}</td>
					<td class='due_date'>{{ date("d/M/Y",strtotime($quotation->quotation_date)) }}</td>
					<td class='grand_total text-right'>{{ $currency." ".$quotation->grand_total }}</td>
					<td class="view text-center">
						<a class="btn btn-info btn-sm" href="{{ url('client/view_quotation/'.md5($quotation->id)) }}"><i class="fas fa-eye"></i> {{ _lang('View') }}</a>				
					</td>
				  </tr>
				  @endforeach
				</tbody>
			  </table>
			</div>
		</div>
	</div>
</div>

@endsection


