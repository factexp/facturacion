@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('Transaction List') }}</span></div>

			<div class="panel-body">
			    @php $currency = currency(); @endphp
				<table id="income-table" class="table table-bordered">
					<thead>
						<tr>
							<th>{{ _lang('Date') }}</th>
							<th>{{ _lang('Account') }}</th>
							<th>{{ _lang('Category') }}</th>
							<th class="text-right">{{ _lang('Amount') }}</th>
							<th>{{ _lang('Payment Method') }}</th>
							<th class="action-col">{{ _lang('View Details') }}</th>
						</tr>
					</thead>
					<tbody>
						@foreach($transactions as $transaction)
						 <tr>
							<td>{{ date("d/M/Y",strtotime($transaction->trans_date)) }}</td>
							<td>{{ isset($transaction->account) ? $transaction->account->account_title : '' }}</td>
							<td>{{ isset($transaction->expense_type->name) ? $transaction->expense_type->name : _lang('Transfer') }}</td>
							<td class="text-right">{{ $currency.' '.decimalPlace($transaction->amount) }}</td>
							<td>{{ isset($transaction->payment_method) ? $transaction->payment_method->name : '' }}</td>
							<td class="text-center"><a href="{{ url('client/view_transaction/'.$transaction->id) }}" data-title="{{ _lang('View Transaction Details') }}" class="btn btn-primary btn-sm ajax-modal">{{ _lang('View') }}</a></td>
						</tr>
						@endforeach
					</tbody>
			  </table>
			</div>
		</div>
	</div>
</div>

@endsection