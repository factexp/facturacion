@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
	    @if (\Session::has('paypal_success'))
		  <div class="alert alert-success">
			<p>{{ \Session::get('paypal_success') }}</p>
		  </div>
		  <br />
		@endif
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('Invoice List') }}</span></div>

			<div class="panel-body">
			  @php $currency = currency() @endphp
			  <table class="table table-bordered data-table">
				<thead>
				  <tr>
					<th>{{ _lang('Invoice Number') }}</th>
					<th>{{ _lang('Client') }}</th>
					<th>{{ _lang('Due Date') }}</th>
					<th class="text-right">{{ _lang('Grand Total') }}</th>
					<th class="text-center">{{ _lang('Status') }}</th>
					<th class="text-center">{{ _lang('View') }}</th>
				  </tr>
				</thead>
				<tbody>
				  
				  @foreach($invoices as $invoice)
				  <tr id="row_{{ $invoice->id }}">
					<td class='invoice_number'>{{ $invoice->invoice_number }}</td>
					<td class='client_id'>{{ $invoice->client->contact_name }}</td>
					<td class='due_date'>{{ date("d/M/Y",strtotime($invoice->due_date)) }}</td>
					<td class='grand_total text-right'>{{ $currency." ".$invoice->grand_total }}</td>
					<td class='status text-center'>{!! invoice_status($invoice->status) !!}</td>
					<td class='view text-center'><a class="btn btn-info btn-sm" href="{{ url('client/view_invoice/'.md5($invoice->id)) }}" data-title="{{ _lang('View Invoice') }}" data-fullscreen="true"><i class="fas fa-eye"></i> {{ _lang('View') }}</a></td>
				  </tr>
				  @endforeach
				</tbody>
			  </table>
			</div>
		</div>
	</div>
</div>

@endsection


