@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('Invoice List') }}</span>
			<a class="btn btn-primary btn-sm pull-right" href="{{route('invoices.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			  @php $currency = currency() @endphp
			  <table class="table table-bordered data-table">
				<thead>
				  <tr>
					<th>{{ _lang('Invoice Number') }}</th>
					<th>{{ _lang('Client') }}</th>
					<th>{{ _lang('Due Date') }}</th>
					<th class="text-right">{{ _lang('Grand Total') }}</th>
					<th class="text-center">{{ _lang('Status') }}</th>
					<th class="text-center">{{ _lang('Action') }}</th>
				  </tr>
				</thead>
				<tbody>
				  
				  @foreach($invoices as $invoice)
				  <tr id="row_{{ $invoice->id }}">
					<td class='invoice_number'>{{ $invoice->invoice_number }}</td>
					<td class='client_id'>{{ $invoice->client->contact_name }}</td>
					<td class='due_date'>{{ date("d/M/Y",strtotime($invoice->due_date)) }}</td>
					<td class='grand_total text-right'>{{ $currency." ".$invoice->grand_total }}</td>
					<td class='status text-center'>{!! invoice_status($invoice->status) !!}</td>
					<td class="text-center">

						<div class="dropdown">
							<button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">{{ _lang('Action') }}
							<span class="caret"></span></button>
							<ul class="dropdown-menu">
								<li><a href="{{ action('InvoiceController@edit', $invoice->id) }}"><i class="fas fa-edit"></i> {{ _lang('Edit') }}</a></li>
								<li><a href="{{ action('InvoiceController@show', $invoice->id) }}" data-title="{{ _lang('View Invoice') }}" data-fullscreen="true"><i class="fas fa-eye"></i> {{ _lang('View') }}</a></li>
								<li><a href="{{ url('invoices/create_payment/'.$invoice->id) }}" data-title="{{ _lang('Make Payment') }}" class="ajax-modal"><i class="fas fa-credit-card"></i> {{ _lang('Make Payment') }}</a></li>
								<li><a href="{{ url('invoices/view_payment/'.$invoice->id) }}" data-title="{{ _lang('View Payment') }}" data-fullscreen="true" class="ajax-modal"><i class="fas fa-credit-card"></i> {{ _lang('View Payment') }}</a></li>
								<li>
									<form action="{{action('InvoiceController@destroy', $invoice['id'])}}" method="post">									
										{{ csrf_field() }}
										<input name="_method" type="hidden" value="DELETE">
										<button class="button-link btn-remove" type="submit"><i class="fas fa-trash-alt"></i> {{ _lang('Delete') }}</button>
									</form>
								</li>
							</ul>
						</div>
					</td>
				  </tr>
				  @endforeach
				</tbody>
			  </table>
			</div>
		</div>
	</div>
</div>

@endsection


