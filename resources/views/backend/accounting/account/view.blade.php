@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading">{{ _lang('View Account') }}</div>

	<div class="panel-body">
	  <table class="table table-bordered">
		<tr><td>{{ _lang('Account Title') }}</td><td>{{ $account->account_title }}</td></tr>
		<tr><td>{{ _lang('Opening Date') }}</td><td>{{ date("d M, Y",strtotime($account->opening_date)) }}</td></tr>
		<tr><td>{{ _lang('Account Number') }}</td><td>{{ $account->account_number }}</td></tr>
		<tr><td>{{ _lang('Opening Balance') }}</td><td>{{ currency().' '.$account->opening_balance }}</td></tr>
		<tr><td>{{ _lang('Note') }}</td><td>{{ $account->note }}</td></tr>
	  </table>
	</div>
  </div>
 </div>
</div>
@endsection


