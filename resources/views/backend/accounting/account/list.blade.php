@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Account') }}</span>
			<a class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ _lang('Add Account') }}" href="{{route('accounts.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
				<table class="table table-bordered data-table">
				<thead>
				  <tr>
					<th>{{ _lang('Account Title') }}</th>
					<th>{{ _lang('Opening Date') }}</th>
					<th>{{ _lang('Account Number') }}</th>
					<th class="text-right">{{ _lang('Opening Balance') }}</th>
					<th class="action-col">{{ _lang('Action') }}</th>
				  </tr>
				</thead>
				<tbody>
				  @php $currency = currency(); @endphp
				  @foreach($accounts as $account)
				  <tr id="row_{{ $account->id }}">
					<td class='account_title'>{{ $account->account_title }}</td>
					<td class='opening_date'>{{ date("d M, Y",strtotime($account->opening_date)) }}</td>
					<td class='account_number'>{{ $account->account_number }}</td>
					<td class='opening_balance text-right'>{{ $currency." ".decimalPlace($account->opening_balance) }}</td>
					<td class="text-center">
					  <form action="{{ action('AccountController@destroy', $account->id) }}" method="post">
						<a href="{{ action('AccountController@edit', $account->id) }}" data-title="{{ _lang('Update Account') }}" class="btn btn-warning btn-sm ajax-modal">{{ _lang('Edit') }}</a>
						<a href="{{ action('AccountController@show', $account->id) }}" data-title="{{ _lang('View Account') }}" class="btn btn-info btn-sm ajax-modal">{{ _lang('View') }}</a>
						{{ csrf_field() }}
						<input name="_method" type="hidden" value="DELETE">
						<button class="btn btn-danger btn-sm btn-remove" type="submit">{{ _lang('Delete') }}</button>
					  </form>
					</td>
				  </tr>
				  @endforeach
				</tbody>
			  </table>
			</div>
		</div>
	</div>
</div>

@endsection


