@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading panel-title">{{ _lang('Update Purchase Order') }}</div>

	<div class="panel-body">
		<form method="post" class="validate" autocomplete="off" action="{{ action('PurchaseController@update', $id) }}" enctype="multipart/form-data">
			   
			{{ csrf_field()}}
			<input name="_method" type="hidden" value="PATCH">	
			
			<div class="col-md-3">
				<div class="form-group">
				<label class="control-label">{{ _lang('Order Date') }}</label>						
				<input type="text" class="form-control datepicker" name="order_date" value="{{ $purchase->order_date }}" readOnly="true" required>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				<label class="control-label">{{ _lang('Supplier') }}</label>						
				<select class="form-control select2" name="supplier_id" required>
						<option value="">{{ _lang('Select One') }}</option>
						{{ create_option("suppliers","id","supplier_name",$purchase->supplier_id,array("company_id="=>company_id())) }}
				</select>	
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
				<label class="control-label">{{ _lang('Order Status') }}</label>						
				<select class="form-control select2" name="order_status" required>
					<option value="1" {{ $purchase->order_status == '1' ? 'selected' : '' }}>{{ _lang('Ordered') }}</option>
					<option value="2" {{ $purchase->order_status == '2' ? 'selected' : '' }}>{{ _lang('Pending') }}</option>
					<option value="3" {{ $purchase->order_status == '3' ? 'selected' : '' }}>{{ _lang('Received') }}</option>
					<option value="4" {{ $purchase->order_status == '4' ? 'selected' : '' }}>{{ _lang('Canceled') }}</option>
				</select>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
				<label class="control-label">{{ _lang('Payment Status') }}</label>						
				<select class="form-control select2" name="payment_status" required>
					<option value="1" {{ $purchase->payment_status == '0' ? 'selected' : '' }}>{{ _lang('Due') }}</option>
					<option value="2" {{ $purchase->payment_status == '1' ? 'selected' : '' }}>{{ _lang('Paid') }}</option>
				</select>
				</div>
			</div>

			<div class="col-md-8">
				<div class="form-group">
				<label class="control-label">{{ _lang('Select Product') }}</label>						
				<select class="form-control select2" name="product" id="product">
						<option value="">{{ _lang('Select Product') }}</option>
						@foreach(App\Item::where("company_id",company_id())->where("item_type","product")->get() as $item)
							<option value="{{ $item->id }}">{{ $item->item_name }}</option>
						@endforeach
				</select>
				</div>
			</div>


			<div class="col-md-4">
				<div class="form-group">
				<label class="control-label">{{ _lang('Attachemnt') }}</label>						
				<input type="file" class="form-control appsvan-file" data-value="{{ $purchase->attachemnt }}" name="attachemnt">
				</div>
			</div>

			@php $currency = currency(); @endphp

			<!--Order table -->
			<div class="col-md-12">
					<div class="table-responsive">
						<table id="order-table" class="table table-bordered">
							<thead>
								<tr>
									<th>{{ _lang('Name') }}</th>
									<th class="text-center" style="width:100px">{{ _lang('Quantity') }}</th>
									<th class="text-right">{{ _lang('Net Unit Cost').' '.$currency }}</th>
									<th class="text-right" style="width:100px">{{ _lang('Discount').' '.$currency }}</th>
									<th class="text-right">{{ _lang('Tax method') }}</th>
									<th class="text-right">{{ _lang('Tax').' '.$currency }}</th>
									<th class="text-right">{{ _lang('Sub Total').' '.$currency }}</th>
									<th class="text-center">{{ _lang('Action') }}</th>
								</tr>
							</thead>
							<tbody>
								@foreach($purchase->purchase_items as $item)
									<tr id="product-{{ $item->product_id }}">
											<td>{{ $item->item->item_name }}</td>
											<td class="text-center quantity">{{ $item->quantity }}</td>
											<td class="text-right unit-cost">{{ $item->unit_cost }}</td>
											<td class="text-right discount">{{ $item->discount }}</td>
											<td class="text-right tax-method">{{ strtoupper($item->item->product->tax_method) }}</td>
											<td class="text-right tax">{{ $item->tax_amount }}</td>
											<td class="text-right sub-total">{{ $item->sub_total }}</td>
											<td class="text-center">
												<button type="button" class="btn btn-success btn-xs edit-product"><i class="fas fa-edit"></i></button>
												<button type="button" class="btn btn-danger btn-xs remove-product"><i class='fa fa-trash'></i></button>
											</td>
											<input type="hidden" name="product_id[]" value="{{ $item->product_id }}">
											<input type="hidden" name="quantity[]" class="input-quantity" value="{{ $item->quantity }}">
											<input type="hidden" name="unit_cost[]" class="input-unit-cost" value="{{ $item->unit_cost }}">
											<input type="hidden" name="discount[]" class="input-discount" value="{{ $item->discount }}">
											<input type="hidden" name="tax_amount[]" class="input-tax" value="{{ $item->tax_amount }}">
											<input type="hidden" name="tax_id[]"  value="{{ $item->tax_id }}">
											<input type="hidden" name="unit_tax[]" class="input-unit-tax" value="{{ $item->tax_amount/$item->quantity }}">
											<input type="hidden" name="sub_total[]" class="input-sub-total" value="{{ $item->sub_total }}">
									</tr>
								@endforeach
							</tbody>
							<tfoot class="tfoot active">
								<tr>
									<th>{{ _lang('Total') }}</th>
									<th class="text-center" id="total-qty">0</th>
									<th></th>
									<th class="text-right" id="total-discount">0.00</th>
									<th></th>
									<th class="text-right" id="total-tax">0.00</th>
									<th class="text-right" id="total">0.00</th>
									<th class="text-center"></th>
									<input type="hidden" name="product_total" id="product_total" value="0">
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
        		<!--End Order table -->

				<div class="col-md-4 clear">
				  <div class="form-group">
					<label class="control-label">{{ _lang('Order Tax')." ".$currency }}</label>						
					<select class="form-control select2" name="order_tax_id">
						 <option value="">{{ _lang('No Tax') }}</option>
						 @foreach(App\Tax::where("company_id",company_id())->get() as $tax)
							  <option value="{{ $tax->id }}"  {{ $purchase->order_tax_id == $tax->id ? 'selected' : ''  }}>{{ $tax->tax_name }} - {{ $tax->type =='percent' ? $tax->rate.' %' : $tax->rate }}</option>
						 @endforeach
          			</select>
				  </div>
				</div>


				<div class="col-md-4">
				  <div class="form-group">
					<label class="control-label">{{ _lang('Order Discount')." ".$currency }}</label>						
					<input type="text" class="form-control float-field" name="order_discount" value="{{ $purchase->order_discount }}">
				  </div>
				</div>

				<div class="col-md-4">
				  <div class="form-group">
					<label class="control-label">{{ _lang('Shipping Cost')." ".$currency }}</label>						
					<input type="text" class="form-control float-field" name="shipping_cost" value="{{ $purchase->shipping_cost }}">
				  </div>
				</div>


				<div class="col-md-12">
				  <div class="form-group">
					<label class="control-label">{{ _lang('Note') }}</label>						
					<textarea class="form-control" name="note">{{ old('note') }}</textarea>
				  </div>
				</div>

				
			<div class="col-md-12">
			  <div class="form-group">
					<button type="submit" class="btn btn-primary">{{ _lang('Save') }}</button>
			  </div>
			</div>
		</form>
	</div>
  </div>
 </div>
</div>
@endsection

@section('js-script')
<script>
var total_quantity = 0;	
var total_discount = 0;	
var total_tax = 0;	
var product_total = 0;	

update_summary();

$(document).on('change','#product',function(){
    	var product_id = $(this).val();
		
		if( product_id == '' ){
			return;
		}

		//if product has already in order table
		if($("#order-table > tbody > #product-"+product_id).length > 0 ){
			
				var tr = $("#order-table > tbody > #product-"+product_id);

				//Get current value
				var quantity = parseFloat($(tr).find(".quantity").html());
				var c_unit_cost = parseFloat($(tr).find(".unit-cost").html());
				var c_discount = parseFloat($(tr).find(".discount").html());
				var c_tax = parseFloat($(tr).find(".tax").html());
				var c_subtotal = parseFloat($(tr).find(".sub-total").html());

				var tax_amount = ((c_tax/quantity) * (quantity+1)).toFixed(2);
				var sub_total = ((c_subtotal/quantity)*(quantity+1)).toFixed(2);
		
				//Set new value
				$(tr).find(".quantity").html(quantity+1);
				$(tr).find(".tax").html(tax_amount);
				$(tr).find(".sub-total").html(sub_total);

				//Set value to hidden fields
				$(tr).find(".input-quantity").val(quantity+1);
				$(tr).find(".input-tax").val(tax_amount);
				$(tr).find(".input-sub-total").val(sub_total);


			update_summary();
			$("#product").val("");

			return;
		}



        //Ajax request for getting product details
		$.ajax({
       		method: "GET",
			 url: "{{ url('products/get_product') }}/"+product_id,
			 beforeSend: function(){
          $("#preloader").fadeIn(100);
			 },success: function(data){
				 $("#preloader").fadeOut(100);
         		 var json = JSON.parse(data);
				 var item = json['item'];
				 var product = json['product'];
				 var tax = json['tax'];
 
                 //Product Cost
				 var product_price = parseFloat(product['product_cost']);
				 var tax_method = product['tax_method'];

				 //Tax Value calculation
				 var unit_cost = 0.00;
				 var tax_amount = 0.00;
				 var sub_total = 0.00;

         		//if product has tax
				if(product['tax_id'] != "" && product['tax_id'] != null){

				var tax_rate = parseFloat(tax['rate']);

            	if(tax_method == "inclusive"){
							
               			if(tax['type'] == 'percent'){
								  tax_amount = product_price * tax_rate/(100+tax_rate);
									unit_cost = product_price*100/(100+tax_rate);
									sub_total = product_price;
							 }else if(tax['type'] == 'fixed'){
								  tax_amount = tax_rate;
									unit_cost = product_price - tax_rate;
									sub_total = product_price;
							 }
						}else if(tax_method == "exclusive"){
							if(tax['type'] == 'percent'){
								tax_amount = (product_price/100) * tax_rate;
							    unit_cost = product_price;
								sub_total = product_price + tax_amount;
							 }else if(tax['type'] == 'fixed'){
								  tax_amount = tax_rate;
									unit_cost = product_price;
									sub_total = product_price + tax_amount;
							 }
						}

				 }else{
					  tax_amount = 0;
						unit_cost = product_price;
						sub_total = product_price;
				 }

				 var product_row = `<tr id="product-${item['id']}">
											<td>${item['item_name']}</td>
											<td class="text-center quantity">1</td>
											<td class="text-right unit-cost">${unit_cost.toFixed(2)}</td>
											<td class="text-right discount">0.00</td>
											<td class="text-right tax-method">${tax_method.toUpperCase()}</td>
											<td class="text-right tax">${tax_amount.toFixed(2)}</td>
											<td class="text-right sub-total">${sub_total.toFixed(2)}</td>
											<td class="text-center">
												<button type="button" class="btn btn-success btn-sm edit-product"><i class="fas fa-edit"></i></button>
												<button type="button" class="btn btn-danger btn-sm remove-product"><i class='fa fa-trash'></i></button>
											</td>
											<input type="hidden" name="product_id[]" value="${item['id']}">
											<input type="hidden" name="quantity[]" class="input-quantity" value="1">
											<input type="hidden" name="unit_cost[]" class="input-unit-cost" value="${unit_cost.toFixed(2)}">
											<input type="hidden" name="discount[]" class="input-discount" value="0.00">
											<input type="hidden" name="tax_amount[]" class="input-tax" value="${tax_amount.toFixed(2)}">
											<input type="hidden" name="tax_id[]"  value="${product['tax_id']}">
											<input type="hidden" name="unit_tax[]" class="input-unit-tax" value="${tax_amount.toFixed(2)}">
											<input type="hidden" name="sub_total[]" class="input-sub-total" value="${sub_total.toFixed(2)}">
									</tr>`;

        		$("#order-table > tbody").append(product_row);
				update_summary();

				$("#product").val("");

			 }
		});

	});


	
	//click remove product
	$(document).on('click','.remove-product',function(){
		$(this).parent().parent().remove();
		update_summary();
	});

	//Click Edit product
	var current_row;
	$(document).on('click','.edit-product',function(){
		var tr = $(this).parent().parent();
		current_row = tr;

		//Get current value
		var quantity = parseFloat($(tr).find(".quantity").html());
		var c_unit_cost = parseFloat($(tr).find(".unit-cost").html());
		var c_discount = parseFloat($(tr).find(".discount").html());

		var form = `<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">{{ _lang('Quantity') }}</label>						
							<input type="number" class="form-control" value="${quantity}" id="modal-quantity">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label">{{ _lang('Discount').' '.currency() }}</label>						
							<input type="text" class="form-control float-field" value="${c_discount}" id="modal-discount">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<button type="button" id="update-product" class="btn btn-primary">{{ _lang('Save') }}</button>
						</div>
					</div>`;
	
		$("#main_modal .modal-title").html("{{ _lang('Update Product') }}");
		$("#main_modal .modal-body").html(form);
		$("#main_modal").modal("show");


	});


	$(document).on('click','#update-product',function(){
			$("#main_modal").modal("hide");

			var quantity = parseFloat($("#modal-quantity").val());
			var c_discount = parseFloat($("#modal-discount").val());

			var unit_tax = $(current_row).find(".input-unit-tax").val();
			var c_unit_cost = $(current_row).find(".input-unit-cost").val();

			//Set new value
			$(current_row).find(".quantity").html(quantity);
			$(current_row).find(".tax").html((unit_tax*quantity).toFixed(2));
			$(current_row).find(".discount").html(c_discount.toFixed(2));

			var sub_total = (c_unit_cost*quantity)+(unit_tax*quantity)-c_discount;
			$(current_row).find(".sub-total").html(sub_total.toFixed(2));

			//set value to hidden field
			$(current_row).find(".input-quantity").val(quantity);
			$(current_row).find(".input-tax").val((unit_tax*quantity).toFixed(2));
			$(current_row).find(".input-discount").val(c_discount.toFixed(2));
			$(current_row).find(".input-sub-total").val(sub_total.toFixed(2));

			update_summary();

	});


function update_summary(){
	total_quantity = 0;
	total_discount  = 0;
	total_tax = 0;
	product_total = 0;

	$("#order-table > tbody > tr").each(function(index, obj){
		total_quantity = total_quantity + parseFloat($(this).find(".quantity").html());	
		total_discount = total_discount +  parseFloat($(this).find(".discount").html());	
		total_tax = total_tax +  parseFloat($(this).find(".tax").html());	
		product_total = product_total +  parseFloat($(this).find(".sub-total").html());	
	}); 

	$("#total-qty").html(total_quantity);
	$("#total-discount").html(total_discount.toFixed(2));
	$("#total-tax").html(total_tax.toFixed(2));
	$("#total").html(product_total.toFixed(2));
	$("#product_total").val(product_total.toFixed(2));

}


</script>
@endsection


