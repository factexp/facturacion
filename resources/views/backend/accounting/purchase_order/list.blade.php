@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Purchase Order') }}</span>
			<a class="btn btn-primary btn-sm pull-right" href="{{route('purchase_orders.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			<table class="table table-bordered data-table">
				<thead>
					<tr>
						<th>{{ _lang('Order Date') }}</th>
						<th>{{ _lang('Supplier') }}</th>
						<th class="text-center">{{ _lang('Order Status') }}</th>
						<th class="text-right">{{ _lang('Grand Total') }}</th>
						<th class="text-right">{{ _lang('Paid') }}</th>
						<th class="text-right">{{ _lang('Due') }}</th>
						<th class="text-center">{{ _lang('Payment Status') }}</th>
						<th class="text-center">{{ _lang('Action') }}</th>
					</tr>
				</thead>
				<tbody>
			    @php $currency = currency(); @endphp		
					@foreach($purchases as $purchase)
					<tr id="row_{{ $purchase->id }}">
						  <td class='order_date'>{{ date('d/M/Y',strtotime($purchase->order_date)) }}</td>
							<td class='supplier_id'>{{ $purchase->supplier->supplier_name }}</td>
								@if($purchase->order_status == 1)
										<td class='order_status text-center'><span class="label label-info">{{ _lang('Ordered') }}</span></td>
								@elseif($purchase->order_status == 2)
										<td class='order_status text-center'><span class="label label-danger">{{ _lang('Pending') }}</span></td>
								@elseif($purchase->order_status == 3)
										<td class='order_status text-center'><span class="label label-success">{{ _lang('Received') }}</span></td>
								@endif
							<td class='grand_total text-right'>{{ $currency.' '.$purchase->grand_total }}</td>	
							<td class='paid text-right'>{{ $currency.' '.$purchase->paid }}</td>	
							<td class='due text-right'>{{ $purchase->grand_total > $purchase->paid ? $currency.' '.($purchase->grand_total-$purchase->paid) : $currency.' 0.00' }}</td>	
							<td class='payment_status text-center'>@if($purchase->payment_status == 0) <span class="label label-danger">{{ _lang('Due') }}</span> @else <span class="label label-success">{{ _lang('Paid') }}</span> @endif</td>	
							<td class="text-center">
								
								<div class="dropdown">
									<button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">{{ _lang('Action') }}
									<span class="caret"></span></button>
									<ul class="dropdown-menu">
										<li><a href="{{ action('PurchaseController@edit', $purchase->id) }}"><i class="fas fa-edit"></i> {{ _lang('Edit') }}</a></li>
										<li><a href="{{ action('PurchaseController@show', $purchase->id) }}"><i class="fas fa-eye"></i> {{ _lang('View') }}</a></li>
										<li><a href="{{ url('purchase_orders/create_payment/'.$purchase->id) }}" data-title="{{ _lang('Make Payment') }}" class="ajax-modal"><i class="fas fa-credit-card"></i> {{ _lang('Make Payment') }}</a></li>
										<li><a href="{{ url('purchase_orders/view_payment/'.$purchase->id) }}" data-title="{{ _lang('View Payment') }}" data-fullscreen="true" class="ajax-modal"><i class="fas fa-credit-card"></i> {{ _lang('View Payment') }}</a></li>
										<li>
											<form action="{{action('PurchaseController@destroy', $purchase['id'])}}" method="post">									
												{{ csrf_field() }}
												<input name="_method" type="hidden" value="DELETE">
												<button class="button-link btn-remove" type="submit"><i class="fas fa-trash-alt"></i> {{ _lang('Delete') }}</button>
											</form>
										</li>
									</ul>
								</div>
								
							</td>
					</tr>
					@endforeach
				</tbody>
		  </table>
			</div>
		</div>
	</div>
</div>

@endsection


