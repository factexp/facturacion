<div class="panel panel-default">
<div class="panel-body">
	@php $currency = currency() @endphp
	<table class="table table-bordered">
	 <tr><td>{{ _lang('Order Date') }}</td><td>{{ date('d/M/Y',strtotime($purchase->order_date)) }}</td></tr>
			<tr><td>{{ _lang('Supplier') }}</td><td>{{ $purchase->supplier->supplier_name }}</td></tr>
			<tr>
				<td>{{ _lang('Order Status') }}</td>
					@if($purchase->order_status == 1)
							<td><span class="label label-info">{{ _lang('Ordered') }}</span></td>
					@elseif($purchase->order_status == 2)
							<td><span class="label label-danger">{{ _lang('Pending') }}</span></td>
					@elseif($purchase->order_status == 3)
							<td><span class="label label-success">{{ _lang('Received') }}</span></td>
					@elseif($purchase->order_status == 4)
							<td><span class="label label-danger">{{ _lang('Canceled') }}</span></td>
					@endif
			</tr>
			<tr><td>{{ _lang('Order Tax') }}</td><td>{{ $currency.' '.$purchase->order_tax }}</td></tr>
			<tr><td>{{ _lang('Discount') }}</td><td>{{ $currency.' '.$purchase->order_discount }}</td></tr>
			<tr><td>{{ _lang('Shipping Cost') }}</td><td>{{ $currency.' '.$purchase->shipping_cost }}</td></tr>
			<tr><td>{{ _lang('Product Total') }}</td><td>{{ $currency.' '.$purchase->product_total }}</td></tr>
			<tr><td>{{ _lang('Grand Total') }}</td><td>{{ $currency.' '.$purchase->grand_total }}</td></tr>
			<tr><td>{{ _lang('Paid') }}</td><td>{{ $currency.' '.$purchase->paid }}</td></tr>
			<tr><td>{{ _lang('Due') }}</td><td>{{ $currency.' '.($purchase->grand_total-$purchase->paid) }}</td></tr>
			<tr><td>{{ _lang('Payment Status') }}</td><td>@if($purchase->payment_status == 0) <span class="label label-danger">{{ _lang('Due') }}</span> @else <span class="label label-success">{{ _lang('Paid') }}</span> @endif</td></tr>
			<tr><td>{{ _lang('Attachemnt') }}</td><td>@if($purchase->attachemnt != "") <a class="btn btn-success btn-xs" target="_blank" href="{{ asset('public/uploads/attachments/'.$purchase->attachemnt) }}">{{ _lang('View') }}</a> @else <span class="label label-danger">{{ _lang('Not Available !') }}</span>@endif</td></tr>
			<tr><td>{{ _lang('Note') }}</td><td>{{ $purchase->note }}</td></tr>	
	</table>


	  <!--Order table -->
		<div class="table-responsive">
			<table id="order-table" class="table table-bordered">
				<thead>
					<tr>
						<th>{{ _lang('Name') }}</th>
						<th class="text-center" style="width:100px">{{ _lang('Quantity') }}</th>
						<th class="text-right">{{ _lang('Net Unit Cost').' '.$currency }}</th>
						<th class="text-right" style="width:100px">{{ _lang('Discount').' '.$currency }}</th>
						<th class="text-right">{{ _lang('Tax method') }}</th>
						<th class="text-right">{{ _lang('Tax').' '.$currency }}</th>
						<th class="text-right">{{ _lang('Sub Total').' '.$currency }}</th>
					</tr>
				</thead>

				<tbody>
					@foreach($purchase->purchase_items as $item)
						<tr id="product-{{ $item->product_id }}">
								<td>{{ $item->item->item_name }}</td>
								<td class="text-center quantity">{{ $item->quantity }}</td>
								<td class="text-right unit-cost">{{ $item->unit_cost }}</td>
								<td class="text-right discount">{{ $item->discount }}</td>
								<td class="text-right tax-method">{{ strtoupper($item->item->product->tax_method) }}</td>
								<td class="text-right tax">{{ $item->tax_amount }}</td>
								<td class="text-right sub-total">{{ $item->sub_total }}</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot class="tfoot active">
					<tr>
						<th>{{ _lang('Total') }}</th>
						<th class="text-center" id="total-qty">0</th>
						<th></th>
						<th class="text-right" id="total-discount">0.00</th>
						<th></th>
						<th class="text-right" id="total-tax">0.00</th>
						<th class="text-right" id="total">0.00</th>
					</tr>
				</tfoot>
			</table>
		</div>
		<!--End Order table -->
</div>
</div>

<script>
var total_quantity = 0;	
var total_discount = 0;	
var total_tax = 0;	
var product_total = 0;	

function update_summary(){
	total_quantity = 0;
	total_discount  = 0;
	total_tax = 0;
	product_total = 0;

	$("#order-table > tbody > tr").each(function(index, obj){
		total_quantity = total_quantity + parseFloat($(this).find(".quantity").html());	
		total_discount = total_discount +  parseFloat($(this).find(".discount").html());	
		total_tax = total_tax +  parseFloat($(this).find(".tax").html());	
		product_total = product_total +  parseFloat($(this).find(".sub-total").html());	
	}); 

	$("#total-qty").html(total_quantity);
	$("#total-discount").html(total_discount.toFixed(2));
	$("#total-tax").html(total_tax.toFixed(2));
	$("#total").html(product_total.toFixed(2));

}

update_summary();
</script>	
