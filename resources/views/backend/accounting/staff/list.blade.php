@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('Staff List') }}</span>
				<a class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ _lang('Add Staff') }}" href="{{route('staffs.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			<table class="table table-bordered data-table">
			<thead>
			  <tr>
				<th class="text-center">{{ _lang('#') }}</th>
				<th>{{ _lang('Name') }}</th>
				<th>{{ _lang('Email') }}</th>
				<th>{{ _lang('Status') }}</th>
				<th class="text-center">{{ _lang('Action') }}</th>
			  </tr>
			</thead>
			<tbody>
			  
			  @foreach($users as $user)
			    <tr id="row_{{ $user->id }}">
					<td class="text-center"><img src="{{ asset('public/uploads/profile/'.$user->profile_picture) }}" class="img-avatar"></td>
					<td class='name'>{{ $user->name }}</td>
					<td class='email'>{{ $user->email }}</td>				
					<td class='status'>{{ $user->status == 1 ? _lang('Active') : _lang('In-Active') }}</td>					
					<td class="text-center">
					  <form action="{{action('StaffController@destroy', $user['id'])}}" method="post">
						<a href="{{action('StaffController@edit', $user['id'])}}" data-title="{{ _lang('Update Staf') }}" class="btn btn-warning btn-sm ajax-modal">{{ _lang('Edit') }}</a>
						<a href="{{action('StaffController@show', $user['id'])}}" data-title="{{ _lang('View Staf') }}" class="btn btn-info btn-sm ajax-modal">{{ _lang('View') }}</a>
						{{ csrf_field() }}
						<input name="_method" type="hidden" value="DELETE">
						<button class="btn btn-danger btn-sm btn-remove" type="submit">{{ _lang('Delete') }}</button>
					  </form>
					</td>
			    </tr>
			  @endforeach
			</tbody>
		  </table>
			</div>
		</div>
	</div>
</div>

@endsection


