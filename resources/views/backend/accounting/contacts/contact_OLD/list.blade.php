@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Contact') }}</span>
			<a class="btn btn-primary btn-sm pull-right" data-title="{{ _lang('Add New Contact') }}" href="{{route('contacts.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			<table id="contacts-table" class="table table-bordered">
			<thead>
			  <tr>
				<th>{{ _lang('Image') }}</th>
				<th>{{ _lang('Profile Type') }}</th>
				<th>{{ _lang('Contact Name') }}</th>
				<th>{{ _lang('Email') }}</th>
				<th>{{ _lang('Phone') }}</th>
				<th>{{ _lang('Group') }}</th>
				<th class="text-center">{{ _lang('Action') }}</th>
			  </tr>
			</thead>
			<tbody>
			  
			</tbody>
		  </table>
			</div>
		</div>
	</div>
</div>

@endsection



@section('js-script')
<script>
	$(function() {
        $('#contacts-table').DataTable({
            processing: true,
            serverSide: true,
						ajax: '{{ url('contacts/get_table_data') }}',
						"columns" : [
								{ data : "contact_image", name : "contact_image" },
								{ data : "profile_type", name : "profile_type" },
								{ data : "contact_name", name : "contact_name" },
								{ data : "contact_email", name : "contact_email" },
								{ data : "contact_phone", name : "contact_phone" },
								{ data : "group.name", name : "group.name" },
								{ data : "action", name : "action" },
						],
						responsive: true,
						"bStateSave": true,
						"bAutoWidth":false,	
						"ordering": false,
						"language": {
						   "decimal":        "",
						   "emptyTable":     "{{ _lang('No Data Found') }}",
						   "info":           "{{ _lang('Showing') }} _START_ {{ _lang('to') }} _END_ {{ _lang('of') }} _TOTAL_ {{ _lang('Entries') }}",
						   "infoEmpty":      "{{ _lang('Showing 0 To 0 Of 0 Entries') }}",
						   "infoFiltered":   "(filtered from _MAX_ total entries)",
						   "infoPostFix":    "",
						   "thousands":      ",",
						   "lengthMenu":     "{{ _lang('Show') }} _MENU_ {{ _lang('Entries') }}",
						   "loadingRecords": "{{ _lang('Loading...') }}",
						   "processing":     "{{ _lang('Processing...') }}",
						   "search":         "{{ _lang('Search') }}",
						   "zeroRecords":    "{{ _lang('No matching records found') }}",
						   "paginate": {
							  "first":      "{{ _lang('First') }}",
							  "last":       "{{ _lang('Last') }}",
							  "next":       "{{ _lang('Next') }}",
							  "previous":   "{{ _lang('Previous') }}"
						  }
						} 
        });
    });
</script>
@endsection


