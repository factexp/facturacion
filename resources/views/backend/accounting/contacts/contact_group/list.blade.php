@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Contact Group') }}</span>
			<a class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ _lang('Add Contact Group') }}" href="{{route('contact_groups.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			 @if (\Session::has('success'))
			  <div class="alert alert-success">
				<p>{{ \Session::get('success') }}</p>
			  </div>
			  <br />
			 @endif
			<table class="table table-bordered data-table">
			<thead>
			  <tr>
				<th>{{ _lang('Group') }}</th>
				<th>{{ _lang('Note') }}</th>
				<th class="text-center">{{ _lang('Action') }}</th>
			  </tr>
			</thead>
			<tbody>
			  
			  @foreach($contactgroups as $contactgroup)
			  <tr id="row_{{ $contactgroup->id }}">
				<td class='group'>{{ $contactgroup->name }}</td>
				<td class='note'>{{ $contactgroup->note }}</td>
				<td class="text-center">
				  <form action="{{action('ContactGroupController@destroy', $contactgroup['id'])}}" method="post">
					<a href="{{action('ContactGroupController@edit', $contactgroup['id'])}}" data-title="{{ _lang('Update Contact Group') }}" class="btn btn-warning btn-sm ajax-modal">{{ _lang('Edit') }}</a>
					<a href="{{action('ContactGroupController@show', $contactgroup['id'])}}" data-title="{{ _lang('View Contact Group') }}" class="btn btn-info btn-sm ajax-modal">{{ _lang('View') }}</a>
					{{ csrf_field() }}
					<input name="_method" type="hidden" value="DELETE">
					<button class="btn btn-danger btn-sm btn-remove" type="submit">{{ _lang('Delete') }}</button>
				  </form>
				</td>
			  </tr>
			  @endforeach
			</tbody>
		  </table>
			</div>
		</div>
	</div>
</div>

@endsection


