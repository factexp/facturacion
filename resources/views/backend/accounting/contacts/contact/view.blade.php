@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading">{{ _lang('View Contact') .' - '. $contact->contact_name }}</div>

	<div class="panel-body">
	  <ul class="nav nav-tabs">
		  <li class="active"><a data-toggle="tab" href="#general-info" aria-expanded="true">{{ _lang('General Info') }}</a></li>
		  <li class=""><a data-toggle="tab" href="#invoices" aria-expanded="false">{{ _lang('Invoices') }}</a></li>
		  <li class=""><a data-toggle="tab" href="#quotations" aria-expanded="false">{{ _lang('Quotations') }}</a></li>
		  <li class=""><a data-toggle="tab" href="#transactions" aria-expanded="false">{{ _lang('Transactions') }}</a></li>
		  <li class=""><a data-toggle="tab" href="#email" aria-expanded="false">{{ _lang('Email') }}</a></li>
	  </ul>
	  <div class="tab-content" id="crm-tab">
	
	      <div id="general-info" class="tab-pane fade in active">
			  <table class="table table-bordered">
				<tr><td colspan="2" class="text-center"><img class="thumb-image-lg img-thumbnail" src="{{ asset('public/uploads/contacts/'.$contact->contact_image) }}"></td></tr>
				<tr><td>{{ _lang('Profile Type') }}</td><td>{{ $contact->profile_type }}</td></tr>
				<tr><td>{{ _lang('Company Name') }}</td><td>{{ $contact->company_name }}</td></tr>
				<tr><td>{{ _lang('Contact Name') }}</td><td>{{ $contact->contact_name }}</td></tr>
				<tr><td>{{ _lang('Contact Email') }}</td><td>{{ $contact->contact_email }}</td></tr>
				<tr><td>{{ _lang('Contact Phone') }}</td><td>{{ $contact->contact_phone }}</td></tr>
				<tr><td>{{ _lang('Country') }}</td><td>{{ $contact->country }}</td></tr>
				<tr><td>{{ _lang('City') }}</td><td>{{ $contact->city }}</td></tr>
				<tr><td>{{ _lang('State') }}</td><td>{{ $contact->state }}</td></tr>
				<tr><td>{{ _lang('Zip') }}</td><td>{{ $contact->zip }}</td></tr>
				<tr><td>{{ _lang('Address') }}</td><td>{{ $contact->address }}</td></tr>
				<tr><td>{{ _lang('Facebook') }}</td><td>{{ $contact->facebook }}</td></tr>
				<tr><td>{{ _lang('Twitter') }}</td><td>{{ $contact->twitter }}</td></tr>
				<tr><td>{{ _lang('Linkedin') }}</td><td>{{ $contact->linkedin }}</td></tr>
				<tr><td>{{ _lang('Remarks') }}</td><td>{{ $contact->remarks }}</td></tr>
				<tr><td>{{ _lang('Group') }}</td><td>{{ $contact->group->name }}</td></tr>
			  </table>
		  </div>
		  
		  
		  <div id="invoices" class="tab-pane fade">
		      @php $currency = currency() @endphp
			  <table class="table table-bordered data-table">
				<thead>
				  <tr>
					<th>{{ _lang('Invoice Number') }}</th>
					<th>{{ _lang('Due Date') }}</th>
					<th class="text-right">{{ _lang('Grand Total') }}</th>
					<th class="text-right">{{ _lang('Paid') }}</th>
					<th class="text-center">{{ _lang('Status') }}</th>
					<th class="text-center">{{ _lang('Action') }}</th>
				  </tr>
				</thead>
				<tbody>
				  
				  @foreach($invoices as $invoice)
				  <tr id="row_{{ $invoice->id }}">
					<td class='invoice_number'>{{ $invoice->invoice_number }}</td>
					<td class='due_date'>{{ date("d/M/Y",strtotime($invoice->due_date)) }}</td>
					<td class='grand_total text-right'>{{ $currency." ".$invoice->grand_total }}</td>
					<td class='paid text-right'>{{ $currency." ".$invoice->paid }}</td>
					<td class='status text-center'>{!! invoice_status($invoice->status) !!}</td>
					<td class="text-center">

						<div class="dropdown">
							<button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">{{ _lang('Action') }}
							<span class="caret"></span></button>
							<ul class="dropdown-menu">
								<li><a href="{{ action('InvoiceController@edit', $invoice->id) }}"><i class="fas fa-edit"></i> {{ _lang('Edit') }}</a></li>
								<li><a href="{{ action('InvoiceController@show', $invoice->id) }}" data-title="{{ _lang('View Invoice') }}" data-fullscreen="true"><i class="fas fa-eye"></i> {{ _lang('View') }}</a></li>
								<li><a href="{{ url('invoices/create_payment/'.$invoice->id) }}" data-title="{{ _lang('Make Payment') }}" class="ajax-modal"><i class="fas fa-credit-card"></i> {{ _lang('Make Payment') }}</a></li>
								<li><a href="{{ url('invoices/view_payment/'.$invoice->id) }}" data-title="{{ _lang('View Payment') }}" data-fullscreen="true" class="ajax-modal"><i class="fas fa-credit-card"></i> {{ _lang('View Payment') }}</a></li>
								<li>
									<form action="{{action('InvoiceController@destroy', $invoice['id'])}}" method="post">									
										{{ csrf_field() }}
										<input name="_method" type="hidden" value="DELETE">
										<button class="button-link btn-remove" type="submit"><i class="fas fa-trash-alt"></i> {{ _lang('Delete') }}</button>
									</form>
								</li>
							</ul>
						</div>
					</td>
				  </tr>
				  @endforeach
				</tbody>
			  </table>
		  </div>
		  
		  <div id="quotations" class="tab-pane fade">
		        @php $currency = currency() @endphp
				<table class="table table-bordered data-table">
					<thead>
						<tr>
							<th>{{ _lang('Quotation Number') }}</th>
							<th>{{ _lang('Date') }}</th>
							<th class="text-right">{{ _lang('Grand Total') }}</th>
							<th class="text-center">{{ _lang('Action') }}</th>
						</tr>
					</thead>
					<tbody>					  
						@foreach($quotations as $quotation)
						<tr id="row_{{ $quotation->id }}">
							<td class='invoice_number'>{{ $quotation->quotation_number }}</td>
							<td class='due_date'>{{ date("d/M/Y",strtotime($quotation->quotation_date)) }}</td>
							<td class='grand_total text-right'>{{ $currency." ".decimalPlace($quotation->grand_total) }}</td>
							<td class="text-center">

								<div class="dropdown">
									<button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">{{ _lang('Action') }}
									<i class="mdi mdi-chevron-down"></i></button>
									<ul class="dropdown-menu">
										<a class="dropdown-item" href="{{ action('QuotationController@edit', $quotation->id) }}"><i class="fas fa-edit"></i> {{ _lang('Edit') }}</a>
										<a class="dropdown-item" href="{{ action('QuotationController@show', $quotation->id) }}" data-title="{{ _lang('View Invoice') }}" data-fullscreen="true"><i class="fas fa-eye"></i> {{ _lang('View') }}</a>
										<a class="dropdown-item" href="{{ action('QuotationController@convert_invoice', $quotation->id) }}"><i class="fas fa-credit-card"></i> {{ _lang('Convert to Invoice') }}</a>
										
										<form action="{{action('QuotationController@destroy', $quotation->id)}}" method="post">									
											{{ csrf_field() }}
											<input name="_method" type="hidden" value="DELETE">
											<button class="button-link btn-remove" type="submit"><i class="fas fa-trash-alt"></i> {{ _lang('Delete') }}</button>
										</form>
									</ul>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
		  </div>
		  
		  <div id="transactions" class="tab-pane fade">
				<table class="table table-bordered data-table">
					<thead>			
						<th>{{ _lang('Date') }}</th>
						<th>{{ _lang('Account') }}</th>
						<th>{{ _lang('Category') }}</th>
						<th class="text-right">{{ _lang('Amount') }}</th>
						<th>{{ _lang('Payment Method') }}</th>
					</thead>
					<tbody>
                       @foreach($transactions as $transaction)
					     <tr>
							<td>{{ date("d/M/Y",strtotime($transaction->trans_date)) }}</td>
							<td>{{ isset($transaction->account) ? $transaction->account->account_title : '' }}</td>
							<td>{{ $transaction->income_type->name }}</td>
							<td class="text-right">{{ $currency.' '.decimalPlace($transaction->amount) }}</td>
							<td>{{ isset($transaction->payment_method) ? $transaction->payment_method->name : '' }}</td>
						</tr>
					   @endforeach
					</tbody>
			   </table>
		  </div>
		  
		  <div id="email" class="tab-pane fade">
		    <form action="{{ url('contacts/send_email/'.$contact->id) }}" class="validate" method="post">
			    {{ csrf_field() }}
				<div class="col-md-12">
				  <div class="form-group">
					<label class="control-label">{{ _lang('Email Subject') }}</label>						
					<input type="text" class="form-control" name="email_subject" value="{{ old('email_subject') }}" required>
				  </div>
				</div>
				<div class="col-md-12">
				  <div class="form-group">
					<label class="control-label">{{ _lang('Email Message') }}</label>						
					<textarea class="form-control summernote" name="email_message" required>{{ old('email_message') }}</textarea>
				  </div>
				</div>
				<div class="col-md-12">
				  <div class="form-group">
					<button type="submit" class="btn btn-primary">{{ _lang('Send Email') }}</button>
				  </div>
				</div>
			</form>
		  </div>
		  
		  
	  </div> <!--End TAB-->
	</div>
  </div>
 </div>
</div>
@endsection


