@extends('layouts.app')

@section('content')
<div class="row">
<form method="post" class="validate" autocomplete="off" action="{{url('contacts')}}" enctype="multipart/form-data">
	<div class="col-md-8">
	<div class="panel panel-default">
	<div class="panel-heading panel-title">{{ _lang('Add New Contact') }}</div>

	<div class="panel-body">
		{{ csrf_field() }}

		<div class="col-md-6">
		  <div class="form-group">
			<label class="control-label">{{ _lang('Profile Type') }}</label>						
			<select class="form-control select2" name="profile_type" required>
		        <option value="Company" {{ old('profile_type')=="Company" ? "selected" : "" }}>{{ _lang('Company') }}</option>
		        <option value="Individual" {{ old('profile_type')=="Individual" ? "selected" : "" }}>{{ _lang('Individual') }}</option>
			</select>
		  </div>
		</div>

		<div class="col-md-6">
		  <div class="form-group">
			<label class="control-label">{{ _lang('Company Name') }}</label>						
			<input type="text" class="form-control" name="company_name" value="{{ old('company_name') }}">
		  </div>
		</div>

		<div class="col-md-6">
		  <div class="form-group">
			<label class="control-label">{{ _lang('Contact Name') }}</label>						
			<input type="text" class="form-control" name="contact_name" value="{{ old('contact_name') }}" required>
		  </div>
		</div>

		<div class="col-md-6">
		  <div class="form-group">
			<label class="control-label">{{ _lang('Contact Email') }}</label>						
			<input type="text" class="form-control" name="contact_email" value="{{ old('contact_email') }}" required>
		  </div>
		</div>

		<div class="col-md-6">
		  <div class="form-group">
			<label class="control-label">{{ _lang('Contact Phone') }}</label>						
			<input type="text" class="form-control" name="contact_phone" value="{{ old('contact_phone') }}">
		  </div>
		</div>

		<div class="col-md-6">
		  <div class="form-group">
			<label class="control-label">{{ _lang('Country') }}</label>						
			<select class="form-control select2" name="country">
				{{ get_country_list( old('country') ) }}
			</select>
		  </div>
		</div>
		
		<div class="col-md-6">
		  <div class="form-group">
			<label class="control-label">{{ _lang('Group') }}</label>						
			<select class="form-control select2" name="group_id" required>
				<option value="">{{ _lang('- Select Group -') }}</option>
				{{ create_option("contact_groups","id","name",old('group_id'),array("company_id="=>company_id())) }}
			</select>
		 </div>
		</div>

		<div class="col-md-6">
		  <div class="form-group">
			<label class="control-label">{{ _lang('City') }}</label>						
			<input type="text" class="form-control" name="city" value="{{ old('city') }}">
		  </div>
		</div>

		<div class="col-md-6">
		  <div class="form-group">
			<label class="control-label">{{ _lang('State') }}</label>						
			<input type="text" class="form-control" name="state" value="{{ old('state') }}">
		  </div>
		</div>

		<div class="col-md-6">
		  <div class="form-group">
			<label class="control-label">{{ _lang('Zip') }}</label>						
			<input type="text" class="form-control" name="zip" value="{{ old('zip') }}">
		  </div>
		</div>

		<div class="col-md-6">
		  <div class="form-group">
			<label class="control-label">{{ _lang('Address') }}</label>						
			<textarea class="form-control" name="address">{{ old('address') }}</textarea>
		  </div>
		</div>
		
		<div class="col-md-6">
		  <div class="form-group">
			<label class="control-label">{{ _lang('Remarks') }}</label>						
			<textarea class="form-control" name="remarks">{{ old('remarks') }}</textarea>
		  </div>
		</div>

		<div class="col-md-12">
		  <div class="form-group">
			<label class="control-label">{{ _lang('Facebook') }}</label>						
			<input type="text" class="form-control" name="facebook" value="{{ old('facebook') }}">
		  </div>
		</div>

		<div class="col-md-12">
		  <div class="form-group">
			<label class="control-label">{{ _lang('Twitter') }}</label>						
			<input type="text" class="form-control" name="twitter" value="{{ old('twitter') }}">
		  </div>
		</div>

		<div class="col-md-12">
		  <div class="form-group">
			<label class="control-label">{{ _lang('Linkedin') }}</label>						
			<input type="text" class="form-control" name="linkedin" value="{{ old('linkedin') }}">
		  </div>
		</div>

		<div class="form-group">
		  <div class="col-md-12">
		    <button type="reset" class="btn btn-danger">{{ _lang('Reset') }}</button>
			<button type="submit" class="btn btn-primary">{{ _lang('Save') }}</button>
		  </div>
		</div>
	</div>
  </div>
 </div>
 
 <div class="col-md-4">
	<div class="panel panel-default">
	<div class="panel-heading">{{ _lang('Contact Image') }}</div>
		<div class="panel-body">
		   <div class="col-md-12">
			  <div class="form-group">
				<label class="control-label">{{ _lang('Contact Image') }}</label>						
				<input type="file" class="form-control dropify" name="contact_image"  data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG">
			  </div>
			</div>
		</div>
 	</div>
  </div> 
  
  <div class="col-md-4">
	<div class="panel panel-default">
		<div class="panel-heading">		
			<div class="togglebutton">
			  <label>
				{{ _lang('Login Details') }}&nbsp;&nbsp;
				<input type="checkbox" id="client_login" value="1" name="client_login">
			  </label>
			</div>
		</div>
		<div class="panel-body" id="client_login_panel">
		   <div class="col-md-12">
			  <div class="form-group">
				<label class="control-label">{{ _lang('Name') }}</label>						
				<input type="text" class="form-control" name="name" value="{{ old('name') }}">
			  </div>
			</div>
			
		   <div class="col-md-12">
			  <div class="form-group">
				<label class="control-label">{{ _lang('Email') }}</label>						
				<input type="email" class="form-control" name="email" value="{{ old('email') }}">
			  </div>
			</div>

			<div class="col-md-12">
			  <div class="form-group">
				<label class="control-label">{{ _lang('Password') }}</label>						
				<input type="password" class="form-control" name="password">
			  </div>
			</div>
			
			<div class="col-md-12">
			 <div class="form-group">
				<label class="control-label">{{ _lang('Confirm Password') }}</label>						
				<input type="password" class="form-control" name="password_confirmation">
			 </div>
			</div>
			
			<div class="col-md-12">
			  <div class="form-group">
				<label class="control-label">{{ _lang('Status') }}</label>						
				<select class="form-control" name="status">
				   <option value="1">{{ _lang('Active') }}</option>
				   <option value="0">{{ _lang('Inactive') }}</option>
				</select>
			  </div>
			</div>
		</div>
 	</div>
  </div>
  
 </form>
</div>
@endsection

@section('js-script')
<script>
$("#client_login_panel input, #client_login_panel select").prop("disabled",true);

$(document).on('change','#client_login',function(){
	if($(this).is(':checked') == false){
		$("#client_login_panel input, #client_login_panel select").prop("disabled",true);
	}else{
		$("#client_login_panel input, #client_login_panel select").prop("disabled",false);
	}
});
</script>
@endsection


