@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Income and Expense Type') }}</span>
			<a class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ _lang('Add Income/Expense Type') }}" href="{{route('chart_of_accounts.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			<table class="table table-bordered data-table">
				<thead>
				  <tr>
					<th>{{ _lang('Name') }}</th>
					<th>{{ _lang('Type') }}</th>
					<th class="action-col">{{ _lang('Action') }}</th>
				  </tr>
				</thead>
				<tbody>
				  
				  @foreach($chartofaccounts as $chartofaccount)
				  <tr id="row_{{ $chartofaccount->id }}">
					<td class='name'>{{ $chartofaccount->name }}</td>
					<td class='type'>{{ d_lang(ucwords($chartofaccount->type)) }}</td>	
					<td class="text-center">
					  <form action="{{action('ChartOfAccountController@destroy', $chartofaccount['id'])}}" method="post">
						<a href="{{action('ChartOfAccountController@edit', $chartofaccount['id'])}}" data-title="{{ _lang('Update Income/Expense Type') }}" class="btn btn-warning btn-sm ajax-modal">{{ _lang('Edit') }}</a>
						<a href="{{action('ChartOfAccountController@show', $chartofaccount['id'])}}" data-title="{{ _lang('View Income/Expense Type') }}" class="btn btn-info btn-sm ajax-modal">{{ _lang('View') }}</a>
						{{ csrf_field() }}
						<input name="_method" type="hidden" value="DELETE">
						<button class="btn btn-danger btn-sm btn-remove" type="submit">{{ _lang('Delete') }}</button>
					  </form>
					</td>
				  </tr>
				  @endforeach
				</tbody>
			  </table>
			</div>
		</div>
	</div>
</div>

@endsection


