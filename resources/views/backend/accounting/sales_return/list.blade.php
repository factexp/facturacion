@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Sales Return') }}</span>
			<a class="btn btn-primary btn-sm pull-right" href="{{ route('sales_returns.create') }}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			<table class="table table-bordered data-table">
				<thead>
					<tr>
						<th>{{ _lang('Return Date') }}</th>
						<th>{{ _lang('Customer') }}</th>
						<th class="text-right">{{ _lang('Grand Total') }}</th>
						<th class="text-center">{{ _lang('Action') }}</th>
					</tr>
				</thead>
				<tbody>
			    @php $currency = currency(); @endphp		
					@foreach($sales_returns as $sales)
					<tr id="row_{{ $sales->id }}">
						    <td class='order_date'>{{ date('d/M/Y',strtotime($sales->return_date)) }}</td>
							<td class='customer_id'>{{ $sales->customer->contact_name }}</td>
							<td class='grand_total text-right'>{{ $currency.' '.$sales->grand_total }}</td>	
							<td class="text-center">
								
								<div class="dropdown">
									<button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">{{ _lang('Action') }}
									<span class="caret"></span></button>
									<ul class="dropdown-menu">
										<li><a href="{{ action('SalesReturnController@edit', $sales->id) }}"><i class="fas fa-edit"></i> {{ _lang('Edit') }}</a></li>
										<li><a href="{{ action('SalesReturnController@show', $sales->id) }}" data-title="{{ _lang('View Sales Return') }}" data-fullscreen="true" class="ajax-modal"><i class="fas fa-eye"></i> {{ _lang('View') }}</a></li>
										<li>
											<form action="{{action('SalesReturnController@destroy', $sales->id)}}" method="post">									
												{{ csrf_field() }}
												<input name="_method" type="hidden" value="DELETE">
												<button class="button-link btn-remove" type="submit"><i class="fas fa-trash-alt"></i> {{ _lang('Delete') }}</button>
											</form>
										</li>
									</ul>
								</div>
								
							</td>
					</tr>
					@endforeach
				</tbody>
		  </table>
			</div>
		</div>
	</div>
</div>

@endsection


