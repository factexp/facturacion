@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Tax') }}</span>
			<a class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ _lang('Add Tax') }}" href="{{route('taxs.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			 @if (\Session::has('success'))
			  <div class="alert alert-success">
				<p>{{ \Session::get('success') }}</p>
			  </div>
			  <br />
			 @endif
			<table class="table table-bordered data-table">
			<thead>
			  <tr>
				<th>{{ _lang('Tax Name') }}</th>
				<th>{{ _lang('Rate') }}</th>
				<th>{{ _lang('Type') }}</th>
				<th class="text-center">{{ _lang('Action') }}</th>
			  </tr>
			</thead>
			<tbody>
			  @php $currency = currency(); @endphp
			  @foreach($taxs as $tax)
			  <tr id="row_{{ $tax->id }}">
				<td class='tax_name'>{{ $tax->tax_name }}</td>
				<td class='rate'>{{ $currency." ".$tax->rate }}</td>	
				<td class='type'>{{ ucwords($tax->type) }}</td>	
				<td class="text-center">
				  <form action="{{action('TaxController@destroy', $tax['id'])}}" method="post">
					<a href="{{action('TaxController@edit', $tax['id'])}}" data-title="{{ _lang('Update Tax') }}" class="btn btn-warning btn-sm ajax-modal">{{ _lang('Edit') }}</a>
					<a href="{{action('TaxController@show', $tax['id'])}}" data-title="{{ _lang('View Tax') }}" class="btn btn-info btn-sm ajax-modal">{{ _lang('View') }}</a>
					{{ csrf_field() }}
					<input name="_method" type="hidden" value="DELETE">
					<button class="btn btn-danger btn-sm btn-remove" type="submit">{{ _lang('Delete') }}</button>
				  </form>
				</td>
			  </tr>
			  @endforeach
			</tbody>
		  </table>
			</div>
		</div>
	</div>
</div>

@endsection


