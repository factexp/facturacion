@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Repeating Expense') }}</span>
			<a class="btn btn-primary btn-sm pull-right" data-title="{{ _lang('Add Repeating Expense') }}" href="{{route('repeating_expense.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
				<table id="repeating-expense-table" class="table table-bordered">
					<thead>
						<tr>
						<th>{{ _lang('Date') }}</th>
						<th>{{ _lang('Account') }}</th>
						<th>{{ _lang('Expense Type') }}</th>
						<th class="text-right">{{ _lang('Amount') }}</th>
						<th>{{ _lang('Payee') }}</th>
						<th>{{ _lang('Payment Method') }}</th>
						<th>{{ _lang('Status') }}</th>
						<th class="action-col">{{ _lang('Action') }}</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection


@section('js-script')
<script>
	$(function() {
        $('#repeating-expense-table').DataTable({
            processing: true,
            serverSide: true,
			ajax: '{{ url('repeating_expense/get_table_data') }}',
			"columns" : [
					{ data : "trans_date", name : "trans_date" },
					{ data : "account.account_title", name : "account.account_title" },
					{ data : "expense_type.name", name : "expense_type.name" },
					{ data : "amount", name : "amount" },
					{ data : "payee.contact_name", name : "payee.contact_name" },
					{ data : "payment_method.name", name : "payment_method.name" },
					{ data : "status", name : "status" },
					{ data : "action", name : "action" },
			],
			responsive: true,
			"bStateSave": true,
			"bAutoWidth":false,	
			"ordering": false,
			"language": {
				"decimal":        "",
				"emptyTable":     "{{ _lang('No Data Found') }}",
				"info":           "{{ _lang('Showing') }} _START_ {{ _lang('to') }} _END_ {{ _lang('of') }} _TOTAL_ {{ _lang('Entries') }}",
				"infoEmpty":      "{{ _lang('Showing 0 To 0 Of 0 Entries') }}",
				"infoFiltered":   "(filtered from _MAX_ total entries)",
				"infoPostFix":    "",
				"thousands":      ",",
				"lengthMenu":     "{{ _lang('Show') }} _MENU_ {{ _lang('Entries') }}",
				"loadingRecords": "{{ _lang('Loading...') }}",
				"processing":     "{{ _lang('Processing...') }}",
				"search":         "{{ _lang('Search') }}",
				"zeroRecords":    "{{ _lang('No matching records found') }}",
				"paginate": {
					"first":      "{{ _lang('First') }}",
					"last":       "{{ _lang('Last') }}",
					"next":       "{{ _lang('Next') }}",
					"previous":   "{{ _lang('Previous') }}"
				}
			} 
        });
    });
</script>
@endsection


