@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Payers and Payee') }}</span>
			<a class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ _lang('Add Payers/Payee') }}" href="{{route('payers_payees.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			<table class="table table-bordered data-table">
			<thead>
			  <tr>
				<th>{{ _lang('Name') }}</th>
				<th>{{ _lang('Type') }}</th>
				<th class="action-col">{{ _lang('Action') }}</th>
			  </tr>
			</thead>
			<tbody>
			  
			  @foreach($payerpayees as $payerpayee)
			  <tr id="row_{{ $payerpayee->id }}">
				<td class='name'>{{ $payerpayee->name }}</td>
				<td class='type'>{{ ucwords($payerpayee->type) }}</td>	
				<td class="text-center">
				  <form action="{{action('PayerPayeeController@destroy', $payerpayee['id'])}}" method="post">
					<a href="{{action('PayerPayeeController@edit', $payerpayee['id'])}}" data-title="{{ _lang('Update Payers/Payee') }}" class="btn btn-warning btn-sm ajax-modal">{{ _lang('Edit') }}</a>
					<a href="{{action('PayerPayeeController@show', $payerpayee['id'])}}" data-title="{{ _lang('View Payers/Payee') }}" class="btn btn-info btn-sm ajax-modal">{{ _lang('View') }}</a>
					{{ csrf_field() }}
					<input name="_method" type="hidden" value="DELETE">
					<button class="btn btn-danger btn-sm btn-remove" type="submit">{{ _lang('Delete') }}</button>
				  </form>
				</td>
			  </tr>
			  @endforeach
			</tbody>
		  </table>
			</div>
		</div>
	</div>
</div>

@endsection


