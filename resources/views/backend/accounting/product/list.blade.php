@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Product') }}</span>
			<a class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ _lang('Add Product') }}" href="{{route('products.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			 @if (\Session::has('success'))
			  <div class="alert alert-success">
				<p>{{ \Session::get('success') }}</p>
			  </div>
			  <br />
			 @endif
			<table class="table table-bordered data-table">
			<thead>
			  <tr>
					<th>{{ _lang('Product') }}</th>
					<th>{{ _lang('Product Cost') }}</th>
					<th>{{ _lang('Product Price') }}</th>
					<th>{{ _lang('Product Unit') }}</th>
					<th class="text-center">{{ _lang('Available Stock') }}</th>
					<th>{{ _lang('Tax Method') }}</th>
					<th class="text-center">{{ _lang('Action') }}</th>
			  </tr>
			</thead>
			<tbody>
				
				@php $currency = currency(); @endphp
			  @foreach($items as $item)
			  <tr id="row_{{ $item->id }}">
					<td class='item_id'>{{ $item->item_name }}</td>
					<td class='product_cost'>{{ $currency.' '.$item->product->product_cost }}</td>
					<td class='product_price'>{{ $currency.' '.$item->product->product_price }}</td>
					<td class='product_unit'>{{ $item->product->product_unit }}</td>
					<td class='tax_method text-center'>{{ $item->product_stock->quantity }}</td>
					<td class='tax_method'>{{ ucwords($item->product->tax_method) }}</td>
					<td class="text-center">
						<form action="{{action('ProductController@destroy', $item['id'])}}" method="post">
						<a href="{{action('ProductController@edit', $item['id'])}}" data-title="{{ _lang('Update Product') }}" class="btn btn-warning btn-sm ajax-modal">{{ _lang('Edit') }}</a>
						<a href="{{action('ProductController@show', $item['id'])}}" data-title="{{ _lang('View Product') }}" class="btn btn-info btn-sm ajax-modal">{{ _lang('View') }}</a>
						{{ csrf_field() }}
						<input name="_method" type="hidden" value="DELETE">
						<button class="btn btn-danger btn-sm btn-remove" type="submit">{{ _lang('Delete') }}</button>
						</form>
					</td>
			  </tr>
			  @endforeach
			</tbody>
		  </table>
			</div>
		</div>
	</div>
</div>

@endsection


