<!DOCTYPE html>
<html lang="en">
<head>
<title>{{ get_option('site_title', 'Purchase Order') }}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">

<style type="text/css">
@php include public_path('css/bootstrap.css') @endphp
@php include public_path('css/style.css') @endphp

body { 
   -webkit-print-color-adjust: exact; !important;
   background: #FFF;
   font-size: 14px;
}
.table th {
	background-color: #2a77d6 !important;
	color: #ffffff;
}

.base_color{
	background: #2a77d6 !important;
}
.invoice-box {
	margin: auto;
	padding: 15px 0px;
	min-height: auto;
}
.invoice-logo{
	width: 100px;
}
.invoice-col-6{
  width: 50%;
  float:left;
  padding-right: 0px;
  padding-left: 0px;
}
	
	
</style>  
</head>

<body>

<div class="invoice-box pdf" id="quotation-view">
	<div class="col-md-12">
		 <table cellpadding="0" cellspacing="0">
				<tbody>
					 <tr class="top">
						<td colspan="2">
							 <table>
								<tbody>
									 <tr>
										<td>
											 <b>{{ _lang('Quotation') }} #:  {{ $quotation->quotation_number }}</b><br>
											 {{ _lang('Created') }}: <b>{{ date('F d, Y',strtotime( $quotation->quotation_date)) }}</b><br>						
										</td>
										<td class="invoice-logo">
											 <img src="{{ get_company_logo() }}" style="width:100px;">
										</td>
									 </tr>
								</tbody>
							 </table>
						</td>
					 </tr>
					 <tr class="information">
						<td colspan="2">
								
							<div class="invoice-col-6 mt-2">
								 <h4><b>{{ _lang('Quotation To') }}</b></h4>
								 @if(isset($quotation->client))	
									 {{ $quotation->client->contact_name }}<br>
									 {{ $quotation->client->contact_email }}<br>
									 {{ $quotation->client->company_name }}<br>
									 {{ $quotation->client->address }}<br>
								 @endif                        
							</div>
							<!--Company Address-->
							<div class="invoice-col-6 mt-2">
								<div class="d-inline-block float-md-right"> 
									<h4><b>{{ _lang('Company Details') }}</b></h4>
									{{ get_company_option('company_name') }}<br>
									{{ get_company_option('address') }}<br>
									{{ get_company_option('email') }}<br>
									
									<!--Invoice Payment Information-->
									<h4>{{ _lang('Quotation Total') }}: &nbsp;<b>{{ currency() }}</b> {{ decimalPlace($quotation->grand_total) }}</h4>
								</div>
							</div>
							<div class="clear"></div>		
						</td>
					</tr>
				</tbody>
			 </table>
		 </div>
		 <!--End Invoice Information-->
		 @php $currency = currency(); @endphp
		 <!--Invoice Product-->
		 <div>
			<div class="table-responsive">
				<table class="table table-bordered">
					 <thead style="background:#dce9f9;">
						 <tr>
							 <th>{{ _lang('Name') }}</th>
							 <th class="text-center" style="width:100px">{{ _lang('Quantity') }}</th>
							 <th class="text-right">{{ _lang('Unit Cost') }}</th>
							 <th class="text-right" style="width:100px">{{ _lang('Discount') }}</th>
							 <th class="no-print">{{ _lang('Tax method') }}</th>
							 <th class="text-right">{{ _lang('Tax') }}</th>
							 <th class="text-right">{{ _lang('Sub Total') }}</th>
						 </tr>
					 </thead>
					 <tbody  id="invoice">
						 @foreach($quotation->quotation_items as $item)
							 <tr id="product-{{ $item->item_id }}">
								 <td>{{ $item->item->item_name }}</td>
								 <td class="text-center">{{ $item->quantity }}</td>
								 <td class="text-right">{{ $currency.' '.$item->unit_cost }}</td>
								 <td class="text-right">{{ $currency.' '.$item->discount }}</td>
								 <td class="no-print">{{ isset($item->item->product) ? strtoupper($item->item->product->tax_method) : strtoupper($item->item->service->tax_method)  }}</td>
								 <td class="text-right">{{ $currency.' '.$item->tax_amount }}</td>
								 <td class="text-right">{{ $currency.' '.$item->sub_total }}</td>
							 </tr>
						 @endforeach
					 </tbody>
				</table>
			</div>	
		 </div>
		 <!--End Invoice Product-->	
		 <!--Summary Table-->
		 <div class="invoice-summary-right">
			<table class="table table-bordered" width="100%" style="background:#dce9f9">
				 <tbody>
						<tr>
							 <td>{{ _lang('Tax') }}</td>
							 <td class="text-right"><b>{{ $currency }}</b> {{ $quotation->tax_total }}</td>
						</tr>
						<tr>
							 <td>{{ _lang('Grand Total') }}</td>
							 <td class="text-right"><b>{{ $currency }}</b> {{ $quotation->grand_total }}</td>
						</tr>
				 </tbody>
			</table>
		 </div>
		 <!--End Summary Table-->
		 <div class="clear"></div>
		 <!--Related Transaction-->
		 <!--END Related Transaction-->		
		 <!--Invoice Note-->
		 @if($quotation->note  != '')
			<div class="invoice-note">{{ strip_tags($quotation->note) }}</div>
		 @endif
		 <!--End Invoice Note-->
	</div>
</body>
</html>
