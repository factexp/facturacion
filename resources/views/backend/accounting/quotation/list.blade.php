@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('Quotation List') }}</span>
			<a class="btn btn-primary btn-sm pull-right" href="{{route('quotations.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
				@php $currency = currency() @endphp
			  <table class="table table-bordered data-table">
				<thead>
				  <tr>
					<th>{{ _lang('Quotation Number') }}</th>
					<th>{{ _lang('Client') }}</th>
					<th>{{ _lang('Quotation Date') }}</th>
					<th class="text-right">{{ _lang('Grand Total') }}</th>
					<th class="text-center">{{ _lang('Action') }}</th>
				  </tr>
				</thead>
				<tbody>
				  
				  @foreach($quotations as $quotation)
				  <tr id="row_{{ $quotation->id }}">
					<td class='quotation_number'>{{ $quotation->quotation_number }}</td>
					<td class='client_id'>{{ $quotation->client->contact_name }}</td>
					<td class='due_date'>{{ date("d/M/Y",strtotime($quotation->quotation_date)) }}</td>
					<td class='grand_total text-right'>{{ $currency." ".$quotation->grand_total }}</td>
					<td class="text-center">

							<div class="dropdown">
									<button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">{{ _lang('Action') }}
									<span class="caret"></span></button>
									<ul class="dropdown-menu">
										<li><a href="{{ action('QuotationController@edit', $quotation->id) }}"><i class="fas fa-edit"></i> {{ _lang('Edit') }}</a></li>
										<li><a href="{{ action('QuotationController@show', $quotation->id) }}"><i class="fas fa-eye"></i> {{ _lang('View') }}</a></li>
										<li><a href="{{ action('QuotationController@convert_invoice', $quotation->id) }}"><i class="fas fa-exchange-alt"></i> {{ _lang('Convert to Invoice') }}</a></li>
										<li>
											<form action="{{action('QuotationController@destroy', $quotation->id)}}" method="post">									
												{{ csrf_field() }}
												<input name="_method" type="hidden" value="DELETE">
												<button class="button-link btn-remove" type="submit"><i class="fas fa-trash-alt"></i> {{ _lang('Delete') }}</button>
											</form>
										</li>
									</ul>
								</div>
					</td>
				  </tr>
				  @endforeach
				</tbody>
			  </table>
			</div>
		</div>
	</div>
</div>

@endsection


