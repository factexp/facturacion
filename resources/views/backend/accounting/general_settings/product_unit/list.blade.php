@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Product Unit') }}</span>
			<a class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ _lang('Add Product Unit') }}" href="{{route('product_units.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			<table class="table table-bordered data-table">
			<thead>
			  <tr>
				<th>{{ _lang('Unit Name') }}</th>
				<th class="text-center">{{ _lang('Action') }}</th>
			  </tr>
			</thead>
			<tbody>
			  
			  @foreach($productunits as $productunit)
			  <tr id="row_{{ $productunit->id }}">
				<td class='unit_name'>{{ $productunit->unit_name }}</td>
				<td class="text-center">
				  <form action="{{action('ProductUnitController@destroy', $productunit['id'])}}" method="post">
					<a href="{{action('ProductUnitController@edit', $productunit['id'])}}" data-title="{{ _lang('Update Product Unit') }}" class="btn btn-warning btn-sm ajax-modal">{{ _lang('Edit') }}</a>
					<a href="{{action('ProductUnitController@show', $productunit['id'])}}" data-title="{{ _lang('View Product Unit') }}" class="btn btn-info btn-sm ajax-modal">{{ _lang('View') }}</a>
					{{ csrf_field() }}
					<input name="_method" type="hidden" value="DELETE">
					<button class="btn btn-danger btn-sm btn-remove" type="submit">{{ _lang('Delete') }}</button>
				  </form>
				</td>
			  </tr>
			  @endforeach
			</tbody>
		  </table>
			</div>
		</div>
	</div>
</div>

@endsection


