@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
		  <ul class="nav nav-tabs setting-tab">
			  <li class="active"><a data-toggle="tab" href="#general" aria-expanded="true">{{ _lang('General') }}</a></li>
			  <li class=""><a data-toggle="tab" href="#payment-gateway" aria-expanded="false">{{ _lang('Payment Gateway') }}</a></li>
			  <li class=""><a data-toggle="tab" href="#logo" aria-expanded="false">{{ _lang('Logo') }}</a></li>
		  </ul>
		  <div class="tab-content">
				
			  <div id="general" class="tab-pane fade in active">
				  <div class="panel panel-default">
				  <div class="panel-heading"><span class="panel-title">{{ _lang('Company Settings') }}</span></div>

				  <div class="panel-body">
					  <form method="post" class="appsvan-submit params-panel" autocomplete="off" action="{{ url('company/general_settings/update') }}" enctype="multipart/form-data">
						{{ csrf_field() }}
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Company Name') }}</label>						
							<input type="text" class="form-control" name="company_name" value="{{ get_company_option('company_name') }}" required>
						  </div>
						</div>					
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Phone') }}</label>						
							<input type="text" class="form-control" name="phone" value="{{ get_company_option('phone') }}">
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Email') }}</label>						
							<input type="text" class="form-control" name="email" value="{{ get_company_option('email') }}">
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Currency Symbol') }}</label>						
							<input type="text" class="form-control" name="currency_symbol" value="{{ get_company_option('currency_symbol') }}" required>
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Invoice Prefix') }}</label>						
							<input type="text" class="form-control" name="invoice_prefix" value="{{ get_company_option('invoice_prefix') }}">
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Next Invoice Number') }}</label>						
							<input type="number" class="form-control" name="invoice_starting" min="1" value="{{ get_company_option('invoice_starting',1001) }}" required>
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Default Account') }}</label>						
							<select class="form-control select2" name="default_account" required>
						        <option value="">{{ _lang('Select One') }}</option>
								{{ create_option("accounts","id","account_title",get_company_option('default_account'),array("company_id="=>company_id())) }}
							</select>
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Default Income Category') }}</label>						
							<select class="form-control select2" name="default_chart_id" required>
						        <option value="">{{ _lang('Select One') }}</option>
								{{ create_option("chart_of_accounts","id","name",get_company_option('default_chart_id'),array("type="=>"income","AND company_id="=>company_id())) }}
							</select>
						  </div>
						</div>

						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Quotation Prefix') }}</label>						
							<input type="text" class="form-control" name="quotation_prefix" value="{{ get_company_option('quotation_prefix') }}">
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Next Quotation Number') }}</label>						
							<input type="number" class="form-control" name="quotation_starting" min="1" value="{{ get_company_option('quotation_starting',1001) }}" required>
						  </div>
						</div>
												
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Language') }}</label>						
							<select class="form-control select2" name="language">
								<option value="">{{ _lang('Select One') }}</option>
								{{ load_language( get_company_option('language') ) }}
							</select>
						  </div>
						</div>
						
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Theme Direction') }}</label>						
							<select class="form-control" name="backend_direction" required>
								<option value="ltr" {{ get_company_option('backend_direction') == 'ltr' ? 'selected' : '' }}>{{ _lang('LTR') }}</option>
								<option value="rtl" {{ get_company_option('backend_direction') == 'rtl' ? 'selected' : '' }}>{{ _lang('RTL') }}</option>
							</select>
						  </div>
						</div>
					
						<div class="col-md-6">
						  <div class="form-group">
							<label class="control-label">{{ _lang('Address') }}</label>						
							<textarea class="form-control" name="address">{{ get_company_option('address') }}</textarea>
						  </div>
						</div>

							
						<div class="form-group">
						  <div class="col-md-12">
							<button type="submit" class="btn btn-primary">{{ _lang('Save Settings') }}</button>
						  </div>
						</div>
					  </form>
				  </div>
				  </div>
			  </div>
			  
			  <div id="payment-gateway" class="tab-pane fade">
			     <div class="panel panel-default">
				  <div class="panel-heading"><span class="panel-title">{{ _lang('Payment Gateway') }}</span></div>
				    <div class="panel-body">
					   <form method="post" class="appsvan-submit params-panel" autocomplete="off" action="{{ url('company/general_settings/update') }}" enctype="multipart/form-data">				         
							
						    {{ csrf_field() }}
							<label>{{ _lang('PayPal') }}</label>
							<div class="params-panel">
								<div class="col-md-6">
								  <div class="form-group">
									<label class="control-label">{{ _lang('PayPal Active') }}</label>						
									<select class="form-control" name="paypal_active" required>
									   <option value="Yes" {{ get_company_option('paypal_active') == 'Yes' ? 'selected' : '' }}>{{ _lang('Yes') }}</option>
									   <option value="No" {{ get_company_option('paypal_active') == 'No' ? 'selected' : '' }}>{{ _lang('No') }}</option>
									</select>
								  </div>
								</div>
								
								
								<div class="col-md-6">
								  <div class="form-group">
									<label class="control-label">{{ _lang('PayPal Currency') }}</label>						
									<select class="form-control select2" name="paypal_currency" id="paypal_currency">
										<option value="USD">{{ _lang('U.S. Dollar') }}</option>
										<option value="AUD">{{ _lang('Australian Dollar') }}</option>
										<option value="BRL">{{ _lang('Brazilian Real') }}</option>
										<option value="CAD">{{ _lang('Canadian Dollar') }}</option>
										<option value="CZK">{{ _lang('Czech Koruna') }}</option>
										<option value="DKK">{{ _lang('Danish Krone') }}</option>
										<option value="EUR">{{ _lang('Euro') }}</option>
										<option value="HKD">{{ _lang('Hong Kong Dollar') }}</option>
										<option value="HUF">{{ _lang('Hungarian Forint') }}</option>
										<option value="INR">{{ _lang('Indian Rupee') }}</option>
										<option value="ILS">{{ _lang('Israeli New Sheqel') }}</option>
										<option value="JPY">{{ _lang('Japanese Yen') }}</option>
										<option value="MYR">{{ _lang('Malaysian Ringgit') }}</option>
										<option value="MXN">{{ _lang('Mexican Peso') }}</option>
										<option value="NOK">{{ _lang('Norwegian Krone') }}</option>
										<option value="NZD">{{ _lang('New Zealand Dollar') }}</option>
										<option value="PHP">{{ _lang('Philippine Peso') }}</option>
										<option value="PLN">{{ _lang('Polish Zloty') }}</option>
										<option value="GBP">{{ _lang('Pound Sterling') }}</option>
										<option value="SGD">{{ _lang('Singapore Dollar') }}</option>
										<option value="SEK">{{ _lang('Swedish Krona') }}</option>
										<option value="CHF">{{ _lang('Swiss Franc') }}</option>
										<option value="TWD">{{ _lang('Taiwan New Dollar') }}</option>
										<option value="THB">{{ _lang('Thai Baht') }}</option>
										<option value="TRY">{{ _lang('Turkish Lira') }}</option>
									</select>
								  </div>
								</div>
								
								<div class="col-md-6">
								  <div class="form-group">
									<label class="control-label">{{ _lang('PayPal Email') }}</label>						
									<input type="text" class="form-control" name="paypal_email" value="{{ get_company_option('paypal_email') }}">
								  </div>
								</div>
								
							</div>
							
							<br>
							<label>{{ _lang('Stripe Configuration') }}</label>
							<div class="params-panel">								
								<div class="col-md-6">
								  <div class="form-group">
									<label class="control-label">{{ _lang('Stripe Active') }}</label>						
									<select class="form-control" name="stripe_active" required>
									   <option value="yes" {{ get_company_option('stripe_active') == 'yes' ? 'selected' : '' }}>{{ _lang('Yes') }}</option>
									   <option value="no" {{ get_company_option('stripe_active') == 'no' ? 'selected' : '' }}>{{ _lang('No') }}</option>
									</select>
								  </div>
								</div>
								
								<div class="col-md-6">
								  <div class="form-group">
									<label class="control-label">{{ _lang('Stripe Currency') }}</label>						
									<select class="form-control select2" name="stripe_currency" id="stripe_currency">
										<option value="USD">{{ _lang('U.S. Dollar') }}</option>
										<option value="AUD">{{ _lang('Australian Dollar') }}</option>
										<option value="BRL">{{ _lang('Brazilian Real') }}</option>
										<option value="CAD">{{ _lang('Canadian Dollar') }}</option>
										<option value="CZK">{{ _lang('Czech Koruna') }}</option>
										<option value="DKK">{{ _lang('Danish Krone') }}</option>
										<option value="EUR">{{ _lang('Euro') }}</option>
										<option value="HKD">{{ _lang('Hong Kong Dollar') }}</option>
										<option value="HUF">{{ _lang('Hungarian Forint') }}</option>
										<option value="INR">{{ _lang('Indian Rupee') }}</option>
										<option value="ILS">{{ _lang('Israeli New Sheqel') }}</option>
										<option value="JPY">{{ _lang('Japanese Yen') }}</option>
										<option value="MYR">{{ _lang('Malaysian Ringgit') }}</option>
										<option value="MXN">{{ _lang('Mexican Peso') }}</option>
										<option value="NOK">{{ _lang('Norwegian Krone') }}</option>
										<option value="NZD">{{ _lang('New Zealand Dollar') }}</option>
										<option value="PHP">{{ _lang('Philippine Peso') }}</option>
										<option value="PLN">{{ _lang('Polish Zloty') }}</option>
										<option value="GBP">{{ _lang('Pound Sterling') }}</option>
										<option value="SGD">{{ _lang('Singapore Dollar') }}</option>
										<option value="SEK">{{ _lang('Swedish Krona') }}</option>
										<option value="CHF">{{ _lang('Swiss Franc') }}</option>
										<option value="TWD">{{ _lang('Taiwan New Dollar') }}</option>
										<option value="THB">{{ _lang('Thai Baht') }}</option>
										<option value="TRY">{{ _lang('Turkish Lira') }}</option>
									</select>
								  </div>
								</div>
								
								<div class="col-md-6">
								  <div class="form-group">
									<label class="control-label">{{ _lang('Secret Key') }}</label>						
									<input type="text" class="form-control" name="stripe_secret_key" value="{{ get_company_option('stripe_secret_key') }}">
								  </div>
								</div>
								
								<div class="col-md-6">
								  <div class="form-group">
									<label class="control-label">{{ _lang('Publishable Key') }}</label>						
									<input type="text" class="form-control" name="stripe_publishable_key" value="{{ get_company_option('stripe_publishable_key') }}">
								  </div>
								</div>
                            </div>
								
							<div class="form-group">
							  <div class="col-md-12">
								<button type="submit" class="btn btn-primary">{{ _lang('Save Settings') }}</button>
							  </div>
							</div>
								
							
					   </form>	
				   </div>
				 </div>
			  </div>

			  
			  <div id="logo" class="tab-pane fade">
			     <div class="panel panel-default">
				  <div class="panel-heading"><span class="panel-title">{{ _lang('Logo Upload') }}</span></div>
				    <div class="panel-body">
					   <form method="post" class="appsvan-submit params-panel" autocomplete="off" action="{{ url('company/upload_logo') }}" enctype="multipart/form-data">				         
							
							{{ csrf_field() }}
							
							<div class="col-md-6 col-md-offset-3">
							  <div class="form-group">
								<label class="control-label">{{ _lang('Upload Logo') }}</label>						
								<input type="file" class="form-control dropify" name="logo" data-max-file-size="8M" data-allowed-file-extensions="png jpg jpeg PNG JPG JPEG" data-default-file="{{ get_company_logo() }}" required>
							  </div>
							</div>
							
							<br>
							<div class="form-group">
							  <div class="col-md-4 col-md-offset-4">
								<button type="submit" class="btn btn-primary btn-block">{{ _lang('Upload') }}</button>
							  </div>
							</div>	
							
					   </form>	
				   </div>
				 </div>
			  </div>
			  
		   </div>  
		</div>
	  </div>
     </div>
    </div>
@endsection

@section('js-script')
<script type="text/javascript">
if($("#mail_type").val() != "smtp"){
	$(".smtp").prop("disabled",true);
}
$(document).on("change","#mail_type",function(){
	if( $(this).val() != "smtp" ){
		$(".smtp").prop("disabled",true);
	}else{
		$(".smtp").prop("disabled",false);
	}
});

$("#paypal_currency").val("{{ get_company_option('paypal_currency') }}");
$("#stripe_currency").val("{{ get_company_option('stripe_currency') }}");


</script>
@stop

