@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading">{{ _lang('View Email Template') }}</div>

	<div class="panel-body">
	  <table class="table table-bordered">
		<tr><td>{{ _lang('Name') }}</td><td>{{ $companyemailtemplate->name }}</td></tr>
			<tr><td>{{ _lang('Subject') }}</td><td>{{ $companyemailtemplate->subject }}</td></tr>
			<tr><td>{{ _lang('Body') }}</td><td>{{ $companyemailtemplate->body }}</td></tr>
			<tr><td>{{ _lang('Company Id') }}</td><td>{{ $companyemailtemplate->company_id }}</td></tr>
			
	  </table>
	</div>
  </div>
 </div>
</div>
@endsection


