@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading panel-title">{{ _lang('Update Email Template') }}</div>

			<div class="panel-body">
				<form method="post" class="validate" autocomplete="off" action="{{action('CompanyEmailTemplateController@update', $id)}}" enctype="multipart/form-data">
					{{ csrf_field()}}
					<input name="_method" type="hidden" value="PATCH">				
					
					<div class="col-md-12">
					 <div class="form-group">
						<label class="control-label">{{ _lang('Name') }}</label>						
						<input type="text" class="form-control" name="name" value="{{ $companyemailtemplate->name }}" required>
					 </div>
					</div>

					<div class="col-md-12">
					 <div class="form-group">
						<label class="control-label">{{ _lang('Subject') }}</label>						
						<input type="text" class="form-control" name="subject" value="{{ $companyemailtemplate->subject }}" required>
					 </div>
					</div>

					<div class="col-md-12">
					 <div class="form-group">
						<label class="control-label">{{ _lang('Body') }}</label>						
						<textarea class="form-control summernote" name="body" required>{{ $companyemailtemplate->body }}</textarea>
					 </div>
					</div>
				
					<div class="col-md-12">
					  <div class="form-group">
						<button type="submit" class="btn btn-primary">{{ _lang('Update') }}</button>
					  </div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection


