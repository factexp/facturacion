@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading panel-title">{{ _lang('Add Email Template') }}</div>

	<div class="panel-body">
		  <form method="post" class="validate" autocomplete="off" action="{{url('company_email_template')}}" enctype="multipart/form-data">
			{{ csrf_field() }}
			
			<div class="col-md-12">
			  <div class="form-group">
				<label class="control-label">{{ _lang('Name') }}</label>						
				<input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
			  </div>
			</div>

			<div class="col-md-12">
			  <div class="form-group">
				<label class="control-label">{{ _lang('Subject') }}</label>						
				<input type="text" class="form-control" name="subject" value="{{ old('subject') }}" required>
			  </div>
			</div>

			<div class="col-md-12">
			  <div class="form-group">
				<label class="control-label">{{ _lang('Body') }}</label>						
				<textarea class="form-control summernote" name="body" required>{{ old('body') }}</textarea>
			  </div>
			</div>

				
			<div class="col-md-12">
			  <div class="form-group">
				<button type="reset" class="btn btn-danger">{{ _lang('Reset') }}</button>
				<button type="submit" class="btn btn-primary">{{ _lang('Save') }}</button>
			  </div>
			</div>
		  </form>
	  </div>
	</div>
  </div>
 </div>
</div>
@endsection


