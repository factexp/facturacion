@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Email Template') }}</span>
			<a class="btn btn-primary btn-sm pull-right" data-title="{{ _lang('Add Email Template') }}" href="{{ route('company_email_template.create') }}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			 <table class="table table-bordered data-table">
				<thead>
				  <tr>
					<th>{{ _lang('Name') }}</th>
					<th>{{ _lang('Subject') }}</th>
					<th class="text-center">{{ _lang('Action') }}</th>
				  </tr>
				</thead>
				<tbody>
				  
				  @foreach($companyemailtemplates as $companyemailtemplate)
				  <tr id="row_{{ $companyemailtemplate->id }}">
					<td class='name'>{{ $companyemailtemplate->name }}</td>
					<td class='subject'>{{ $companyemailtemplate->subject }}</td>
					<td class="text-center">
					  <form action="{{ action('CompanyEmailTemplateController@destroy', $companyemailtemplate['id']) }}" method="post">
						<a href="{{ action('CompanyEmailTemplateController@edit', $companyemailtemplate['id']) }}" class="btn btn-warning btn-xs">{{ _lang('Edit') }}</a>
						<a href="{{ action('CompanyEmailTemplateController@show', $companyemailtemplate['id']) }}" class="btn btn-info btn-xs ajax-modal" data-title="{{ _lang('View Email Template') }}">{{ _lang('View') }}</a>
						{{ csrf_field() }}
						<input name="_method" type="hidden" value="DELETE">
						<button class="btn btn-danger btn-xs btn-remove" type="submit">{{ _lang('Delete') }}</button>
					  </form>
					</td>
				  </tr>
				  @endforeach
				</tbody>
			  </table>
			</div>
		</div>
	</div>
</div>

@endsection


