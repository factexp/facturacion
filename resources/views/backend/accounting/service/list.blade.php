@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Service') }}</span>
			<a class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ _lang('Add New Service') }}" href="{{ route('services.create') }}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			 @if (\Session::has('success'))
			  <div class="alert alert-success">
				<p>{{ \Session::get('success') }}</p>
			  </div>
			  <br />
			 @endif
			<table class="table table-bordered data-table">
			<thead>
			  <tr>
					<th>{{ _lang('Service') }}</th>
					<th>{{ _lang('Cost') }}</th>
					<th>{{ _lang('Tax Method') }}</th>
					<th class="text-center">{{ _lang('Action') }}</th>
			  </tr>
			</thead>
			<tbody>
				
				@php $currency = currency(); @endphp
			  @foreach($items as $item)
			  <tr id="row_{{ $item->id }}">
					<td class='item_id'>{{ $item->item_name }}</td>
					<td class='cost'>{{ $currency.' '.$item->service->cost }}</td>
					<td class='tax_method'>{{ ucwords($item->service->tax_method) }}</td>
					<td class="text-center">
						<form action="{{action('ServiceController@destroy', $item['id'])}}" method="post">
						<a href="{{action('ServiceController@edit', $item['id'])}}" data-title="{{ _lang('Update Service') }}" class="btn btn-warning btn-sm ajax-modal">{{ _lang('Edit') }}</a>
						<a href="{{action('ServiceController@show', $item['id'])}}" data-title="{{ _lang('View Service') }}" class="btn btn-info btn-sm ajax-modal">{{ _lang('View') }}</a>
						{{ csrf_field() }}
						<input name="_method" type="hidden" value="DELETE">
						<button class="btn btn-danger btn-sm btn-remove" type="submit">{{ _lang('Delete') }}</button>
						</form>
					</td>
			  </tr>
			  @endforeach
			</tbody>
		  </table>
			</div>
		</div>
	</div>
</div>

@endsection


