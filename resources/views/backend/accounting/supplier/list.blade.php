@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Of Supplier') }}</span>
			<a class="btn btn-primary btn-sm pull-right ajax-modal" data-title="{{ _lang('Add Supplier') }}" href="{{route('suppliers.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			 @if (\Session::has('success'))
			  <div class="alert alert-success">
				<p>{{ \Session::get('success') }}</p>
			  </div>
			  <br />
			 @endif
			<table class="table table-bordered data-table">
					<thead>
						<tr>
							<th>{{ _lang('Supplier Name') }}</th>
							<th>{{ _lang('Company Name') }}</th>
							<th>{{ _lang('Vat Number') }}</th>
							<th>{{ _lang('Email') }}</th>
							<th>{{ _lang('Phone') }}</th>
							<th class="text-center">{{ _lang('Action') }}</th>
						</tr>
					</thead>
					<tbody>
						
						@foreach($suppliers as $supplier)
						<tr id="row_{{ $supplier->id }}">
							<td class='supplier_name'>{{ $supplier->supplier_name }}</td>
							<td class='company_name'>{{ $supplier->company_name }}</td>
							<td class='vat_number'>{{ $supplier->vat_number }}</td>
							<td class='email'>{{ $supplier->email }}</td>
							<td class='phone'>{{ $supplier->phone }}</td>
							<td class="text-center">
								<form action="{{action('SupplierController@destroy', $supplier['id'])}}" method="post">
								<a href="{{action('SupplierController@edit', $supplier['id'])}}" data-title="{{ _lang('Update Supplier Information') }}" class="btn btn-warning btn-sm ajax-modal">{{ _lang('Edit') }}</a>
								<a href="{{action('SupplierController@show', $supplier['id'])}}" data-title="{{ _lang('View Supplier') }}" class="btn btn-info btn-sm ajax-modal">{{ _lang('View') }}</a>
								{{ csrf_field() }}
								<input name="_method" type="hidden" value="DELETE">
								<button class="btn btn-danger btn-sm btn-remove" type="submit">{{ _lang('Delete') }}</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
		  	</table>
			</div>
		</div>
	</div>
</div>

@endsection


