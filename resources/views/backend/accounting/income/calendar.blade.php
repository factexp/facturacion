@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading">{{ _lang('Income Calendar') }}</div>

	<div class="panel-body">
		<div id='income_calendar'></div>
	</div>
  </div>
 </div>
</div>
@endsection

@section('js-script')
<script>

$(document).ready(function() {
	$('#income_calendar').fullCalendar({
			themeSystem: 'bootstrap4',	
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			//defaultDate: '2018-03-12',
			eventBackgroundColor: "#3F51B5",
			eventBorderColor: "#3F51B5",
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			timeFormat: 'h:mm',
			events: [
			    @php $currency = currency(); @endphp
				@foreach($transactions as $trans)
					{
					  title: '{{ $trans->income_type->name." - ".$currency." ".$trans->amount }}',
					  start: '{{ $trans->trans_date }}',
					  url: '{{ action("IncomeController@show", $trans->id) }}'
					},
				@endforeach
		   ],
		   eventRender: function eventRender(event, element, view) {
			   element.addClass('ajax-modal');
			   element.data("title","{{ _lang('View Income') }}");
		   }
	});
});
</script>	
@endsection


