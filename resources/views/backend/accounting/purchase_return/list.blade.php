@extends('layouts.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default no-export">
			<div class="panel-heading"><span class="panel-title">{{ _lang('List Purchase Return') }}</span>
			<a class="btn btn-primary btn-sm pull-right" href="{{route('purchase_returns.create')}}">{{ _lang('Add New') }}</a>
			</div>

			<div class="panel-body">
			<table class="table table-bordered data-table">
				<thead>
					<tr>
						<th>{{ _lang('Return Date') }}</th>
						<th>{{ _lang('Supplier') }}</th>
						<th>{{ _lang('Account') }}</th>
						<th class="text-right">{{ _lang('Grand Total') }}</th>
						<th class="text-center">{{ _lang('Action') }}</th>
					</tr>
				</thead>
				<tbody>
			    @php $currency = currency(); @endphp		
					@foreach($purchase_returns as $purchase)
					<tr id="row_{{ $purchase->id }}">
						  <td class='order_date'>{{ date('d/M/Y',strtotime($purchase->return_date)) }}</td>
							<td class='supplier_id'>{{ $purchase->supplier->supplier_name }}</td>
							<td class='account_id'>{{ $purchase->account->account_title }}</td>
							<td class='grand_total text-right'>{{ $currency.' '.$purchase->grand_total }}</td>	
							<td class="text-center">
								
								<div class="dropdown">
									<button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown">{{ _lang('Action') }}
									<span class="caret"></span></button>
									<ul class="dropdown-menu">
										<li><a href="{{ action('PurchaseReturnController@edit', $purchase->id) }}"><i class="fas fa-edit"></i> {{ _lang('Edit') }}</a></li>
										<li><a href="{{ action('PurchaseReturnController@show', $purchase->id) }}" data-title="{{ _lang('View Purchase Return') }}" data-fullscreen="true" class="ajax-modal"><i class="fas fa-eye"></i> {{ _lang('View') }}</a></li>
										<li>
											<form action="{{action('PurchaseReturnController@destroy', $purchase['id'])}}" method="post">									
												{{ csrf_field() }}
												<input name="_method" type="hidden" value="DELETE">
												<button class="button-link btn-remove" type="submit"><i class="fas fa-trash-alt"></i> {{ _lang('Delete') }}</button>
											</form>
										</li>
									</ul>
								</div>
								
							</td>
					</tr>
					@endforeach
				</tbody>
		  </table>
			</div>
		</div>
	</div>
</div>

@endsection


