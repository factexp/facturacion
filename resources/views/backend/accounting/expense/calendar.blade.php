@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-12">
	<div class="panel panel-default">
	<div class="panel-heading">{{ _lang('Expense Calendar') }}</div>

	<div class="panel-body">
		<div id='expense_calendar'></div>
	</div>
  </div>
 </div>
</div>
@endsection

@section('js-script')
<script>

$(document).ready(function() {
	$('#expense_calendar').fullCalendar({
			themeSystem: 'bootstrap4',	
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},
			//defaultDate: '2018-03-12',
			eventBackgroundColor: "#F44336",
			eventBorderColor: "#F44336",
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			timeFormat: 'h:mm',
			events: [
				@php $currency = currency(); @endphp
				@foreach($transactions as $trans)
					{
					  title: '{{ $trans->income_type->name." - ".$currency." ".$trans->amount }}',
					  start: '{{ $trans->trans_date }}',
					  url: '{{ action("ExpenseController@show", $trans->id) }}'
					},
				@endforeach
		   ],
		   eventRender: function eventRender(event, element, view) {
			   element.addClass('ajax-modal');
			   element.data("title","{{ _lang('View Expense') }}");
		   }
	});
});
</script>	
@endsection


